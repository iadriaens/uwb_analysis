# -*- coding: utf-8 -*-
"""
Created on Tue Sep 23 15:15:01 2014

@author: matti pastell
@editor: ines adriaens
"""
# import packages
import pandas as pd
import struct
import numpy as np

# define functions
def readTrackLab(name):
    f2 = open(name, "rb")
    b = f2.read()
    f2.close()
    sp = b.split(b"S\x00e\x00s\x00s\x00i\x00o\x00n")
    ids = []
    coords = []
    for track in sp[1:-1]:
        if (len(track) > (94+83)):
            id = track[:75].replace(b"\x00", b"")[11:].split(b"\x01")[0].decode("utf-8")
            ids.append(id)
            start_idx = track[:100].rfind(b"\x1a") + 3
            coords.append(TrackToCoords(track[start_idx:], id))

    df = pd.concat(coords)
    df.time = pd.to_datetime(df.time, format="%Y-%m-%d %H:%M:%S")
    return df

#Recursive is too slow
def splitter(s,r=[],w=84):
    pos = 0
    N = len(s)    
    while pos < (N-w):
        r.append(s[pos:(w+pos)])
        pos = pos+w
    return(r)
        

def TrackToCoords(chunk, id):
    x = []
    y = []
    z = []
    time = []     
    Track = splitter(chunk, [])
    tulos = []
    
    for row in Track:
        time = row[:52].replace(b"\x00",b"").decode("utf-8")
        x,y,z = struct.unpack('3d', row[53:][:24])
        tulos.append({"time" : time, "id" : id , "x" : x, "y" : y, "z" : z})
    #print "Converter is done!"    
    return(pd.DataFrame(tulos))


def distancetraveled(x, y):
    x1 = x[:-1,]
    x2 = x[1:,]
    y1 = y[:-1,]
    y2 = y[1:,]
    return(np.sqrt(((x2-x1)**2+(y2-y1)**2)))        

def speed(xs, ys):
    return(np.sqrt(xs**2+ys**2))

