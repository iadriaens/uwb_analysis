%% HPC_smooth1
%
%
%

% set i
i = 1;

disp(['HPC_smooth_' num2str(i) ' has started'])


% set outname
% outname = ['hpc_smooth_' num2str(i) '.mat'];

% load data
load('D1_trkloaded.mat')
fields_ = fieldnames(cdata);

% select data
data = cdata.(fields_{i});

% distance travelled, data imputation and calculation of robust smooth
timerval = zeros(length(fields_),2); % var storing gap propagation and smooth
    
% calculate gapsize
ind = find(~isnan(data.avg_x));
gapsize = diff([floor(datenum(data.date(1)));...
    datenum(data.date(ind))])*24*60*60;

% add gapsize to cdata
data.gapsize(:,1) = NaN;
data.gapsize(ind) = round(gapsize);

% calculate distances when <5s missing (gapsize <=5)
set = data(~isnan(data.avg_x) & ...
    (data.gapsize <= 5 | ...
    isnan(data.gapsize)),:);

% distance travelled expressed per second
set.dist(1) = NaN;     % first
set.dist(2:end) = sqrt(diff(set.avg_x).^2 + ...
    (diff(set.avg_y).^2));
set.dist = set.dist./set.gapsize;

% join data with cdata
data = outerjoin(data,set, ...
    'Keys',{'object_name',...
    'date',...
    'time_in_h',...
    'avg_x','gapsize',...
    'avg_y','avg_z'},...
    'MergeKeys',1);

% missing data imputation > the larger the gap (init_.gap) the larger
% the window used to calculate the distribution to sample from
% (init_.window)
tic
data = ...
    fillnormgaps(data,2:60,round(linspace(5,120,length(2:60))));
timerval(i,1) = toc



%% visualise and save


% figure
h = figure('Units','centimeters','Outerposition',[0 0 30 20]);
subplot(3,1,1);hold on; box on;title(['i = ' num2str(i) ', cow ' fields_{i} ', smoothed y'])
xlabel('Date'); ylabel('Position (m)')
plot(data.date,data.avg_y_sm,'k','LineWidth',0.8)
plot(data.date(data.misscode == 0),data.avg_y_sm(data.misscode == 0),'o','LineWidth',1,'MarkerSize',2,'Color',[102/255 178/255 255/255])
plot(data.date(data.misscode == 1),data.avg_y_sm(data.misscode == 1),'o','LineWidth',1,'MarkerSize',2,'Color',[204/255 204/255 255/255])
plot(data.date(data.misscode == 2),data.avg_y_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2)

subplot(3,1,2);hold on; box on; title('Smoothed x')
xlabel('Date'); ylabel('Position (m)')
plot(data.date,data.avg_x_sm,'k','LineWidth',0.8)
plot(data.date(data.misscode == 0),data.avg_x_sm(data.misscode == 0),'mo','LineWidth',1,'MarkerSize',2,'Color',[255/255 204/255 204/255])
plot(data.date(data.misscode == 1),data.avg_x_sm(data.misscode == 1),'ro','LineWidth',1,'MarkerSize',2,'Color',[255/255 102/255 102/255])
plot(data.date(data.misscode == 2),data.avg_x_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2,'Color',[255/255 51/255 51/255])

subplot(3,1,3);hold on; box on; title('Smoothed distance')
xlabel('Date'); ylabel('Position (m)')
plot(data.date,data.dist_sm,'k','LineWidth',0.8)
plot(data.date(data.misscode == 0),data.dist_sm(data.misscode == 0),'mo','LineWidth',1,'MarkerSize',2,'Color',[204/255 255/255 204/255])
plot(data.date(data.misscode == 1),data.dist_sm(data.misscode == 1),'ro','LineWidth',1,'MarkerSize',2,'Color',[102/255 255/255 102/255])
plot(data.date(data.misscode == 2),data.dist_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2,'Color',[51/255 205/255 51/255])

plot(data.date(isnan(data.misscode) & data.dist_sm > 0.5),...
     data.dist_sm(isnan(data.misscode) & data.dist_sm > 0.5), 'rx',...
     'MarkerSize',4,'LineWidth',1)

saveas(h, ['Fig_Data_imputation_' num2str(i) '.fig'])
saveas(h, ['Fig_Data_imputation_' num2str(i) '.tif'])

save(['data_cow_' num2str(i) '.mat'], 'data','timerval')

disp(['HPC_smooth_' num2str(i) ' is finished'])



%% function
function out = fillnormgaps(data,gaps,windows)
    data.misscode(:,1) = NaN;
    data.dist_xy(:,1) = NaN;
    for j = 1:length(gaps)   % per gapsize
        ind = find(data.gapsize == gaps(j));
        startidx = max(1,ind-windows(j));  % start indices
        endidx = ind;               % end indices
        
        for k = 1:length(ind)
            numnan = sum(~isnan(data.gapsize(...
                                       startidx(k):endidx(k))));
            if numnan < 0.5*windows(j)   % if not enough data available
                data.misscode(endidx(k)-gaps(j)+1:endidx(k)) = 0;
                
                % interpolation dist
                idx = find(~isnan(data.dist(startidx(k):endidx(k)-1)),1,'last');
                if ~isempty(idx)
                    r = interp1([startidx(k)+idx-1 endidx(k)+1],...
                            [data.dist(startidx(k)+idx-1) data.dist(endidx(k)+1)],...
                            startidx(k)+idx:endidx(k));
                    data.dist(startidx(k)+idx:endidx(k)) = r;
                end
                
                idx = find(~isnan(data.avg_x(startidx(k):endidx(k)-1)),1,'last');
                if ~isempty(idx)
                    % interpolation avg_x
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_x(startidx(k)+idx-1) data.avg_x(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);                           
                    data.avg_x(startidx(k)+idx:endidx(k)-1) = r;

                    % interpolation avg_y
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_y(startidx(k)+idx-1) data.avg_y(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);
                    data.avg_y(startidx(k)+idx:endidx(k)-1) = r;

                    % interpolation avg_z
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_z(startidx(k)+idx-1) data.avg_z(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);
                    data.avg_z(startidx(k)+idx:endidx(k)-1) = r;

                    % dist_xy
                    data.dist_xy(ind(k)-(gaps(j)-1):ind(k)-1)...
                            = sqrt(diff(data.avg_x(ind(k)-(gaps(j)):ind(k)-1)).^2 + ...
                              (diff(data.avg_y(ind(k)-(gaps(j)):ind(k)-1)).^2));
                end
                
            else
                % indicate misscode = 1 = filled with ~N(m,s)
                data.misscode(endidx(k)-gaps(j)+1:endidx(k)) = 1;
                
                % dist
                m = nanmean(data.dist(startidx(k):endidx(k)));
                s = nanstd(data.dist(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.dist(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % avg_z
                m = nanmean(data.avg_z(startidx(k):endidx(k)));
                s = nanstd(data.avg_z(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_z(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
               % avg_x
                m = nanmean(data.avg_x(startidx(k):endidx(k)));
                s = nanstd(data.avg_x(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_x(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % avg_y
                m = nanmean(data.avg_y(startidx(k):endidx(k)));
                s = nanstd(data.avg_y(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_y(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % dist_xy
                data.dist_xy(ind(k)-(gaps(j)-1):ind(k)-1)...
                        = sqrt(diff(data.avg_x(ind(k)-(gaps(j)):ind(k)-1)).^2 + ...
                          (diff(data.avg_y(ind(k)-(gaps(j)):ind(k)-1)).^2));

           end
        end 
    end
    
    % if gap > max gapsize
    ind = find(data.gapsize > max(gaps) & data.gapsize < 3*60);
    
    
    for j = 1:length(ind)
        % misscode = 2
        data.misscode(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = 2;
    
        % find last available measurement (dist)
        firstarray = data(ind(j)-data.gapsize(ind(j)),:);
        lastarray = data(ind(j)+1,:);
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)+1],...
                [firstarray.dist lastarray.dist],...
                ind(j)-data.gapsize(ind(j))+1:ind(j));
        data.dist(ind(j)-data.gapsize(ind(j))+1:ind(j)) = r;
    
        % avg_x, avg_y, avg_z
        firstarray = data(ind(j)-data.gapsize(ind(j)),:);
        lastarray = data(ind(j),:);
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_x lastarray.avg_x],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_x(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_y lastarray.avg_y],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_y(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_z lastarray.avg_z],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_z(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        % dist_xy
        data.dist_xy(ind(j)-data.gapsize(ind(j))+1:ind(j)-1)...
                = sqrt(diff(data.avg_x(ind(j)-data.gapsize(ind(j))+1:ind(j))).^2 + ...
                  (diff(data.avg_y(ind(j)-data.gapsize(ind(j))+1:ind(j))).^2));
        
    end
    
    % boundaries based on no-imputed data
    data.avg_x(data.avg_x < 0) = 0;
    data.avg_x(data.avg_x > max(data.avg_x(isnan(data.misscode)))) = ...
                            max(data.avg_x(isnan(data.misscode)));
    data.avg_y(data.avg_y < 0) = 0;
    data.avg_y(data.avg_y > max(data.avg_y(isnan(data.misscode)))) = ...
                            max(data.avg_y(isnan(data.misscode)));
    data.avg_z(data.avg_z < 0) = 0;
    data.avg_z(data.avg_z > max(data.avg_z(isnan(data.misscode)))) = ...
                            max(data.avg_z(isnan(data.misscode)));
                           
                        
    % smooth then dist
    data.avg_x_sm(:,1) = movmedian(data.avg_x,45);
    data.avg_y_sm(:,1) = movmedian(data.avg_y,45);
    data.avg_z_sm(:,1) = movmedian(data.avg_z,45);
    data.dist_sm(:,1) = 0;
    data.dist_sm(2:end) = sqrt(diff(data.avg_x_sm).^2 + ...
                  (diff(data.avg_y_sm).^2));
    
    % boundaries for dist
    data.dist_sm(data.dist_sm < 0) = 0;
    data.dist_sm(data.dist_sm > 1.5) = 1.5;


    out = data;
end
    

