# -*- coding: utf-8 -*-
"""
Created on Wed Oct 01 13:39:40 2014

@author: psa119
"""
# import packages
import P0_MPastell_tracklab as tracklab
import pandas as pd
import glob
import gzip
import os

# read files
files = glob.glob(r"WURNET.NL/Homes/adria036/AppData/FolderRedirection/*.tlp")



for f in files:
    try:
        df, l = tracklab.readTrackLab(f)
        outfile = f.replace("Tlab_data", "gz_data").replace(".tlp", ".csv")
        df.to_csv(outfile, float_format="%.4f")
        
        f_in = open(outfile, "rb") 
        gf = gzip.open(outfile + ".gz", 'wb', 9)
        gf.writelines(f_in)
        f_in.close()
        gf.close()
        os.remove(outfile)
        
        #os.system('\"c:\Program Files\7-Zip\7z.exe\" a -tgzip ' +  outfile + ".gz" + outfile)
        print(f)
    except:
        print("Error reading", f)
        continue
    


f_in = open(files[0], 'rb')
f_out = gzip.open('file.txt.gz', 'wb')
f_out.writelines(f_in)
f_out.close()
f_in.close()