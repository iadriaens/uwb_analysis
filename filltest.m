
figure()
t = 10:10:100;
A = [0.1 0.2 0.3 NaN NaN 0.6 0.7 NaN 0.9 1];
plot(t,A,'o')

n = 2;
gapwindow = [30 0];

[F,TF] = fillmissing(A,@(xs,ts,tq) forwardfill(xs,ts,tq,n),gapwindow,'SamplePoints',t);


function y = forwardfill(xs,ts,tq,n)
    % Fill n values in the missing gap using the previous nonmissing value
    y = NaN(1,numel(tq));
    y(1:min(numel(tq),n)) = xs;
end


% xs � data values used for filling
% 
% ts � locations of the values used for filling relative to the sample points
% 
% tq � locations of the missing values relative to the sample points
% 
% n � number of values in the gap to fill