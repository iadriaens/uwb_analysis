%% S11_CP_plan
%
% Time series segmentation


clear variables
close all
clc

%% STEP 1: constants and filepaths
% this folder contains the results of the smoothing and data imputation
% scripts

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];

%% STEP 2: load data            
% data resulting from data imputation script / reference data
% the 'ground truth' = comparison base = data_meta.selection = from first
% to last bout

% load data
load([init_.resdir 'D3_sdata_IQ_UWB.mat'])  % from first to last icebout
load([init_.resdir 'D2_data_meta.mat'])

% delete cow 280 (no data)
sdata = rmfield(sdata,'cow_280');

%% STEP3: characterize number of bouts per day

% duration of lying down
ind = find(data_meta.selection.durlydown <10);
length(ind)/height(data_meta.selection)*100 % 13%

% duration of standing
data_meta.selection.endtime = data_meta.selection.time + ...
             minutes(data_meta.selection.durlydown);
        
data_meta.selection.prevendtime(1) = NaT;
data_meta.selection.prevendtime(2:end) = data_meta.selection.endtime(1:end-1); 
data_meta.selection.durstanding = (datenum(data_meta.selection.endtime) - ...
                                  datenum(data_meta.selection.prevendtime))*3600;
data_meta.selection.durstanding(data_meta.selection.durstanding <0)=NaN;

ind = find(data_meta.selection.durstanding <10); % 16

% per day max number of bouts with groupsummary
indx = unique(data_meta.selection(:,[10 4]),'rows');
indx.index(:,1) = (1:height(indx));
data_meta.selection = sortrows(innerjoin(data_meta.selection,indx,...
                               'Keys',{'cow','bDat'}),{'cow','time'});


grsum = groupsummary(data_meta.selection,'index');
max(grsum.GroupCount) % 1 with 19 bouts, 1 with 16 bouts. Threshold = 15 bouts = 2*15 changes




