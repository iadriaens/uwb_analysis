%% S14_categorization
%--------------------------------------------------------------------------
% created by @adria036 (ines adriaens)
%         on January 4, 2022
%   
%--------------------------------------------------------------------------
% this part implements the categorization of segments based on 
%    - level,
%    - variability, 
%    - outliers (statistical properties),
%    - direction of change,
%    - location (slatted flooring or not), 
%    - agreements across changes detected,
%    - missing data
% Categories included are 'lying', 'not lying' and 'uncertain'.
%--------------------------------------------------------------------------

clear variables
close all
clc

%% set filepaths and constants and load data

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
% load data
load([init_.resdir 'D3_sdata_IQ_UWB.mat'])
load([init_.resdir 'D2_data_meta.mat'])
load([init_.resdir 'D6all_cpts_res_cat.mat'])

% delete cow 280 and 495 (no iceQube data) 
sdata = rmfield(sdata,'cow_280');
sdata = rmfield(sdata,'cow_495');

% reference date for numeric time
refdate = datenum(2019,7,3);

%% calculate statistical properties, add changepoints to dataset

% cows
cows = fieldnames(cptsr);

for i = 1:length(cows) % all cows with results
    
    
    T1=1;    % count cpts for cd
    T2=1;   % count cpts for z

    
    % all days for this cow
    days = fieldnames(cptsr.(cows{i}));
    for j = 1:length(days)
        % ref day numeric
        day = days{j}; day = str2double(day(5:end)); % num day (~refday)
        
        % select and store/split data
        data.(cows{i}).(days{j}) = sdata.(cows{i})(:,[2 3 7 13 17 18 15]);                                      
        data.(cows{i}).(days{j})(isnan(data.(cows{i}).(days{j}).avg_z_sm) | ...
                                 isnan(data.(cows{i}).(days{j}).centerdist),:) = [];
        data.(cows{i}).(days{j}).numdate = ...
               datenum(data.(cows{i}).(days{j}).date)-refdate; % numeric date
        data.(cows{i}).(days{j}) = data.(cows{i}).(days{j})(...
               floor(data.(cows{i}).(days{j}).numdate) == day,:);
        
           
           
        %------------------------------- Z -------------------------------

        % changepoints detected for z
        rzdata = array2table(cptsr.(cows{i}).(days{j}).ipoints,...
                            'VariableNames',{'istart','iend','avg','std'});
        
        % add additional statistical/categorization parameters:
        %       1. difference in level
        %       2. outliers
        %       3. std without outliers (90% CI?) Q10-90?
        %       4. missing data/gapsize 
        %       5. length of segment
        rzdata.avgdif(1) = NaN;
        rzdata.avgdif(2:end) = abs(diff(rzdata.avg)); % level difference
        
        % calculate statistical proporties
        for l = 1:height(rzdata)
            istart = rzdata.istart(l);
            iend = rzdata.iend(l);
            segdata = data.(cows{i}).(days{j})(istart:iend,:);
            segdata.isoutlier = isoutlier(segdata.avg_z_sm,'mean');
            segdata.timegap(1) = 0; 
            segdata.timegap(2:end) = diff(segdata.time_in_h*3600);
            tstart = segdata.time_in_h(1)*3600;
            tend = segdata.time_in_h(end)*3600;
            
            % in slatted in 90% of the time of the segment
            rzdata.inslatted(l) = double(...
                       sum(segdata.inslatted) ./ height(segdata) > 0.9);
            % statistical properties
            rzdata.quantrange(l) = quantile(segdata.avg_z_sm,0.95) - ...
                                   quantile(segdata.avg_z_sm,0.05);
            rzdata.range(l) = range(segdata.avg_z_sm);
            rzdata.seglength(l) = tend - tstart;
            rzdata.maxgapsize(l) = max(segdata.timegap);
            rzdata.gappercent(l) = (sum(segdata.timegap(segdata.timegap > 180)) ./ ...
                                   (tend-tstart))*100;
            rzdata.outlpercent(l) = (sum(segdata.isoutlier)./ ...
                                   rzdata.seglength(l))*100;
            rzdata.avgoutl(l) = nanmean(segdata.avg_z_sm(segdata.isoutlier == 0)); 
            rzdata.stdoutl(l) = nanstd(segdata.avg_z_sm(segdata.isoutlier == 0)); 
            % dates numeric
            rzdata.numdatestart(l) = data.(cows{i}).(days{j}).numdate(...
                                            rzdata.istart(l)); % start
            rzdata.numdateend(l) = data.(cows{i}).(days{j}).numdate(...
                                            rzdata.iend(l)); % end
            % check data for clustering
            rzdata.checktruth(l) = (sum(segdata.islying) ./ height(segdata))*100;
            rzdata.checktruth_bin(l) = double(rzdata.checktruth(l) > 85);
        end
        % avg distance
        rzdata.avgdifoutl(1) = NaN;
        rzdata.avgdifoutl(2:end) = abs(diff(rzdata.avgoutl)); % level difference
        rzdata.nextseggap(:,1) = NaN;
        rzdata.nextseggap(1:end-1) = rzdata.gappercent(2:end);
        
        % add that it's a changepoint to data
        data.(cows{i}).(days{j}).ischangeZ(1) = T2;
        data.(cows{i}).(days{j}).ischangeZ(rzdata.iend) = ...
                                    (T2+1:T2+height(rzdata))';
        fie = ['z_' days{j}];
        data.(cows{i}).(fie) = rzdata;
        T2 = T2+height(rzdata);
        
        clear abs iend istart T tend tstart
        
        %------------------------------- CD -------------------------------
         
        % changepoints detected for cd
        rcddata = array2table(cptsr.(cows{i}).(days{j}).ipointsCD,...
                            'VariableNames',{'istart','iend','avg','std'});
        
        % add additional statistical/categorization parameters:
        %       1. difference in level
        %       2. outliers
        %       3. std without outliers (90% CI?) Q10-90?
        %       4. missing data/gapsize 
        %       5. length of segment
        rcddata.avgdif(1) = NaN;
        rcddata.avgdif(2:end) = abs(diff(rcddata.avg)); % level difference
        
        % calculate statistical proporties
        for l = 1:height(rcddata)
            istart = rcddata.istart(l);
            iend = rcddata.iend(l);
            segdata = data.(cows{i}).(days{j})(istart:iend,:);
            segdata.isoutlier = isoutlier(segdata.centerdist,'mean');
            segdata.timegap(1) = 0; 
            segdata.timegap(2:end) = diff(segdata.time_in_h*3600);
            tstart = segdata.time_in_h(1)*3600;
            tend = segdata.time_in_h(end)*3600;
            
            % in slatted in 90% of the time of the segment
            rcddata.inslatted(l) = double(...
                       sum(segdata.inslatted) ./ height(segdata) > 0.9);
            % statistical properties
            rcddata.quantrange(l) = quantile(segdata.centerdist,0.95) - ...
                                   quantile(segdata.centerdist,0.05);
            rcddata.range(l) = range(segdata.centerdist);
            rcddata.seglength(l) = tend - tstart;
            rcddata.maxgapsize(l) = max(segdata.timegap);
            rcddata.gappercent(l) = (sum(segdata.timegap(segdata.timegap > 180)) ./ ...
                                   (tend-tstart))*100;
            rcddata.outlpercent(l) = (sum(segdata.isoutlier)./ ...
                                   rcddata.seglength(l))*100;
            rcddata.avgoutl(l) = nanmean(segdata.centerdist(segdata.isoutlier == 0)); 
            rcddata.stdoutl(l) = nanstd(segdata.centerdist(segdata.isoutlier == 0)); 
            % dates numeric
            rcddata.numdatestart(l) = data.(cows{i}).(days{j}).numdate(...
                                            rcddata.istart(l)); % start
            rcddata.numdateend(l) = data.(cows{i}).(days{j}).numdate(...
                                            rcddata.iend(l)); % end
            % check data for clustering
            rcddata.checktruth(l) = (sum(segdata.islying) ./ height(segdata))*100;
            rcddata.checktruth_bin(l) = double(rcddata.checktruth(l) > 85);
        end
        % avg distance
        rcddata.avgdifoutl(1) = NaN;
        rcddata.avgdifoutl(2:end) = abs(diff(rcddata.avgoutl)); % level difference
        rcddata.nextseggap(:,1) = NaN;
        rcddata.nextseggap(1:end-1) = rcddata.gappercent(2:end);
        
        % add that it's a changepoint to data
        data.(cows{i}).(days{j}).ischangeCD(1) = T1;
        data.(cows{i}).(days{j}).ischangeCD(rcddata.iend) = ...
                                    (T1+1:T1+height(rcddata))';
        fie = ['cd_' days{j}];
        data.(cows{i}).(fie) = rcddata;
        T1 = T1+height(rcddata);
    end
end

clear i iend istart j l T1 T2 tend tstart rcddata rzdata refdate day fie segdata


%% combined data and clustering

% cpts z + properties
cows = fieldnames(cptsr);

for i = 1:length(cows)
    
    cpts.(cows{i}).cpz = [];
    cpts.(cows{i}).cpcd = [];
    
    days = fieldnames(cptsr.(cows{i}));
    
    for j = 1:length(days)
        
        % add to previous set
        fieldname_= ['z_' days{j}];
        cpts.(cows{i}).cpz = [cpts.(cows{i}).cpz;data.(cows{i}).(fieldname_)];
        fieldname_= ['cd_' days{j}];
        cpts.(cows{i}).cpcd = [cpts.(cows{i}).cpcd;data.(cows{i}).(fieldname_)];
    end
end

% visualize statistical properties per animal
%   avg             3
%   std             4
%   avgdiff         5
%   inslatted?      6
%   quantrange      7
%   range           8
%   seglength       9
%   max gapsize     10
%   gappercent      11
%   outlpercent     12
%   avgoutl         13
%   stdoutl         14
%   avgdifoutl      19
%   nextseggap      20


fields_ = fieldnames(data);
for i = 1:length(fields_)
    
    % prepare figure;
    figure('Units','centimeters','OuterPosition',[105 -5 30 22])
    cowdata = cpts.(fields_{i}).cpz(:,[3 4 5 6 7 8 9 10 11 12 13 14 19 20]);
    corrplot(cowdata{:,:},'varNames',cowdata.Properties.VariableNames)
    
    subplot 
    
    
end


% different possibilities for training a classification based on the
% different variables 
%       PCA
%       clustering
%
%   I have 
% Z
pcadata = normalize(cpts.cow_90.cpz{:,[13 14 5 7 8 9 10 11 12 ]});
[coeff,score,latent,tsquared,explained] = pca(pcadata);

figure; hold on;
scatter3(score(cpts.cow_90.cpz.checktruth_bin == 1,1),score(cpts.cow_90.cpz.checktruth_bin == 1,2),score(cpts.cow_90.cpz.checktruth_bin == 1,3),'green')
scatter3(score(cpts.cow_90.cpz.checktruth_bin == 0,1),score(cpts.cow_90.cpz.checktruth_bin == 0,2),score(cpts.cow_90.cpz.checktruth_bin == 0,3),'blue')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
zlabel('3rd Principal Component')

%CD
pcadata = normalize(cpts.cow_90.cpcd{:,[13 14 5 7 8 9 10 11 12 ]});
[coeff,score,latent,tsquared,explained] = pca(pcadata);

figure; hold on;
scatter3(score(cpts.cow_90.cpcd.checktruth_bin == 1,1),score(cpts.cow_90.cpcd.checktruth_bin == 1,2),score(cpts.cow_90.cpcd.checktruth_bin == 1,3),'green')
scatter3(score(cpts.cow_90.cpcd.checktruth_bin == 0,1),score(cpts.cow_90.cpcd.checktruth_bin == 0,2),score(cpts.cow_90.cpcd.checktruth_bin == 0,3),'blue')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
zlabel('3rd Principal Component')

% https://nl.mathworks.com/help/stats/pca.html#bti6r20-1

% lying ground truth (data_ld)




% save([init_.resdir 'D4_dataday.mat'],'data', '-v7.3')
