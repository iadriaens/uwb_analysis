%% S3_missingdata
% this script explores, summarizes and visualizes the missing data
% 
% STEP 0: settings and data loading 
% STEP 1: average number of missing records per cow per day
%
%

% load data > data, weatherData, cowinfo
% 
tic
S2_loaddata
toc

% set results path
paths.resdir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];


%% Average number of missing records per cow per day
% fields in data
fields_ = fieldnames(data);

% summarize and plot position in function of time
close all
h = [];
for i = 1:size(fields_,1)
        
    % calculate summary stats per day
    summary.day.(fields_{i}) = groupsummary(data.(fields_{i}),"object_name",...
             ["mean","std","nummissing","nnz"],["avg_x","avg_y","avg_z"]);
    
    % summary matrix of missing values, one col/cow
    if i == 1
        summary.miss = summary.day.(fields_{i})(:,1);
        summary.miss.PercMissing = summary.day.(fields_{i}).nummissing_avg_x ./ ...
                                  summary.day.(fields_{i}).GroupCount*100;
    else
        percmiss = summary.day.(fields_{i})(:,1);
        percmiss.PercMissing = summary.day.(fields_{i}).nummissing_avg_x ./ ...
                                  summary.day.(fields_{i}).GroupCount*100;
        summary.miss = outerjoin(summary.miss,percmiss,'Keys','object_name','MergeKeys',1);
    end
end

% boxplots of % missing
h = figure('Units','centimeters','OuterPosition',[1 1 30 15]);
box on; 
hi = boxplot(summary.miss{:,2:end}','BoxStyle','filled',...
                                    'Symbol','ro',...
                                    'OutlierSize',4.5 ...
                                    );
get(gca)
ylabel('% missing values')
title('Average % missing values per animal per day in July 2019')
xticklabels(num2str(summary.miss{:,1}))
xtickangle(45)
xlabel('CowID')

hold on
plot([0,length(summary.miss{:,1})+1],...
      [nanmean(reshape(summary.miss{:,2:end},...
       size(summary.miss{:,2:end},1) *...
       size(summary.miss{:,2:end},2),1)), ...
       nanmean(reshape(summary.miss{:,2:end},...
       size(summary.miss{:,2:end},1) *...
       size(summary.miss{:,2:end},2),1)) ],'r:','LineWidth',2)

% save
saveas(h, [paths.resdir 'FIG_missingData.png'])   


%% Overall visualisation timing over days
% fields in data
fields_ = fieldnames(data);

close all
Tfig = 1;
for i = 1:size(fields_,1)
    
    if rem(i,4) == 1 % start new figure if remainder of i = 1
        h = figure('Units','centimeters','OuterPosition',[1 1 35 20]);
        T = 1;
        Tfig = Tfig+1;
    end
    % plot 4 days per figure
    subplot(2,2,T); hold on; box on;
    field_ = fields_{i};
        xlabel('Time of the day (hours)'); xlim([0 24])
        ylabel('Proportion of missing values [%]'); ylim([0 0.03])
        title(['Date = ' field_])    % find all data with y > 11 and make histogram

    ind = find(isnan(data.(field_).avg_y)); % find missing data
    hi.miss = histogram(data.(field_).timeH(ind),24*4,'Normalization','probability');
        hi.miss.FaceColor = [255/255 153/155 0/255];
    legend({'Missing data'},'AutoUpdate','off','Location','northwest')
    % plot approximate milking times
    plot([6 6],[0 0.05],'r-.','LineWidth',1.5) % milking AM
    plot([8 8],[0 0.05],'r-.','LineWidth',1.5) % milking AM
    plot([16 16],[0 0.05],'r-.','LineWidth',1.5) % milking PM
    plot([18 18],[0 0.05],'r-.','LineWidth',1.5) % milking PM
    hold off
    if T == 4 || i == size(fields_,1)
        saveas(h,[paths.resdir 'FIG_MissingHist_' ...
                        num2str(Tfig) '.png']) 
    end
    T=T+1;    
end


%% Visualisation of gapsize in function of timing of the day
% gaps between successive values
cte.makefig = 1;  % set to 1 to make/save figures

% fields in data
fields_ = fieldnames(data);

% calculate gapsize
close all
summary.gaptiming = array2table(ones(0,2),'VariableNames',{'time','gapsize'});
for i = 1:size(fields_,1)
    cows = unique(data.(fields_{i}).object_name);
    for j = 1:length(cows)
        ind = find(data.(fields_{i}).object_name == cows(j) & ...
                                    ~isnan(data.(fields_{i}).avg_x));
        
        gapsize = diff([floor(datenum(data.(fields_{i}).date(1)));...
                         datenum(data.(fields_{i}).date(ind))])*24*60*60;
        
        % add gapsize to data
        data.(fields_{i}).gapsize(data.(fields_{i}).object_name == cows(j),1) = NaN;
        data.(fields_{i}).gapsize(ind) = round(gapsize);
    end
    % delete gaps of nseconds length
    data.(fields_{i}).gapsize(data.(fields_{i}).gapsize <= cte.nseconds) = NaN;
    
    % store in single dataset to plot over all days
    summary.gaptiming = [summary.gaptiming;...
                        %data.(fields_{i}).date(~isnan(data.(fields_{i}).gapsize)) ...
                        table(datenum(data.(fields_{i}).date(~isnan(data.(fields_{i}).gapsize))) - ...
                        floor(datenum(data.(fields_{i}).date(~isnan(data.(fields_{i}).gapsize)))),'VariableNames',{'time'}) ...
                        table(data.(fields_{i}).gapsize(~isnan(data.(fields_{i}).gapsize)),'VariableNames',{'gapsize'})];

    if cte.makefig == 1
        % plots per day    
        h = figure('Units','centimeters','OuterPosition',[1 1 35 22]); 
        subplot(2,2,1);hold on; box on;
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x > 21.5);
            plot(24*(datenum(data.(fields_{i}).date(ind))-floor(datenum(data.(fields_{i}).date(ind)))),...
                 data.(fields_{i}).gapsize(ind)/60,'ko','Markersize',3)
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x < 21.5);
            plot(24*(datenum(data.(fields_{i}).date(ind))-floor(datenum(data.(fields_{i}).date(ind)))),...
                 data.(fields_{i}).gapsize(ind)/60,'bs','Markersize',3)
        legend({'deep litter (straw)','synthetic'},'Location','northwest')
        xlim([0 24]);
        title('Duration of gaps in time series data vs. time of the day')
        xlabel('Time (h)')
        ylabel('Duration (min)')

        subplot(2,2,3);hold on; box on
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x > 21.5);
            plot(24*(datenum(data.(fields_{i}).date(ind))-floor(datenum(data.(fields_{i}).date(ind)))),...
                 data.(fields_{i}).gapsize(ind)/60,'ko','Markersize',3)
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x < 21.5);
            plot(24*(datenum(data.(fields_{i}).date(ind))-floor(datenum(data.(fields_{i}).date(ind)))),...
                 data.(fields_{i}).gapsize(ind)/60,'bs','Markersize',3)
        legend({'deep litter (straw)','synthetic'},'Location','northwest')
        ylim([0 0.3*60]);xlim([0 24])
        title('Duration of gaps in time series data vs. time of the day')
        xlabel('Time (h)')
        ylabel('Duration (min)')

        subplot(2,2,2);  
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x > 21.5);
            hi = histogram(data.(fields_{i}).gapsize(ind)/60,0:1:(1.5*60),'Normalization','cdf');
            hi.FaceColor = 'b';         
            hold on;
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x < 21.5);
            hi = histogram(data.(fields_{i}).gapsize(ind)/60,0:1:(1.5*60),'Normalization','cdf');
            hi.FaceColor = 'k'; 
        hold off
        xlim([-1 91])
        ylim([0.4 1.05])
        xlabel('Duration (min)')
        ylabel('Cumulative density')
        title(['Date = ' fields_{i}])

        subplot(2,2,4);  
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x > 21.5);
            hi = histogram(data.(fields_{i}).gapsize(ind)/60,0:1:(1.5*60),'Normalization','cdf');
            hi.FaceColor = 'b';         
        hold on;
        ind = find(~isnan(data.(fields_{i}).gapsize) & data.(fields_{i}).avg_x < 21.5);
            hi = histogram(data.(fields_{i}).gapsize(ind)/60,0:1:(1.5*60),'Normalization','cdf');
            hi.FaceColor = 'k'; 
        hold off
        xlim([-1 30])
        ylim([0.4 1.05])
        xlabel('Duration (min)')
        ylabel('Cumulative density')
        title(['Median duration = ' num2str(round(nanmedian(data.(fields_{i}).gapsize),2)) ' sec'])

        saveas(h,[paths.resdir 'FIG_' 'Missing_duration_' fields_{i} '.png'])
    end
end

% plot all
summary.gaptiming = sortrows(summary.gaptiming,1);
close all
h = figure;
plot(0.5+summary.gaptiming.time*24*4.2083, summary.gaptiming.gapsize/60,...
     'o','MarkerSize',3,...
     'Color',[153/255 0/255 76/255])
xlim([0 102])
ylim([0 10])

% [tst,idx] = unique(summary.gaptiming.time);
% figure;
yyaxis right
boxplot(summary.gaptiming.gapsize/60,round(summary.gaptiming.time,2)*24,...
        'BoxStyle','filled',...
        'Symbol','',...
        'OutlierSize',2.5 ...
        );
xlim([0 102])
ylim([0 10])

yyaxis left
xticks(-0.5:4.3:102)
xticklabels(0:23)
xlabel('Time (h)')
ylabel('Duration (min)')
title('Duration of gaps (missing data) in function of time of the day')
saveas(h,[paths.resdir 'FIG_' 'Missing_duration_SUMMARY_time' '.png'])
saveas(h,[paths.resdir 'FIG_' 'Missing_duration_SUMMARY_time' '.fig'])

%% Visualisation of # missing data  based on previous (x,y) location

% fields
fields_ = fieldnames(data);
try
    summary = rmfield(summary,'misspos');
    summary = rmfield(summary,'missposno');
end
for i = 1:size(fields_,1)
    
    % round x and y data on 50 cm (=1/2m)
    data.(fields_{i}).ravg_x(:,1) = round(data.(fields_{i}).avg_x(:,1)*2)/2;
    data.(fields_{i}).ravg_y(:,1) = round(data.(fields_{i}).avg_y(:,1)*2)/2;
    
    % construct dataset
    tic
    xcol = 1;
    for x = 0:0.5:43
        yrow = 1;
        for y = 0:0.5:14
            
            ind = find(~isnan(data.(fields_{i}).gapsize) & ...
                       data.(fields_{i}).ravg_x == x & ...
                       data.(fields_{i}).ravg_y == y);  % missing data
            summary.misspos.(fields_{i})(xcol+1,yrow+1) = nansum(data.(fields_{i}).gapsize(ind));
            summary.missposno.(fields_{i})(xcol+1,yrow+1) = length(ind); % no of missing at that position
            
            yrow = yrow+1;
        end
        xcol = xcol+1;
    end
    toc

    % set x and y values
    summary.misspos.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.missposno.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.misspos.(fields_{i})(1,:) = [0,0:0.5:14];
    summary.missposno.(fields_{i})(1,:) = [0,0:0.5:14];
    
end

close all
for i = 1:size(fields_,1)
    % plot
    h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.missposno.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.missposno.(fields_{i})(2:end,1),... x values = 216
            summary.missposno.(fields_{i})(1,2:end),... y values = 71
            (summary.missposno.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title(['Number of missing data (~interruptions), in %, date = ' fields_{i}])
    subplot(2,1,2);
    percref = sum(sum(summary.misspos.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.misspos.(fields_{i})(2:end,1),... x values = 216
            summary.misspos.(fields_{i})(1,2:end),... y values = 71
            (summary.misspos.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title(['Total duration of missing data, in %, date = ' fields_{i}])
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing_' fields_{i} '.png'])
    close all
       
end

% summarize excluding 1st of july > too much missing data in beginning of
% the day
for i = 2:size(fields_,1)
    % summary misspos => sum of all 
    if i == 2 
        summary.misspos.alldays = summary.misspos.(fields_{i});
        summary.missposno.alldays = summary.missposno.(fields_{i});
    else
        summary.misspos.alldays = summary.misspos.alldays + ...
                                  summary.misspos.(fields_{i});
        summary.missposno.alldays = summary.missposno.alldays + ...
                                  summary.missposno.(fields_{i});
    end
end
% add coordinates
summary.misspos.alldays(:,1) = [0;(0:0.5:43)'];
summary.missposno.alldays(:,1) = [0;(0:0.5:43)'];
summary.misspos.alldays(1,:) = [0,0:0.5:14];
summary.missposno.alldays(1,:) = [0,0:0.5:14];

% make figure
h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.missposno.alldays(2:end,2:end)));
    hi = heatmap(summary.missposno.alldays(2:end,1),... x values = 216
            summary.missposno.alldays(1,2:end),... y values = 71
            (summary.missposno.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title('Number of missing records (~interruptions), all days')
subplot(2,1,2);
    percref = sum(sum(summary.misspos.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos.alldays(2:end,1),... x values = 216
            summary.misspos.alldays(1,2:end),... y values = 71
            (summary.misspos.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title('Total duration of missing data, in %, all days ')
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing_' fields_{i} '.png'])
    close all

%% visualisation of missing data gapsize < 2'




% fields
fields_ = fieldnames(data);
try
    summary = rmfield(summary,'misspos2');
    summary = rmfield(summary,'misspos2no');
end
for i = 1:size(fields_,1)
    
    % round x and y data on 50 cm (=1/2m)
    data.(fields_{i}).ravg_x2(:,1) = round(data.(fields_{i}).avg_x(:,1)*2)/2;
    data.(fields_{i}).ravg_y2(:,1) = round(data.(fields_{i}).avg_y(:,1)*2)/2;
    
    % construct dataset
    tic
    xcol = 1;
    for x = 0:0.5:43
        yrow = 1;
        for y = 0:0.5:14
            
            ind = find(~isnan(data.(fields_{i}).gapsize) & ...
                       data.(fields_{i}).gapsize < 120 & ...
                       data.(fields_{i}).ravg_x2 == x & ...
                       data.(fields_{i}).ravg_y2 == y);  % missing data
            summary.misspos2.(fields_{i})(xcol+1,yrow+1) = nansum(data.(fields_{i}).gapsize(ind));
            summary.misspos2no.(fields_{i})(xcol+1,yrow+1) = length(ind); % no of missing at that position
            
            yrow = yrow+1;
        end
        xcol = xcol+1;
    end
    toc

    % set x and y values
    summary.misspos2.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.misspos2no.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.misspos2.(fields_{i})(1,:) = [0,0:0.5:14];
    summary.misspos2no.(fields_{i})(1,:) = [0,0:0.5:14];
    
end

close all
for i = 1:size(fields_,1)
    % plot
    h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.misspos2no.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.misspos2no.(fields_{i})(2:end,1),... x values = 216
            summary.misspos2no.(fields_{i})(1,2:end),... y values = 71
            (summary.misspos2no.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title(['Number of missing records (~interruptions) <2 min, in %, date = ' fields_{i}])
    subplot(2,1,2);
    percref = sum(sum(summary.misspos2.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.misspos2.(fields_{i})(2:end,1),... x values = 216
            summary.misspos2.(fields_{i})(1,2:end),... y values = 71
            (summary.misspos2.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title(['Total duration of missing data <2 min, in %, date = ' fields_{i}])
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing2min_' fields_{i} '.png'])
    close all
       
end

% summarize excluding 1st of july > too much missing data in beginning of
% the day
for i = 2:size(fields_,1)
    % summary misspos2 => sum of all 
    if i == 2 
        summary.misspos2.alldays = summary.misspos2.(fields_{i});
        summary.misspos2no.alldays = summary.misspos2no.(fields_{i});
    else
        summary.misspos2.alldays = summary.misspos2.alldays + ...
                                  summary.misspos2.(fields_{i});
        summary.misspos2no.alldays = summary.misspos2no.alldays + ...
                                  summary.misspos2no.(fields_{i});
    end
end
% add coordinates
summary.misspos2.alldays(:,1) = [0;(0:0.5:43)'];
summary.misspos2no.alldays(:,1) = [0;(0:0.5:43)'];
summary.misspos2.alldays(1,:) = [0,0:0.5:14];
summary.misspos2no.alldays(1,:) = [0,0:0.5:14];

% make figure
h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.misspos2no.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos2no.alldays(2:end,1),... x values = 216
            summary.misspos2no.alldays(1,2:end),... y values = 71
            (summary.misspos2no.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title('Number of missing records (~interruptions), gapsize <2min, all days')
subplot(2,1,2);
    percref = sum(sum(summary.misspos2.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos2.alldays(2:end,1),... x values = 216
            summary.misspos2.alldays(1,2:end),... y values = 71
            (summary.misspos2.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title('Total duration of missing data, gapsize <2min, in %, all days ')
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing2min_' fields_{i} '.png'])
    close all



% aan voederbakken? Aan voerhek?
% distance markov chains voor correctie distnace instability

%% Position BEFORE gap
for i = 1:size(fields_,1)
    cows = unique(data.(fields_{i}).object_name);
    for j = 1:length(cows)
        ind = find(data.(fields_{i}).object_name == cows(j) & ...
                                    ~isnan(data.(fields_{i}).avg_x));
        
        gapsize = diff(datenum(data.(fields_{i}).date(ind)))*24*60*60;
        
        % add gapsize to data
        data.(fields_{i}).gapsize2(data.(fields_{i}).object_name == cows(j),1) = NaN;
        data.(fields_{i}).gapsize2(ind(1:end-1)) = round(gapsize);
    end
    % delete gaps of nseconds length
    data.(fields_{i}).gapsize2(data.(fields_{i}).gapsize2 <= cte.nseconds) = NaN;
end


% fields
fields_ = fieldnames(data);
try
    summary = rmfield(summary,'misspos3');
    summary = rmfield(summary,'misspos3no');
end
for i = 1:size(fields_,1)
    
    % round x and y data on 50 cm (=1/2m)
    data.(fields_{i}).ravg_x3(:,1) = round(data.(fields_{i}).avg_x(:,1)*2)/2;
    data.(fields_{i}).ravg_y3(:,1) = round(data.(fields_{i}).avg_y(:,1)*2)/2;
    
    % construct dataset
    tic
    xcol = 1;
    for x = 0:0.5:43
        yrow = 1;
        for y = 0:0.5:14
            
            ind = find(~isnan(data.(fields_{i}).gapsize2) & ...
                       data.(fields_{i}).gapsize2 < 120 & ...
                       data.(fields_{i}).ravg_x3 == x & ...
                       data.(fields_{i}).ravg_y3 == y);  % missing data
            summary.misspos3.(fields_{i})(xcol+1,yrow+1) = nansum(data.(fields_{i}).gapsize2(ind));
            summary.misspos3no.(fields_{i})(xcol+1,yrow+1) = length(ind); % no of missing at that position
            
            yrow = yrow+1;
        end
        xcol = xcol+1;
    end
    toc

    % set x and y values
    summary.misspos3.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.misspos3no.(fields_{i})(:,1) = [0;(0:0.5:43)'];
    summary.misspos3.(fields_{i})(1,:) = [0,0:0.5:14];
    summary.misspos3no.(fields_{i})(1,:) = [0,0:0.5:14];
    
end

close all
for i = 1:size(fields_,1)
    % plot
    h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.misspos3no.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.misspos3no.(fields_{i})(2:end,1),... x values = 216
            summary.misspos3no.(fields_{i})(1,2:end),... y values = 71
            (summary.misspos3no.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title(['Number of missing records (~interruptions) <2 min, in %, startgap, date = ' fields_{i}])
    subplot(2,1,2);
    percref = sum(sum(summary.misspos3.(fields_{i})(2:end,2:end)));
    hi = heatmap(summary.misspos3.(fields_{i})(2:end,1),... x values = 216
            summary.misspos3.(fields_{i})(1,2:end),... y values = 71
            (summary.misspos3.(fields_{i})(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title(['Total duration of missing data <2 min, in %, startgap, date = ' fields_{i}])
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing2min_startgap_' fields_{i} '.png'])
    close all
       
end

% summarize excluding 1st of july > too much missing data in beginning of
% the day
for i = 2:size(fields_,1)
    % summary misspos2 => sum of all 
    if i == 2 
        summary.misspos3.alldays = summary.misspos3.(fields_{i});
        summary.misspos3no.alldays = summary.misspos3no.(fields_{i});
    else
        summary.misspos3.alldays = summary.misspos3.alldays + ...
                                  summary.misspos3.(fields_{i});
        summary.misspos3no.alldays = summary.misspos3no.alldays + ...
                                  summary.misspos3no.(fields_{i});
    end
end
% add coordinates
summary.misspos3.alldays(:,1) = [0;(0:0.5:43)'];
summary.misspos3no.alldays(:,1) = [0;(0:0.5:43)'];
summary.misspos3.alldays(1,:) = [0,0:0.5:14];
summary.misspos3no.alldays(1,:) = [0,0:0.5:14];

% make figure
h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.misspos3no.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos3no.alldays(2:end,1),... x values = 216
            summary.misspos3no.alldays(1,2:end),... y values = 71
            (summary.misspos3no.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title('Number of missing records (~interruptions), gapsize <2min, start gap, all days')
subplot(2,1,2);
    percref = sum(sum(summary.misspos3.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos3.alldays(2:end,1),... x values = 216
            summary.misspos3.alldays(1,2:end),... y values = 71
            (summary.misspos3.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.Colormap = jet;
        title('Total duration of missing data, gapsize <2min, start gap, in %, all days ')
        hi.FontSize = 8;
        
    saveas(h,[paths.resdir 'FIG_heatmapmissing2min_startgap_' fields_{i} '.png'])
    close all

    
    h = figure('Units','centimeters','OuterPosition',[1 1 30 22]); 
    subplot(2,1,1);
    percref = sum(sum(summary.misspos3no.alldays(2:end,2:end)));
    hi = heatmap(summary.misspos3no.alldays(2:end,1),... x values = 216
            summary.misspos3no.alldays(1,2:end),... y values = 71
            (summary.misspos3no.alldays(2:end,2:end)./percref.*100)');
        hi.GridVisible = 0;
    	hi.XDisplayData = 0:0.5:43;
        hi.YDisplayData = 0:0.5:14;
        hi.FontSize = 8;
        hi.Colormap = jet;
        title('Number of missing records (~interruptions), gapsize <2min, start gap, all days')




%% functions

% ==================== MAKEBARNFIG ======================= %
% Figure of barn based on a) figure "Plattegrond Meetstal.pdf" and
% b) Wijbrand's explanation > basic design is stored in PPT_barn_axes.ppt
function h = makebarnfig(figsize)
% create a figure with input = figsize, that has barn as axes and positions
% the data such that we can interpret cow positions
% figsize is a measure in centimeters that indicates the width (=x-axis)
% and corresponds to the 43m width of the barn
% the corresponding height = 14m and the rule of three gives us that the
% height of the figure should be 14*figsize/43
% outersize is fixed on innersize + 2*1.5cm
% 
% try
%     close h
% catch
% end
    innerpos = [1+1.5 1+1.5 figsize 14/43*figsize];
    outerpos = [1 1 figsize+3 14/43*figsize+3];

    h = figure('Units','centimeters',...
               'OuterPosition',outerpos,...
               'InnerPosition',innerpos,...
               'Color',[0.1 0.1 0.1]...
               );
    grid on; hold on;
    plot([0 43],[0 0],'Color','b','LineWidth',1.5)
    plot([0 0],[0 14],'Color','b','LineWidth',1.5)
    plot(43,0,'>','MarkerSize',6,'Color','b','MarkerFaceColor','b')
    plot(0,14,'^','MarkerSize',6,'Color','b','MarkerFaceColor','b')
    axis tight
    box on
    ax = gca;
    ax.Units = 'centimeters';
    text(-0.2, -0.6,'(x,y)=(0,0)','Color',[0.7 0.7 0.7]) 
    xlabel('x position','Color',[0.7 0.7 0.7])
    ylabel('y position','Color',[0.7 0.7 0.7])
    
    % plot barn zones
    plot([21.6 21.6],[0 14],'k','LineWidth',2)
    plot([0 43],[14-3.5 14-3.5],'Color',[2/255 156/255 121/255],'LineWidth',2)
    plot([0 43],[14 14],'r-.','LineWidth',1)
end