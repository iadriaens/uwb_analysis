%% S8_cpnts
% visualisation of the lying bouts + changepoints for development purposes

clear variables
close all
clc

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
% load data
load([init_.resdir 'D3_sdata_IQ_UWB.mat'])
load([init_.resdir 'D2_data_meta.mat'])


%% set constants
% delete cow 280
sdata = rmfield(sdata,'cow_280');
fields_ = fieldnames(sdata);
for f = 1:length(fields_)
% set cow
cow = fields_{f};
% cow = 'cow_99';

% set technique
cpts.Zstat = 'mean';       % technique based on which cpts are found for z
cpts.CDstat = 'mean';      % technique based on which cpts are found for CD

% set settings
cpts.MinDistance = 600;     % 10 min 
cpts.NumChanges = 30;        % number of changes per hour of data

% figure settings
cpts.Units = 'centimeters';     % set units
cpts.figPos = [1 1 30 22];      % set position


%% select data and plot lying bouts
% select data of one day per cow

clear data
close all
                                     
% data contains "z" and "CD" and runs from first lying to last lying bout
data = sdata.(cow)(:,[2 3 7 13 17 18 15]);                                      
  
% reference date for numeric time
refdate = datenum(2019,7,3);


data2 = data(~isnan(data.avg_z_sm) & ~isnan(data.centerdist),:);
data2.numdate = datenum(data2.date)-refdate;


%%
%---------------------------------------------------------------------
% changepoint analysis - ATTEMPT 2 - take full day independent of segment
% length and a fixed number of max changepoints
%       -->  evaluate validity of detection only
%            if there is a getup or liedown event
days = unique(floor(data2.numdate));
for i = 1:length(days)
    % select daydata 
    data_day = data2(floor(data2.numdate) == days(i),:);
    
    
    
    if height(data_day) > 12*60*60
        % prepare results
        day = sprintf('day_%d',days(i));
                
        % changepoints avg_z
        [cptsr.(cow).(day).ipoints, cptsr.(cow).(day).resid] = ...
             findchangepts(data_day.avg_z_sm,...
                           'Statistic',cpts.Zstat,...
                           'MinDistance',cpts.MinDistance,...
                           'MaxNumChanges',30);
        cptsr.(cow).(day).ipoints = [[1;...
                    cptsr.(cow).(day).ipoints(1:end-1);...
                    cptsr.(cow).(day).ipoints(end)],...
                    [cptsr.(cow).(day).ipoints;height(data_day)]];

        % calculate mean value + std
        for j = 1:size(cptsr.(cow).(day).ipoints,1)
            cptsr.(cow).(day).ipoints(j,3) = ...
                mean(data_day.avg_z_sm(...
                        cptsr.(cow).(day).ipoints(j,1):...
                        cptsr.(cow).(day).ipoints(j,2)));
            cptsr.(cow).(day).ipoints(j,4) = ...
                std(data_day.avg_z_sm(...
                        cptsr.(cow).(day).ipoints(j,1):...
                        cptsr.(cow).(day).ipoints(j,2)));
        end
        
        % changepoints cd
        [cptsr.(cow).(day).ipointsCD, cptsr.(cow).(day).residCD] = ...
            findchangepts(data_day.centerdist,...
                           'Statistic',cpts.CDstat,...
                           'MinDistance',cpts.MinDistance,...
                           'MaxNumChanges',30);
        cptsr.(cow).(day).ipointsCD = [[1;...
                  cptsr.(cow).(day).ipointsCD(1:end-1);...
                  cptsr.(cow).(day).ipointsCD(end)],...
                  [cptsr.(cow).(day).ipointsCD;height(data_day)]];

        % calculate mean value + std
        for j = 1:size(cptsr.(cow).(day).ipointsCD,1)
            cptsr.(cow).(day).ipointsCD(j,3) = mean(data_day.centerdist(...
                                cptsr.(cow).(day).ipointsCD(j,1):...
                                cptsr.(cow).(day).ipointsCD(j,2)));
            cptsr.(cow).(day).ipointsCD(j,4) = std(data_day.centerdist(...
                                cptsr.(cow).(day).ipointsCD(j,1):...
                                cptsr.(cow).(day).ipointsCD(j,2)));
        end
    end
end
end
% save results
save([init_.resdir 'D5fin_cpts_res.mat'],'cptsr','cpts')

% clear variables
clear ans cow data data_day daat_ld day days downevents f j i h ind
clear x y upevents seglist data2 k



%% Postprocessing - filter and combine results
% >>> SENSITIVITY

% >>> SPECIFICITY
%       - change = in segment
%       - difference in mean value (~std of the signal)
%       - CD: difference at changepoints in level > 0.25m
%       - CD: difference at changepoints in std > 
% load results
% load([init_.resdir 'D4_cpts_res.mat'])

% cows
cows = fieldnames(cptsr);

for i = 1:length(fields_) % all cows with results
    T=1;    % count cpts for cd
    T2=1;   % count cpts for z
    % all days for this cow
    days = fieldnames(cptsr.(cows{i}));
      
    close all
    % add to changepoints the information for postprocessing
    for j = 1:length(days)
        % ref day numeric
        day = days{j}; day = str2double(day(5:end)); % num day (~refday)
        
        % select and store/split data
        data.(cows{i}).(days{j}) = sdata.(cows{i})(:,[2 3 7 13 17 18 15]);                                      
        data.(cows{i}).(days{j})(isnan(data.(cows{i}).(days{j}).avg_z_sm) | ...
                                 isnan(data.(cows{i}).(days{j}).centerdist),:) = [];
        data.(cows{i}).(days{j}).numdate = ...
               datenum(data.(cows{i}).(days{j}).date)-refdate; % numeric date
        data.(cows{i}).(days{j}) = data.(cows{i}).(days{j})(...
               floor(data.(cows{i}).(days{j}).numdate) == day,:);

        % load figure and plot
%         k = openfig([init_.resdir 'fig_cpts_' cows{i} '_' days{j} '.fig']);

        % results z
        rzdata = array2table(cptsr.(cows{i}).(days{j}).ipoints,...
                            'VariableNames',{'istart','iend','avg','std'});
        % difference in mean for CD
        rzdata.avgdif(1) = NaN;
        rzdata.avgdif(2:end) = abs(diff(rzdata.avg));

        % calculate mean value + std
        for l = 1:height(rzdata)
            rzdata.avgnew(l) = mean(...
                                data.(cows{i}).(days{j}).avg_z_sm(...
                                    rzdata.istart(l):...
                                    rzdata.iend(l)));
            rzdata.stdnew(l) = std(...
                                data.(cows{i}).(days{j}).avg_z_sm(...
                                    rzdata.istart(l):...
                                    rzdata.iend(l)));
            rzdata.numdatestart(l) = data.(cows{i}).(days{j}).numdate(...
                                            rzdata.istart(l)); % start
            rzdata.numdateend(l) = data.(cows{i}).(days{j}).numdate(...
                                            rzdata.iend(l)); % end
                                
        end
        % add that it's a changepoint to data
        data.(cows{i}).(days{j}).ischangeZ(1) = T2;
        data.(cows{i}).(days{j}).ischangeZ(rzdata.iend) = ...
                                    (T2+1:T2+height(rzdata))';
        % save new changepoints
        fie = ['z_' days{j}];
        data.(cows{i}).(fie) = rzdata;
        T2 = T2+height(rzdata);
                                            
        % results cd
        rcpdata = array2table(cptsr.(cows{i}).(days{j}).ipointsCD,...
                            'VariableNames',{'istart','iend','avg','std'});

        % difference in mean for CD
        rcpdata.avgdif(1) = NaN;
        rcpdata.avgdif(2:end) = abs(diff(rcpdata.avg));

        % calculate mean value + std
        for l = 1:height(rcpdata)
            rcpdata.avgnew(l) = mean(...
                                data.(cows{i}).(days{j}).centerdist(...
                                    rcpdata.istart(l):...
                                    rcpdata.iend(l)));
            rcpdata.stdnew(l) = std(...
                                data.(cows{i}).(days{j}).centerdist(...
                                    rcpdata.istart(l):...
                                    rcpdata.iend(l)));
            rcpdata.numdatestart(l) = data.(cows{i}).(days{j}).numdate(...
                                            rcpdata.istart(l)); % start
            rcpdata.numdateend(l) = data.(cows{i}).(days{j}).numdate(...
                                            rcpdata.iend(l)); % end
                                              
        end
        % add that it's a changepoint to data
        data.(cows{i}).(days{j}).ischangeCD(1) = T;
        data.(cows{i}).(days{j}).ischangeCD(rcpdata.iend) = ...
                                    (T+1:T+height(rcpdata))';
        T = T+height(rcpdata);
                                
        % save new changepoints
        fie = ['cd_' days{j}];
        data.(cows{i}).(fie) = rcpdata;
            
    end
end

save([init_.resdir 'D5fin_dataday.mat'],'data', '-v7.3')
