




x=1:86000; 
data = cdata.cow_90.avg_z_sm;
data2 = log(cdata.cow_90.dist_sm+1);
ind = find(~isnan(data) & ~isnan(data2));
data = data(ind);
data2 = min(0.1*ones(length(ind),1),log(data2(ind)+1));
x = x(ind);




[ipoints, residual] = findchangepts(data,'MaxNumChanges',20,...
                    'Statistic','mean','MinDistance',600);
[ipoints2, residual2] = findchangepts(data2,'MaxNumChanges',20,...
                    'Statistic','std','MinDistance',600);

% find gaps that are too large > overlapping with bouts?



ipoints = [[1;ipoints(1:end-1); ipoints(end)], [ipoints;length(data)]];
ipoints2 = [[1;ipoints2(1:end-1); ipoints2(end)], [ipoints2;length(data2)]];


figure; 
subplot(2,1,1);hold on; box on;
plot(data); axis tight

subplot(2,1,2);hold on; box on;
plot(data2); axis tight


for i = 1:length(ipoints)
    
   ipoints(i,3) = mean(data(ipoints(i,1):ipoints(i,2)));
   ipoints(i,4) = std(data(ipoints(i,1):ipoints(i,2)));

   subplot(2,1,1)
   plot([ipoints(i,2) ipoints(i,2)],[0 2],'b','LineWidth',2)
   plot([ipoints(i,1) ipoints(i,2)],[ipoints(i,3) ipoints(i,3)],'k',...
         'LineWidth',2) 
   
   title(['No. CP z = ' num2str(size(ipoints,1))])
   
end

for i = 1:length(ipoints2)
   ipoints2(i,3) = mean(data2(ipoints2(i,1):ipoints2(i,2)));
   ipoints2(i,4) = std(data2(ipoints2(i,1):ipoints2(i,2)));
   subplot(2,1,2)
   plot([ipoints2(i,2) ipoints2(i,2)],[0 0.1],'b','LineWidth',2)
   plot([ipoints2(i,1) ipoints2(i,2)],[ipoints2(i,3) ipoints2(i,3)],'k',...
         'LineWidth',2) 
     
   title(['No. CP dist = ' num2str(size(ipoints2,1))])
end





%% testjes




x=1:86000; 
data = sdata.cow_99.avg_z_sm(x);
data2 = sdata.cow_99.centerdist(x);
ind = find(~isnan(data) & ~isnan(data2));
data = data(ind);
data2 = data2(ind);
% data2 = min(0.1*ones(length(ind),1),log(data2(ind)+1));
x = x(ind);

% plot(x,data)
% hold on
% plot(x, data2)
% data3 = movmedian(data2,120);
% hold on
% plot(x, data3)
%%

findchangepts(data,'MaxNumChanges',20,...
                    'Statistic','mean','MinDistance',600);
findchangepts(data2,'MaxNumChanges',20,...
                    'Statistic','std','MinDistance',600);
                
%%                
findchangepts(data3,'MaxNumChanges',20,...
                    'Statistic','mean','MinDistance',600);
% find gaps that are too large > overlapping with bouts?



ipoints = [[1;ipoints(1:end-1); ipoints(end)], [ipoints;length(data)]];
ipoints2 = [[1;ipoints2(1:end-1); ipoints2(end)], [ipoints2;length(data2)]];


figure; 
subplot(2,1,1);hold on; box on;
plot(data); axis tight

subplot(2,1,2);hold on; box on;
plot(data2); axis tight


for i = 1:length(ipoints)
    
   ipoints(i,3) = mean(data(ipoints(i,1):ipoints(i,2)));
   ipoints(i,4) = std(data(ipoints(i,1):ipoints(i,2)));

   subplot(2,1,1)
   plot([ipoints(i,2) ipoints(i,2)],[0 2],'b','LineWidth',2)
   plot([ipoints(i,1) ipoints(i,2)],[ipoints(i,3) ipoints(i,3)],'k',...
         'LineWidth',2) 
   
   title(['No. CP z = ' num2str(size(ipoints,1))])
   
end

for i = 1:length(ipoints2)
   ipoints2(i,3) = mean(data2(ipoints2(i,1):ipoints2(i,2)));
   ipoints2(i,4) = std(data2(ipoints2(i,1):ipoints2(i,2)));
   subplot(2,1,2)
   plot([ipoints2(i,2) ipoints2(i,2)],[0 0.1],'b','LineWidth',2)
   plot([ipoints2(i,1) ipoints2(i,2)],[ipoints2(i,3) ipoints2(i,3)],'k',...
         'LineWidth',2) 
     
   title(['No. CP dist = ' num2str(size(ipoints2,1))])
end
