%% S12_results_paper.m
%--------------------------------------------------------------------------
% Created by Ines Adriaens, December 1st 2021
% Embedded in KB DDHT Sensing potential
%             Breed4Food Individual cattle tracking
%             NLAS - UWB/locomotion
%--------------------------------------------------------------------------
% This script produces the tables and figures for the manuscript 
% "Detecting dairy cows’ lying behaviour using 3D noisy ultra-wide 
%   band positioning data" by Adriaens, Ouweltjes, Esther, Pastell and
%   Kamphuis
%--------------------------------------------------------------------------
% Contents: 
%   - Table 1: data overview of cows and cow characteristics
%               parity, age, lactation stage, (?) health
%               milk yield, lying bouts ~iceqube
%   - Table 2: UWB data (stats+missing?)
%               stats of z, cd, missing data
%   - Table 3: performance of changepoint detection algorithm
%   - Figure 1: overview of cow data (1 example, 1 day)
%   - Figure 2: performance
%
%--------------------------------------------------------------------------
% EXTRA: 
%   - Table for data summary WCGALP
%   - Figure for data lying behavior WCGALP
%--------------------------------------------------------------------------


% )------------TO DO----
% stability detection!!
% maximal variance in both to assign a segment to 'lying'
%

clear variables
close all
clc

%% Table 1: data overview
% -- see excel file "Cowdata_summary.xlsx"

init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen University '...
                 '& Research\iAdriaens_doc\Projects\eEllen\'...
                 'B4F_indTracking_cattle\B4F_data\'];
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen University '...
                 '& Research\iAdriaens_doc\Projects\eEllen\'...
                 'B4F_indTracking_cattle\B4F_results\'];
init_.respaper = [init_.resdir 'Fig_paper\'];

init_.fn = 'Cowdata_summary.xlsx';

% read summary table
opts = detectImportOptions([init_.datadir init_.fn], 'Range','F40:J47');
cowsum.general = readtable([init_.datadir init_.fn], opts);
cowsum.general.Properties.VariableNames{1} = 'Name';

clear opts

% write to new excel file
writetable(cowsum.general,[init_.respaper 'Ex_tables.xlsx'],...
           'Sheet','cowdata');


%% Table 2a -- summary of lying bouts per animal

% file name
init_.fn2 = 'D2_data_meta.mat';
load([init_.resdir init_.fn2])

% given all iceqube data and given available uwb data
p_short = length(find(data_meta.IceQ.durlydown < 5))./ ...
                height(data_meta.IceQ);
disp(['% shorter than 5 min = ' num2str(p_short*100) '%'])


% IceQ data
cows = data_meta.cowliesum.cow;
for i = 1:height(data_meta.IceQ)
    % number of bouts 
    
    
end 
%% Table 2: cow liedown summary

% file name
init_.fn2 = 'D2_data_meta.mat';
load([init_.resdir init_.fn2])

% data on lying behaviour = cowliesum
%       delete cow not selected = 280
data_meta.cowliesum(data_meta.cowliesum.cow == 280,:) = [];

% stats
data_meta.cowliesum{32,:} = min(data_meta.cowliesum{1:31,:});
data_meta.cowliesum{33,:} = max(data_meta.cowliesum{1:31,:});
data_meta.cowliesum{34,:} = mean(data_meta.cowliesum{1:31,:});
data_meta.cowliesum{35,:} = std(data_meta.cowliesum{1:31,:});


% put in table