%% test script


clear variables 
clc



%% missing data imputation for time series 

% set to load saved data or reprocess raw data
init_.loadkey = 0;      % should be 1 if raw data needs reloading

% set session and dates
init_.sesinfo = table(['ses1';'ses2'], 'VariableNames',{'sesname'});
init_.sesinfo.sesno = ['1';'2'];
init_.sesinfo.startdate = [datetime(2019,7,3,13,00,00); ...
                          datetime(2019,07,10,11,00,00)];
init_.sesinfo.enddate = [datetime(2019,7,8,10,30,00); ...
                          datetime(2019,07,15,10,10,00)];

% set filepaths and filenames
init_.filedir = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\liestand\';
init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
                'University & Research\iAdriaens_doc\Projects\'...
                'eEllen\B4F_indTracking_cattle\B4F_data\'];
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
init_.filename1 = 'CheckAtt_liestand_';
init_.filename2 = 'BasicAttentions_';
init_.filename3 = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\sessioninfo.xlsx';
init_.filename4 = 'CheckTif4_liestand_';

% granularity of the trk data = no of seconds averaged
init_.nsec = 1; 

% gapsize threshold > to calculate differences
init_.gapcte = 5; 

load([init_.datadir 'D0_meta_events.mat'])
load([init_.datadir 'D1_trkloaded.mat'])

% subset
data = cdata.cow_1066;

% calculate gapsize
ind = find(~isnan(data.avg_x));
gapsize = diff([floor(datenum(data.date(1)));...
                datenum(data.date(ind))])*24*60*60;

% add gapsize to cdata
data.gapsize(:,1) = NaN;
data.gapsize(ind) = round(gapsize);

% % calculate distances when <5s missing (gapsize <=5)
% set = data(~isnan(data.avg_x) & ...
%     (data.gapsize <= init_.gapcte | ...
%     isnan(data.gapsize)),:);
% 
% % distance travelled expressed per second
% set.dist(1) = NaN;     % first
% set.dist(2:end) = sqrt(diff(set.avg_x).^2 + ...
%     (diff(set.avg_y).^2));
% set.dist = set.dist./set.gapsize;

% % join set with data
% data = outerjoin(data,set, ...
%     'Keys',{'object_name',...
%     'date',...
%     'time_in_h',...
%     'avg_x','gapsize',...
%     'avg_y','avg_z'},...
%     'MergeKeys',1);

% only select june, 13
data = data(floor(datenum(data.date))==datenum(2019,07,13),:);
data = data(1:20000,:);

%%  test median filter for x,y separately 


figure; plot(data.date(~isnan(data.avg_y)),data.avg_y(~isnan(data.avg_y)))
hold on; plot(data.date,data.avg_y,'k','LineWidth',1)
plot(data.date,movmedian(data.avg_y,60),'r:','LineWidth',1)
plot(data.date,movmedian(data.avg_y,60),'r-','LineWidth',1)
plot(data.date,movmedian(data.avg_y,60,'omitnan'),'r-','LineWidth',1)
plot(data.date,movmedian(data.avg_y,120,'omitnan'),'g-','LineWidth',1)



%%



% insert data in gaps from 1 to 60 seconds, by normal sampling from
% previous arrays
% the larger the gap, the more data is taken in
% minimum amount of data = half of the window

data.truegaps(:,1) = NaN;
data.truegaps(data.gapsize > init_.nsec) = ...
    data.gapsize(data.gapsize > init_.nsec);
data.misscode(:,1) = NaN;
gaps = 2:60;
windows = round(linspace(5,120,length(gaps)));
for i = 1:length(gaps)
    ind = find(data.gapsize == gaps(i));
    

    
    distarray = table(ind-windows(i),'VariableNames',{'startidx'});
    distarray.endidx = ind;
    
    for j = 1:height(distarray)
        distarray.numnan(j) = sum(~isnan(data.gapsize(distarray.startidx(j):...
                                distarray.endidx(j))));
        if distarray.numnan(j) < 0.5*windows(i)
            data.misscode(ind(j)) = 0;
        else
            % dist
            m = nanmean(data.dist(distarray.startidx(j):...
                                  distarray.endidx(j)));
            s = nanstd(data.dist(distarray.startidx(j):...
                                  distarray.endidx(j)));
            a = m - s;
            b = m + s;
            r = a + (b-a).*rand(gaps(i)-1,1);
            
            data.dist(ind(j)-(gaps(i)-1):ind(j)-1) = r;
            data.misscode(ind(j)) = 1;
            
            % avg_z
            m = nanmean(data.avg_z(distarray.startidx(j):...
                                  distarray.endidx(j)));
            s = nanstd(data.avg_z(distarray.startidx(j):...
                                  distarray.endidx(j)));
            a = m - s;
            b = m + s;
            r = a + (b-a).*rand(gaps(i)-1,1);
            
            data.avg_z(ind(j)-(gaps(i)-1):ind(j)-1) = r;

        end
    end 
end


% median smoothed in window of 60 seconds
% tested= rlowess, bayes, sgolay sp 2,3
tic
data.distsmooth1 = smooth(data.dist,...
    60,'rloess');
data.distsmooth1(data.gapsize > 60) = NaN;
figure; hold on; ylim([0 1])
plot(data.date,data.dist)
plot(data.date,data.distsmooth1,'LineWidth',1.5)
toc

% tested = rlowess, bayes, sgolay sp 2,3

tic

data.zsmooth1 = smooth(data.avg_z,...
    60,'rloess');
data.zsmooth1(data.gapsize > 60) = NaN;

figure; hold on; ylim([0 2.5])
plot(data.date,data.avg_z)
plot(data.date,data.zsmooth1,'LineWidth',1.5)

toc




%%