%% S9_postprocessing - analysis cpts results
%--------------------------------------------------------------------------
% created November 1st, 2021
% created by Ines Adriaens, adria036
%--------------------------------------------------------------------------
% Postprocessing script for the changepoint analysis, based on the
% thresholds set and determined from the lying data, after finding a robust
% penalty calculated from the data itself failed. The following settings
% were used:
%       - NumChanges   = 30 = max number of changes per day (~15 bouts/d)
%       - MinDistance  = 600 = minimum 10 minutes between the events
%                              (segment length)
%       - Statistic Z = "mean"
%       - Statistic CD = "mean"
%--------------------------------------------------------------------------
% STEP 1: load data and changepoint results
% STEP 2: set thresholds for false positive CP
%           - explore direction of change
%           - explore minimum difference in level
% STEP 3: evaluate / calculate TP / FP
%           - make tables and figures for manuscript
%--------------------------------------------------------------------------
% results are created by S8_cpnts_final_nofig.m
% no figures are made or saved for this run
%--------------------------------------------------------------------------

% prepare workspace
clear variables
close all
clc


%% STEP 1: load data of changepoint results
%--------------------------------------------------------------------------
% "data" contains for each changepoint the average, std, numeric date of
%     start and numeric date of the end of each segment
% this information is added to the "data" per cow per day
% first step is to add when a cow goes from lying to standing and vice
% versa (ground truth) + window
%--------------------------------------------------------------------------

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];

% load data
load([init_.resdir 'D5fin_cpts_res.mat'])  % cpts - cptsr
load([init_.resdir 'D5fin_dataday.mat'])   % data

% combine data of all days within cow
field_ = fieldnames(data); % cow ids
for i = 1:length(field_)
    
    % get fields
    day_ = fieldnames(data.(field_{i}));
    day_ = day_(startsWith(day_,'day'));
    
    % prepare results array
    result.(field_{i}) = data.(field_{i}).(day_{1});

    % add all other days to results
    for ii = 2:length(day_)
        result.(field_{i}) = [result.(field_{i}); ...
                              data.(field_{i}).(day_{ii})];
    end
    
    % if less than 60s in slatted = 
    ind = find(result.(field_{i}).inslatted == 1);
    dind = diff(ind);
    idx = find(dind>1 & dind <= 300);
    indices = [ind(idx),ind(idx)+dind(idx)];
    
    for j = 1:size(indices,1)
        result.(field_{i}).inslatted(indices(j,1):indices(j,2)) = 1;
    end
end

% clear variables
clear i ii day_ field_
clear ind i idx ii ind dind 

% set thresholds for change
init_.MinLevelZ = 0.2; % minimal level change Z position
init_.MinLevelCD = 0.2; % minimal level change CD position
init_.Window = 300; % 5' before and after true change = TP

%% STEP 2: Explore and preprocess
%--------------------------------------------------------------------------
% 1) random selection of figures to test and plot and get a view on
%    sensible thresholds to test:
%       - if in slatted floor - delete CP
%       - threshold on minimal level change
%       - window? eg. 600s
%       - interpretation for direction of change?
% 2) loop over thresholds -- set selection criteria and visualise
% 3) set thresholds and calculate performance on entire dataset
%       - combine both CD and Z changes
%--------------------------------------------------------------------------

% select 5 random cows and 5 random days to plot/test/develop
field_ = fieldnames(result);   % cow ids
sel = field_(sort(randi([1 length(field_)],[5 1]))); % 5 random ids

% select for each cow a random day to plot
selday = zeros(length(sel),1);
for ii = 1:length(sel)
    day_ = unique(floor(result.(sel{ii}).numdate));
    day_ = day_(~isnan(day_));
    selday(ii) = day_(randi([1 length(day_)],1));
end

% prepare figure settings
pl.OP = [0 6.5 38 22];     % position
pl.datacol = [205 92 92]./255;  % color for data
pl.lyingcol = [75 0 130]./255; % color for lying bouts
pl.slattedcol = [32,178,170]./255; % color for slatted

% make figures
close all
for ii = 1:length(sel)
    % data to plot
    pl.x = result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                                    selday(ii));
    pl.z = result.(sel{ii}).avg_z_sm(floor(result.(sel{ii}).numdate) == ...
                                    selday(ii));
    pl.cd = result.(sel{ii}).centerdist(floor(result.(sel{ii}).numdate) == ...
                                    selday(ii));    
    % in slatted  & islying                        
    pl.inslatted = result.(sel{ii}).inslatted(floor(result.(sel{ii}).numdate) == ...
                                    selday(ii));
    pl.islying = result.(sel{ii}).islying(floor(result.(sel{ii}).numdate) == ...
                                    selday(ii))*(-1)+1;                                
    
    % plot data, lying, CP
    fig.(sel{ii}) = figure('Units','centimeters','OuterPosition',pl.OP);
    subplot(2,1,1); hold on; box on; 
        title([sel{ii} ', day ' num2str(selday(ii)) ', Z'],'Interpreter','none')
        xlabel('Day from start trial [d]')
        ylabel('Z-position (height) [m]')
    plot(pl.x, pl.z,'-','Color',pl.datacol,'LineWidth',1)
    plot(pl.x, pl.islying*2+0.25,'-','Color',pl.lyingcol,'LineWidth',1.7)
    plot(pl.x(pl.inslatted == 1),2.4*ones(sum(pl.inslatted),1),'s',...
        'Color',pl.slattedcol,'MarkerFaceColor',pl.slattedcol,'LineWidth',1)
               
    subplot(2,1,2); hold on; box on;
        title([sel{ii} ', day ' num2str(selday(ii)) ', CD'],'Interpreter','none')
        xlabel('Day from start trial [d]')
        ylabel('Distance from center [m]')
    plot(pl.x, pl.cd,'-','Color',pl.datacol,'LineWidth',1)
    plot(pl.x, pl.islying*10+2,'-','Color',pl.lyingcol,'LineWidth',1.7)
    plot(pl.x(pl.inslatted == 1),13*ones(sum(pl.inslatted),1),'s',...
         'Color',pl.slattedcol,'MarkerFaceColor',pl.slattedcol,'LineWidth',1)
end

% clear variables 
clear ans ii 

%% STEP 3: add evaluation of changepoints
% add the CP results and window to result table for each cow
% calculate the average and std for each cow, and the direction between
% changepoints and add to 'results' table

% fieldnames
field_ = fieldnames(result);



%TODO: put it in a loop with different thresholds!

% calculate chars (mean, std, length missing) per segment
for i = 1:length(field_)
    
    % make sure the data (dates) are complete (add missing)
    result.(field_{i}).numdate_s = round(result.(field_{i}).numdate*24*60*60);
    completetime = table((result.(field_{i}).numdate_s(1):...
                   result.(field_{i}).numdate_s(end))','VariableNames',{'numdate_s'});
    result.(field_{i}) = outerjoin(result.(field_{i}),completetime,'MergeKeys',1); 
    clear completetime
               
               
    % add window around true positive change
    result.(field_{i}).liediff = abs([0;diff(result.(field_{i}).islying)]);
    ind = find(result.(field_{i}).liediff == 1); % indices of change
    ind = [ind, ind - (1:init_.Window); ind, ind + (1:init_.Window)]; % add window indices
    ind = unique(reshape(ind,[numel(ind),1])); % 
    result.(field_{i}).TrueChange(ind) = 1; % add true change
    
    % days for each cow
    days = unique(floor(result.(field_{i}).numdate));
    days = days(~isnan(days));
    
    % find initialized for day (not a detected change, needed to calc stats)
    for ii = 1:length(days)
        ind = find(floor(result.(field_{i}).numdate) == days(ii),1,'first');
        result.(field_{i}).dayinit(ind) = 1;
    end
    
    % segment statistics for Z
    Zseg = find(result.(field_{i}).ischangeZ >= 1 & ...
                result.(field_{i}).inslatted == 0);
    for ii = 1:length(Zseg)-1
        result.(field_{i}).avg_segZ(Zseg(ii):Zseg(ii+1)) = ...
                nanmean(result.(field_{i}).avg_z_sm(Zseg(ii):Zseg(ii+1)));
        result.(field_{i}).std_segZ(Zseg(ii):Zseg(ii+1)) = ...
                nanstd(result.(field_{i}).avg_z_sm(Zseg(ii):Zseg(ii+1)));
    end
    
    % segment statistics for Z
    CDseg = find(result.(field_{i}).ischangeCD >= 1 & ...
                 result.(field_{i}).inslatted == 0); 
    for ii = 1:length(CDseg)-1
        result.(field_{i}).avg_segCD(CDseg(ii):CDseg(ii+1)) = ...
                nanmean(result.(field_{i}).centerdist(CDseg(ii):CDseg(ii+1)));
        result.(field_{i}).std_segCD(CDseg(ii):CDseg(ii+1)) = ...
                nanstd(result.(field_{i}).centerdist(CDseg(ii):CDseg(ii+1)));
    end
    
    % in gap
    % determine whether at the edge of a big (>300s) gap
    % indices of Z
    ind = find(result.(field_{i}).ischangeZ > 0);
    
    % determine whether adjecent to gap or the segment before or after
    % contains > 50% NaN
    for ii = 2:length(ind)  % 1 = first (init), start at second
        
        % if 50% in window around detection = NaN
        perc_nan_before = (sum(isnan(result.(field_{i}).avg_z_sm(ind(ii)-init_.Window : ...
                    ind(ii))))/length(ind(ii)-init_.Window:ind(ii)))*100;
        try 
            perc_nan_after = (sum(isnan(result.(field_{i}).avg_z_sm(ind(ii) : ...
                    ind(ii)+init_.Window)))/length(ind(ii)-init_.Window:ind(ii)))*100;
        catch
            perc_nan_after = 0;
        end
        if perc_nan_before > 50 || perc_nan_after > 50
            result.(field_{i}).Zingap(ind(ii)) = 1;
        end
    end
    
    % centerdist
    ind = find(result.(field_{i}).ischangeCD > 0 );
    
    % determine whether adjecent to gap or the segment before or after
    % contains > 50% NaN
    for ii = 2:length(ind)  % 1 = first (init), start at second
        
        % if 50% in window around detection = NaN
        perc_nan_before = (sum(isnan(result.(field_{i}).centerdist(ind(ii)-init_.Window : ...
                    ind(ii))))/length(ind(ii)-init_.Window:ind(ii)))*100;
        try
            perc_nan_after = (sum(isnan(result.(field_{i}).centerdist(ind(ii) : ...
                    ind(ii)+init_.Window)))/length(ind(ii)-init_.Window:ind(ii)))*100;
        catch
            perc_nan_after = 0;
        end
        if perc_nan_before > 50 || perc_nan_after > 50
            result.(field_{i}).CDingap(ind(ii)) = 1;
        end
    end    
end





% visualise changes (true and detected) on the random plots
for ii = 1:length(sel)
    
    % data to plot
    pl.x = result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                           selday(ii) & result.(sel{ii}).ischangeZ >= 1 & ...
                           result.(sel{ii}).inslatted == 0);
    pl.x = [pl.x pl.x];
    
    for j = 1:size(pl.x,1)
        figure(fig.(sel{ii}));
        subplot(2,1,1);
        plot(pl.x(j,:),[0 2.5],'-','Color',[0 200 255]./255,'LineWidth',1.5)
    end
    plot(result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                  selday(ii) & result.(sel{ii}).TrueChange == 1),...
         result.(sel{ii}).TrueChange(floor(result.(sel{ii}).numdate) == ...
                  selday(ii) & result.(sel{ii}).TrueChange == 1),'s',...
         'Color',pl.lyingcol,...
         'MarkerFaceColor',pl.lyingcol,'MarkerSize',5,'LineWidth',1.5);
    % plot the averages
    plot(result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                  selday(ii)),...
         result.(sel{ii}).avg_segZ(floor(result.(sel{ii}).numdate) == ...
                  selday(ii)), 'LineWidth',1,'Color',[0 0 255]./255);     
    
    % plot the changes next to a gap
    ind = find(floor(result.(sel{ii}).numdate) == selday(ii) & ...
               result.(sel{ii}).Zingap == 1);
    for j = 1:length(ind)
        plot([result.(sel{ii}).numdate(ind(j)) result.(sel{ii}).numdate(ind(j))],...
             [0 2.5],'--','LineWidth',1.2,'Color','r')
    end
              
     
     
    % data to plot --CD
    pl.x = result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                           selday(ii) & result.(sel{ii}).ischangeCD >= 1 & ...
                           result.(sel{ii}).inslatted == 0);
    pl.x = [pl.x pl.x];
    
    for j = 1:size(pl.x,1)
        figure(fig.(sel{ii}));
        subplot(2,1,2);
        plot(pl.x(j,:),[0 14],'-','Color',[0 200 255]./255,'LineWidth',1.5)
    end
    plot(result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                  selday(ii) & result.(sel{ii}).TrueChange == 1),...
         result.(sel{ii}).TrueChange(floor(result.(sel{ii}).numdate) == ...
                  selday(ii) & result.(sel{ii}).TrueChange == 1)*7,'s',...
         'Color',pl.lyingcol,...
         'MarkerFaceColor',pl.lyingcol,'MarkerSize',5,'LineWidth',1.5);
    plot(result.(sel{ii}).numdate(floor(result.(sel{ii}).numdate) == ...
                  selday(ii)),...
         result.(sel{ii}).avg_segCD(floor(result.(sel{ii}).numdate) == ...
                  selday(ii)), 'LineWidth',1,'Color',[0 0 255]./255);   
              
    % plot the changes next to a gap
    ind = find(floor(result.(sel{ii}).numdate) == selday(ii) & ...
               result.(sel{ii}).CDingap == 1);
    for j = 1:length(ind)
        plot([result.(sel{ii}).numdate(ind(j)) result.(sel{ii}).numdate(ind(j))],...
             [0 14],'--','LineWidth',1.2,'Color','r')
    end
end


% add number to true change column
for i = 1:length(field_)
    T = 1;
    % find the gaps > 1
    separation = find(result.(field_{i}).liediff == 1);
    separation = [1;separation;height(result.(field_{i}))];
    
    for ii = 1:length(separation)-1
        result.(field_{i}).numchangeseg(separation(ii):separation(ii+1)) = T;
        T=T+1;
    end
end


    
%% Calculate the confusion matrix

% add to result - col with TP, TN, FP, FN

% prepare confusion matrices
clear confMat
confMat.Zonly = table(field_,'VariableNames',{'CowID'});
confMat.CDonly = table(field_,'VariableNames',{'CowID'});
confMat.both = table(field_,'VariableNames',{'CowID'});

for i = 1:length(field_)
    % find the number of  - all
    confMat.Zonly.allChanges(i) = nansum(result.(field_{i}).liediff);
    confMat.CDonly.allChanges(i) = nansum(result.(field_{i}).liediff);
    confMat.both.allChanges(i) = nansum(result.(field_{i}).liediff);
    
    % find the number of raw detections
    confMat.Zonly.NumChangesDetect(i) = sum(result.(field_{i}).ischangeZ > 0 & ...
                                         result.(field_{i}).dayinit ~= 1);
    confMat.CDonly.NumChangesDetect(i) = sum(result.(field_{i}).ischangeCD > 0 & ...
                                         result.(field_{i}).dayinit ~= 1);
    confMat.both.NumChangesDetectOR(i) = sum((result.(field_{i}).ischangeZ > 0 |...
                                             result.(field_{i}).ischangeCD > 0) & ...
                                             result.(field_{i}).dayinit ~= 1 );
    confMat.both.NumChangesDetectAND(i) = sum(result.(field_{i}).ischangeZ > 0 & ...
                                             result.(field_{i}).ischangeCD > 0 );
    
	% find the number of true positive changes
    confMat.Zonly.TPraw(i) = sum(result.(field_{i}).ischangeZ > 0 & ...
                                 result.(field_{i}).TrueChange == 1);
    confMat.CDonly.TPraw(i) = sum(result.(field_{i}).ischangeCD > 0 & ...
                                 result.(field_{i}).TrueChange == 1);
    
%   =========================== assign to same change! ====================
    uniqueseg = unique(result.(field_{i}).numchangeseg);
    TP = 0;  % initialize true positives
    FN = 0;  % initialize false negatives
    for ii = 1:length(uniqueseg)
        ind = find(result.(field_{i}).numchangeseg == uniqueseg(ii) & ...
                   (result.(field_{i}).ischangeZ > 0 | ...
                    result.(field_{i}).ischangeCD > 0));
        if ~isempty(ind)
            TP = TP+1;
        else
            FN = FN+1;
        end
    end
    confMat.both.TPraw(i) = TP;
    confMat.both.FNraw(i) = FN;
    
%   ============================ asign to same change! ====================
    % is false positive add to results -- Z
    ind = find(result.(field_{i}).ischangeZ > 0 & ...
               result.(field_{i}).TrueChange == 0 & ...
               result.(field_{i}).dayinit ~= 1);
	result.(field_{i}).FP_z(ind) = 1;
    % is false positive add to results -- CD
    ind = find(result.(field_{i}).ischangeCD > 0 & ...
               result.(field_{i}).TrueChange == 0 & ...
               result.(field_{i}).dayinit ~= 1);
	result.(field_{i}).FP_z(ind) = 1;
    
    % add to results the distance between successive alerts
    ind = find(result.(field_{i}).liediff > 0);
    diffalert = diff(result.(field_{i}).numdate_s(ind));
    result.(field_{i}).diffalert(ind(2:end)) = diffalert;
    
    % find the number of changes not first not inslatted
    confMat.Zonly.NumChangesDetectTrue(i) = sum(result.(field_{i}).ischangeZ > 0 & ...
                                         result.(field_{i}).dayinit ~= 1 & ...
                                         result.(field_{i}).inslatted == 0);
    confMat.CDonly.NumChangesDetectTrue(i) = sum(result.(field_{i}).ischangeCD > 0 & ...
                                         result.(field_{i}).dayinit ~= 1 & ...
                                         result.(field_{i}).inslatted == 0);
    
                                     
                                     
    
    % find the number of false detected Z - gap associated
    
    % find the true positive -- Z dimension
    ind = find(result.(field_{i}).ischangeZ == 1 & ...
               result.(field_{i}).TrueChange == 1);
    

end


confMat.both.sensitivity = confMat.both.TPraw./confMat.both.allChanges;
mean(confMat.both.sensitivity)



%%

cowid= 'cow_8108';
day__ = 12; 
subset = result.(cowid)(floor(result.(cowid).numdate) == day__,:);






%% check the data per day incl. icebouts to detect deviations
clear variables
close all
clc

init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen University '...
                 '& Research\iAdriaens_doc\Projects\eEllen\'...
                 'B4F_indTracking_cattle\B4F_results\'];
init_.checkdir = [init_.resdir 'Checkdir\'];

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];

% load data
load([init_.resdir 'D5fin_cpts_res.mat'])  % cpts - cptsr
load([init_.resdir 'D5fin_dataday.mat'])   % data

% load meta data
init_.fn2 = 'D2_data_meta.mat';
load([init_.resdir init_.fn2])


% combine data of all days within cow
field_ = fieldnames(data); % cow ids
for i = 1:length(field_)
    
    % get fields
    day_ = fieldnames(data.(field_{i}));
    day_ = day_(startsWith(day_,'day'));
    
    % prepare results array
    result.(field_{i}) = data.(field_{i}).(day_{1});

    % add all other days to results
    for ii = 2:length(day_)
        result.(field_{i}) = [result.(field_{i}); ...
                              data.(field_{i}).(day_{ii})];
    end
    
    % if less than 60s in slatted = 
    ind = find(result.(field_{i}).inslatted == 1);
    dind = diff(ind);
    idx = find(dind>1 & dind <= 300);
    indices = [ind(idx),ind(idx)+dind(idx)];
    
    for j = 1:size(indices,1)
        result.(field_{i}).inslatted(indices(j,1):indices(j,2)) = 1;
    end
end

% clear variables
clear i ii day_ field_
clear ind i idx ii ind dind 

% result
close all
cows = fieldnames(result);
for i = 1:length(cows)
    days = unique(day(result.(cows{i}).date));
    for j = 1:length(days)
        f = figure('Units','centimeters','OuterPosition',[50 3 35 18]);
        hold on; box on;
        
        daydata = result.(cows{i})(day(result.(cows{i}).date) == days(j),:);
        plot(daydata.date, daydata.avg_z_sm, 'Color', [255 229 204]./255)
        plot(daydata.date,daydata.islying,'LineWidth',1.5)
        plot(daydata.date, log(daydata.avg_z_sm+0.7)+0.36,'Color',[0 0 170]./255)

%         plot(daydata.date, smooth(log(daydata.avg_z_sm+0.7)+0.36,180,'rloess'),'Color',[0 0 0]./255)

        title([cows{i} ', ' datestr(daydata.date(1)) ' to ' ...
        datestr(daydata.date(end))], 'Interpreter','none')
        xlabel('date'); ylabel('z, liedown')
        ylim([0 1.5])
        saveas(f,[init_.checkdir cows{i} 'day_' num2str(days(j)) '.jpg'])
        close all
        
    end
end




