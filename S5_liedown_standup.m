%% S5_liedown_standup
% author: ines adriaens -- github/gitlab @iadriaens -- wur user @adria036
% date  : May 18, 2021
%
% based on the data exploration and visualisation, this script contains the
% "final" 2021 analysis of the UWB tracklab data for the B4F individual cow
% tracking project.
%
% objectives:   - method recognising resting behaviour (1)
%               - detection of lying down and standing up (2)
%               - estimation of time needed to get up/lie down (3)
%
% steps:        - set constants and filepaths
%               - load data
%               - data selection and sorting
%               - data editing, visualisation and quality control
%               - data splicing (development
%               - model development, optimizationg and fitting
%               - summary and visualisation results
%               - reporting and discussing
%
% remark: cross fertilization with KB DDHT SP ~ link between getting up /
%           lying down and the 
%
% input data:   - lying bouts < icecube accelerometer data
%               - manually annotated duration lie down and get up
%               - rough tracklab data
%               
% selected start data ~ overlap accelerometer data / video / tracklab
%               - ses1 = deep litter    03/07/2019 13:00 - 08/07/2019 10:30
%               - ses2 = synthetic      10/07/2019 11:00 - 15/07/2019 10:10
%
% methods:      model to be developed (FILENAME)
%

clear variables
close all
clc

%% STEP 0: set filenames and constants

% set to load saved data or reprocess raw data
init_.loadkey = 0;      % should be 1 if raw data needs reloading

% set session and dates
init_.sesinfo = table(['ses1';'ses2'], 'VariableNames',{'sesname'});
init_.sesinfo.sesno = ['1';'2'];
init_.sesinfo.startdate = [datetime(2019,7,3,13,00,00); ...
                          datetime(2019,07,10,11,00,00)];
init_.sesinfo.enddate = [datetime(2019,7,8,10,30,00); ...
                          datetime(2019,07,15,10,10,00)];

% set filepaths and filenames
init_.filedir = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\liestand\';
% init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
%                 'University & Research\iAdriaens_doc\Projects\'...
%                 'eEllen\B4F_indTracking_cattle\B4F_data\'];
% init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
%                 'University & Research\iAdriaens_doc\Projects\' ...
%                 'eEllen\B4F_indTracking_cattle\B4F_results\'];
% init_.datadir = [cd '\'];
%  init_.resdir = [cd '\'];

init_.filename1 = 'CheckAtt_liestand_';
init_.filename2 = 'BasicAttentions_';
init_.filename3 = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\sessioninfo.xlsx';
init_.filename4 = 'CheckTif4_liestand_';

% granularity of the trk data = no of seconds averaged
init_.nsec = 1; 

% gapsize threshold > to calculate differences
init_.gapcte = 5; 

% missing data imputation 
init_.gaps = 2:60;          % no. of seconds  per gap
init_.windows = round(linspace(5,120,length(init_.gaps))); % distribution window


%% STEP 1: load and format data
% load data of selected days, accelerometer attentions and annotated data
data = struct();
for i = 1:size(init_.sesinfo,1)      % no of sessions included
    % set dates to load trk data from
    dates = datetime(floor(datenum(init_.sesinfo.startdate(i))): ...
        floor(datenum(init_.sesinfo.enddate(i))), ...
        'ConvertFrom','datenum');
    
    % load tracklab data
    for ii = 1:length(dates)
        % filename = date
        init_.filenametrk = datestr(dates(ii),'yyyymmdd');
        fieldname = ['d' init_.filenametrk];
        
        if ~isfield(data,fieldname)
            % detect options
            opts = detectImportOptions([init_.datadir init_.filenametrk ...
                '.trk'],'FileType','text');
            
            % set time to datetime var
            opts = setvartype(opts,'time','datetime');
            
            % change inputformat of datetime variable
            opts = setvaropts(opts,'time','InputFormat',...
                'yyyy/MM/dd HH:mm:ss.SSS');
            
            % read table into variable data
            data.(fieldname) = readtable([init_.datadir ...
                init_.filenametrk '.trk'],opts);
        end
    end
end


%% UWB data per cow over all available days
% summary of the data 'accumulated' over nsec as defined in init_
% store data in cdata = data over all days per individual animal
% cdata fieldnames are 


if init_.loadkey == 1  % only if data needs reloading

    % sort and store the data per cow and include time points missing
    fieldnames_ = fieldnames(data);
    cdata = struct();
    for i = 1:length(fieldnames_)

        % select data and add missing values
        out = F1_data_edit_trk(data.(fieldnames_{i}),init_.nsec);

        % append per cow in new array cdata
        cows = unique(out.object_name);
        for ii = 1:length(cows)
            cowfield = sprintf('cow_%d',cows(ii));  % cow_id

            % add data to cdata
            if ~isfield(cdata,cowfield)  % if this is first data for this cow
                cdata.(cowfield) = out(out.object_name == cows(ii),:);
            else                         % if field already exists - append
                cdata.(cowfield) = [cdata.(cowfield);...
                                    out(out.object_name == cows(ii),:)];
            end
        end
    end

    % clear variables
    clear out i ii cows ans cowfield fieldnames_

    % save data 
    save([init_.datadir 'D1_trkloaded.mat'],'cdata');

end


%% Load data previously saved, add distance travelled

% load data if loadkey == 0
% if init_.loadkey == 0
%     load([init_.datadir 'D0_meta_events.mat'])
%     load([init_.datadir 'D1_trkloaded.mat'])
% end

load('D0_meta_events.mat')
load('D1_trkloaded.mat')



%% UWB data summary and selection
%   -----   selection 1: tracklab data ~ quality   -----
% we will use both (x,y) and z data; from the (x,y) data, we will derive
% the distance travelled; from the z-data we want to detect height shifts


% ----------------- some remarks on missing data ----------------- %
% we know that a lot of data are missing, and from the past analysis, these
% missing data potentially are not fully random; being e.g. linked to
% the position of the cow in the barn, the speed of the cow (when
% stationary fewer values are missing), etc. Because what we aim to
% detect only has a duration of a few seconds (stand up/lie down), we
% need to be very careful with data imputation or interpolation. However,
% it is reasonable to simulate data at the z-level when the gap is not too
% large (--??--), based on the average and variability of (--??--)
% surrounding or preceding data. Also for 'distance' this is possible, but
% intuitively not for separate (x,y) time series (--??--). 
% Possibilites include: 
%   estimate the distribution of the data available in the past 2
%   minutes, conditional on % missing data (e.g. at least 40% available)
%   'bayesian' imputation  >  given nearest neighbours
%   'weighted' moving distribution ?
% 
%
% ----------------- some remarks on missing data ----------------- %









% distance travelled, data imputation and calculation of robust smooth
fields_ = fieldnames(cdata);
timerval = zeros(length(fields_),2); % var storing gap propagation and smooth
for i = 1:length(fields_)
    
    % calculate gapsize
    ind = find(~isnan(cdata.(fields_{i}).avg_x));
    gapsize = diff([floor(datenum(cdata.(fields_{i}).date(1)));...
        datenum(cdata.(fields_{i}).date(ind))])*24*60*60;
    
    % add gapsize to cdata
    cdata.(fields_{i}).gapsize(:,1) = NaN;
    cdata.(fields_{i}).gapsize(ind) = round(gapsize);
    
    % calculate distances when <5s missing (gapsize <=5)
    set = cdata.(fields_{i})(~isnan(cdata.(fields_{i}).avg_x) & ...
        (cdata.(fields_{i}).gapsize <= init_.gapcte | ...
        isnan(cdata.(fields_{i}).gapsize)),:);
    
    % distance travelled expressed per second
    set.dist(1) = NaN;     % first
    set.dist(2:end) = sqrt(diff(set.avg_x).^2 + ...
        (diff(set.avg_y).^2));
    set.dist = set.dist./set.gapsize;
    
    % join data with cdata
    cdata.(fields_{i}) = outerjoin(cdata.(fields_{i}),set, ...
        'Keys',{'object_name',...
        'date',...
        'time_in_h',...
        'avg_x','gapsize',...
        'avg_y','avg_z'},...
        'MergeKeys',1);
    
    % missing data imputation > the larger the gap (init_.gap) the larger
    % the window used to calculate the distribution to sample from
    % (init_.window)
    tic
        cdata.(fields_{i}) = ...
            fillnormgaps(cdata.(fields_{i}),init_.gaps,init_.windows);
    timerval(i,1) = toc;
    
    % median smoothed in window of 60 seconds
%     tic
%         cdata.(fields_{i}).distsmooth = smooth(cdata.(fields_{i}).dist,...
%                                                60,'rloess');
%         cdata.(fields_{i}).distsmooth(cdata.(fields_{i}).gapsize > ...
%                                         max(init_.gaps)) = NaN;
%         cdata.(fields_{i}).zsmooth = smooth(cdata.(fields_{i}).dist,...
%                                                60,'rloess');
%         cdata.(fields_{i}).zsmooth(cdata.(fields_{i}).gapsize > ...
%                                         max(init_.gaps)) = NaN;
%     timerval(i,2) = toc;
end

%% visualize

for i = 1:length(fields_)
    data = cdata.cow_90;

    h = figure('Units','centimeters','Outerposition',[0 0 30 20]);
    subplot(3,1,1);hold on; box on;title(['i = ' num2str(i) ', cow ' fields_{i} ', smoothed y'])
    xlabel('Date'); ylabel('Position (m)')
    plot(data.date,data.avg_y_sm,'k','LineWidth',0.8)
    plot(data.date(data.misscode == 0),data.avg_y_sm(data.misscode == 0),'o','LineWidth',1,'MarkerSize',2,'Color',[102/255 178/255 255/255])
    plot(data.date(data.misscode == 1),data.avg_y_sm(data.misscode == 1),'o','LineWidth',1,'MarkerSize',2,'Color',[204/255 204/255 255/255])
    plot(data.date(data.misscode == 2),data.avg_y_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2)

    subplot(3,1,2);hold on; box on; title('Smoothed x')
    xlabel('Date'); ylabel('Position (m)')
    plot(data.date,data.avg_x_sm,'k','LineWidth',0.8)
    plot(data.date(data.misscode == 0),data.avg_x_sm(data.misscode == 0),'mo','LineWidth',1,'MarkerSize',2,'Color',[255/255 204/255 204/255])
    plot(data.date(data.misscode == 1),data.avg_x_sm(data.misscode == 1),'ro','LineWidth',1,'MarkerSize',2,'Color',[255/255 102/255 102/255])
    plot(data.date(data.misscode == 2),data.avg_x_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2,'Color',[255/255 51/255 51/255])

    subplot(3,1,3);hold on; box on; title('Smoothed distance')
    xlabel('Date'); ylabel('Position (m)')
    plot(data.date,data.dist_sm,'k','LineWidth',0.8)
    plot(data.date(data.misscode == 0),data.dist_sm(data.misscode == 0),'mo','LineWidth',1,'MarkerSize',2,'Color',[204/255 255/255 204/255])
    plot(data.date(data.misscode == 1),data.dist_sm(data.misscode == 1),'ro','LineWidth',1,'MarkerSize',2,'Color',[102/255 255/255 102/255])
    plot(data.date(data.misscode == 2),data.dist_sm(data.misscode == 2),'bo','LineWidth',1,'MarkerSize',2,'Color',[51/255 205/255 51/255])

    plot(data.date(isnan(data.misscode) & data.dist_sm > 0.5),...
         data.dist_sm(isnan(data.misscode) & data.dist_sm > 0.5), 'rx',...
         'MarkerSize',4,'LineWidth',1)

    saveas(h, ['Fig_Data_imputation_' num2str(i) '.fig'])
    saveas(h, ['Fig_Data_imputation_' num2str(i) '.tif'])
end 


for i = 1:length(fields_)
    data = cdata.(fields_{i});
    save([sprintf('HPCdata_%i',i) '.mat'], 'data','timerval')
end

disp('HPC_smooth is finished')

quit

%%
%% figures data imputation
close all
fields_ = fieldnames(cdata);
for i = 2:5%length(fields_)
    h = figure('Units','centimeters','OuterPosition',[0 0 35 24]);
    subplot(3,1,1); 
    
    plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_x, ...
         'Color',[0.5 0.5 0.5],'LineWidth',0.5)
    box on; hold on; 
    plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_x_sm, ...
         'Color','b','LineWidth',1);
    plot(cdata.(fields_{i}).date(~isnan(cdata.(fields_{i}).misscode)), ...
         cdata.(fields_{i}).avg_x_sm(~isnan(cdata.(fields_{i}).misscode)), ...
         'o','Markersize',2,'Color','k');
   xlim([min(cdata.(fields_{i}).date), min(cdata.(fields_{i}).date)+1 ])
   xlabel('Date'); ylabel('x')
   
   subplot(3,1,2); 
    
    plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_y, ...
         'Color',[0.5 0.5 0.5],'LineWidth',0.5)
    box on; hold on; 
    plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_y_sm, ...
         'Color','m','LineWidth',1);
    plot(cdata.(fields_{i}).date(~isnan(cdata.(fields_{i}).misscode)), ...
         cdata.(fields_{i}).avg_y_sm(~isnan(cdata.(fields_{i}).misscode)), ...
         'o','Markersize',2,'Color','k');
   xlim([min(cdata.(fields_{i}).date), min(cdata.(fields_{i}).date)+1 ])
   xlabel('Date'); ylabel('y')
  
   subplot(3,1,3);
       plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_z, ...
         'Color',[0.5 0.5 0.5],'LineWidth',0.5)
    box on; hold on; 
    plot(cdata.(fields_{i}).date, cdata.(fields_{i}).avg_z_sm, ...
         'Color','r','LineWidth',1);
    plot(cdata.(fields_{i}).date(~isnan(cdata.(fields_{i}).misscode)), ...
         cdata.(fields_{i}).avg_z_sm(~isnan(cdata.(fields_{i}).misscode)), ...
         'o','Markersize',2,'Color','k');
   xlim([min(cdata.(fields_{i}).date), min(cdata.(fields_{i}).date)+1 ])
    xlabel('Date'); ylabel('z')
saveas(h, [init_.resdir sprintf('FIG_dataimputation_%d',i) '.tif'])
end





%%
function out = fillnormgaps(data,gaps,windows)
    data.misscode(:,1) = NaN;
    data.dist_xy(:,1) = NaN;
    for j = 1:length(gaps)   % per gapsize
        ind = find(data.gapsize == gaps(j));
        startidx = max(1,ind-windows(j));  % start indices
        endidx = ind;               % end indices
        
        for k = 1:length(ind)
            numnan = sum(~isnan(data.gapsize(...
                                       startidx(k):endidx(k))));
            if numnan < 0.5*windows(j)   % if not enough data available
                data.misscode(endidx(k)-gaps(j)+1:endidx(k)) = 0;
                
                % interpolation dist
                idx = find(~isnan(data.dist(startidx(k):endidx(k)-1)),1,'last');
                if ~isempty(idx)
                    r = interp1([startidx(k)+idx-1 endidx(k)+1],...
                            [data.dist(startidx(k)+idx-1) data.dist(endidx(k)+1)],...
                            startidx(k)+idx:endidx(k));
                    data.dist(startidx(k)+idx:endidx(k)) = r;
                end
                
                idx = find(~isnan(data.avg_x(startidx(k):endidx(k)-1)),1,'last');
                if ~isempty(idx)
                    % interpolation avg_x
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_x(startidx(k)+idx-1) data.avg_x(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);                           
                    data.avg_x(startidx(k)+idx:endidx(k)-1) = r;

                    % interpolation avg_y
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_y(startidx(k)+idx-1) data.avg_y(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);
                    data.avg_y(startidx(k)+idx:endidx(k)-1) = r;

                    % interpolation avg_z
                    r = interp1([startidx(k)+idx-1 endidx(k)],...
                                [data.avg_z(startidx(k)+idx-1) data.avg_z(endidx(k))],...
                                startidx(k)+idx:endidx(k)-1);
                    data.avg_z(startidx(k)+idx:endidx(k)-1) = r;

                    % dist_xy
                    data.dist_xy(ind(k)-(gaps(j)-1):ind(k)-1)...
                            = sqrt(diff(data.avg_x(ind(k)-(gaps(j)):ind(k)-1)).^2 + ...
                              (diff(data.avg_y(ind(k)-(gaps(j)):ind(k)-1)).^2));
                end
                
            else
                % indicate misscode = 1 = filled with ~N(m,s)
                data.misscode(endidx(k)-gaps(j)+1:endidx(k)) = 1;
                
                % dist
                m = nanmean(data.dist(startidx(k):endidx(k)));
                s = nanstd(data.dist(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.dist(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % avg_z
                m = nanmean(data.avg_z(startidx(k):endidx(k)));
                s = nanstd(data.avg_z(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_z(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
               % avg_x
                m = nanmean(data.avg_x(startidx(k):endidx(k)));
                s = nanstd(data.avg_x(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_x(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % avg_y
                m = nanmean(data.avg_y(startidx(k):endidx(k)));
                s = nanstd(data.avg_y(startidx(k):endidx(k)));
                                      
                a = m - s;
                b = m + s;
                r = a + (b-a).*rand(gaps(j)-1,1);

                data.avg_y(ind(k)-(gaps(j)-1):ind(k)-1) = r;
                
                % dist_xy
                data.dist_xy(ind(k)-(gaps(j)-1):ind(k)-1)...
                        = sqrt(diff(data.avg_x(ind(k)-(gaps(j)):ind(k)-1)).^2 + ...
                          (diff(data.avg_y(ind(k)-(gaps(j)):ind(k)-1)).^2));

           end
        end 
    end
    
    % if gap > max gapsize
    ind = find(data.gapsize > max(gaps) & data.gapsize < 3*60);
    
    
    for j = 1:length(ind)
        % misscode = 2
        data.misscode(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = 2;
    
        % find last available measurement (dist)
        firstarray = data(ind(j)-data.gapsize(ind(j)),:);
        lastarray = data(ind(j)+1,:);
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)+1],...
                [firstarray.dist lastarray.dist],...
                ind(j)-data.gapsize(ind(j))+1:ind(j));
        data.dist(ind(j)-data.gapsize(ind(j))+1:ind(j)) = r;
    
        % avg_x, avg_y, avg_z
        firstarray = data(ind(j)-data.gapsize(ind(j)),:);
        lastarray = data(ind(j),:);
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_x lastarray.avg_x],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_x(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_y lastarray.avg_y],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_y(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        r = interp1([ind(j)-data.gapsize(ind(j)) ind(j)],...
                [firstarray.avg_z lastarray.avg_z],...
                ind(j)-data.gapsize(ind(j))+1:ind(j)-1);
        data.avg_z(ind(j)-data.gapsize(ind(j))+1:ind(j)-1) = r;
        
        % dist_xy
        data.dist_xy(ind(j)-data.gapsize(ind(j))+1:ind(j)-1)...
                = sqrt(diff(data.avg_x(ind(j)-data.gapsize(ind(j))+1:ind(j))).^2 + ...
                  (diff(data.avg_y(ind(j)-data.gapsize(ind(j))+1:ind(j))).^2));
        
    end
    
    % boundaries based on no-imputed data
    data.avg_x(data.avg_x < 0) = 0;
    data.avg_x(data.avg_x > max(data.avg_x(isnan(data.misscode)))) = ...
                            max(data.avg_x(isnan(data.misscode)));
    data.avg_y(data.avg_y < 0) = 0;
    data.avg_y(data.avg_y > max(data.avg_y(isnan(data.misscode)))) = ...
                            max(data.avg_y(isnan(data.misscode)));
    data.avg_z(data.avg_z < 0) = 0;
    data.avg_z(data.avg_z > max(data.avg_z(isnan(data.misscode)))) = ...
                            max(data.avg_z(isnan(data.misscode)));
                           
                        
    % smooth then dist
    data.avg_x_sm(:,1) = movmedian(data.avg_x,45);
    data.avg_y_sm(:,1) = movmedian(data.avg_y,45);
    data.avg_z_sm(:,1) = movmedian(data.avg_z,45);
    data.dist_sm(:,1) = 0;
    data.dist_sm(2:end) = sqrt(diff(data.avg_x_sm).^2 + ...
                  (diff(data.avg_y_sm).^2));
    
    % boundaries for dist
    data.dist_sm(data.dist_sm < 0) = 0;
    data.dist_sm(data.dist_sm > 1.5) = 1.5;



    out = data;
end






