##############################################################################
# title: Kalman filter applied to daily lying data of individual dairy cows#
# Application developed by Rudi de Mol as part of 'DS/AI fellowship' provided by Wageningen Data Competence Center to Akke Kok and Iris Boumans #
##############################################################################

#CLEAR GLOBAL ENVIRONMENT
rm(list = ls())

#install.packages("ISLR")
#library(ISLR)

# Working directory
setwd("C:/Rmodel")

#example data - 3 cows
SensorData <- read.csv("SensorData2.csv", header=TRUE)
SensorData$datum <- as.POSIXct(SensorData$datum, format = "%Y-%m-%d") 
CowList <- read.csv("CowList2.csv", header=TRUE)

# Number of cows
NrOfCows = length(CowList$CowNr)

# Make graphs of the sensor data per cow
# Plots to be stored in a pdf file
  #NB make sure this working directory has a folder to /output to save the pdf file.
pdf(file="Outputs/LyingTimePerCow.pdf")

for (cowcount in 1:NrOfCows) {
  # Select sensor data from this cow by selecting on cow number
  SensorDataSel = subset(SensorData,subject==CowList$CowNr[cowcount])
  
  # Plot lying time data of this cow
  plot(SensorDataSel$datum,SensorDataSel$liggen,xlab = "Date",ylab = "Time (hr)", 
       main = paste("Cow ",cowcount, ": ",CowList$CowNr[cowcount]," (",CowList$CowCount[cowcount]," days)",sep = ''),
       xaxs = "i",yaxs="i",ylim = c(0,24),
       xlim = c(as.POSIXct('12/31/2018',format = "%m/%d/%Y"),as.POSIXct('01/31/2020',format = "%m/%d/%Y")),
       pch = 4)
  # Add grid lines to plot
  grid(NULL,12,col = "lightgray",lty = 1,lwd = 1)
  
  # Apply Kalman filter
  # Declarations for Kalman filter
  m <- 3 ##m <-2
  NrOfDays <- length(SensorDataSel$subject)
  y <- NA
  x <- c(SensorDataSel$liggen[1],0,0)  ## x <- twee lengte, [1], 0
  e <- NA
  Var_e <- NA
  V <- 5 ##bekijken in de tijd en dan goed initializeren
  
  #nieuwe versie 9-6-'21
  P <- matrix(data = c(0), nrow=m, ncol=m) #variantie parameters
  P[1,1] <- 9 
  P[2,2] <- 0.01
  P[3,3] <- 0.00 ## valt weg bij 2 var (no)
  
  A <- matrix(data = c(1,0,0, 1,1,0, 1,1,1),nrow = 3,ncol = 3) #correctie 9-6-'21 ## 1,0,1,1 als 2 var
  W <- 0.00*diag(m) #aangepast naar 0
  W[1,1] <- 0.01 #aangepast naar Iris 14-6
  zn <- array(data = NA,dim = c(m,1))
  Kn <- array(data = NA,dim = c(m,1))
  Cn <- array(data = c(1,0,0),dim = c(m,1))
  alfaV <- 0.1
  
  # Define (empty) results
  # Fitted values
  KalmanFit <- array(data = NA, dim = c(NrOfDays))
  # Alerts per day
  KalmanAlert <- array(data = NA, dim = c(NrOfDays))
  # Limits used for alerts and confidence interval
  UpperLimit1 <- array(data = NA, dim = c(NrOfDays))
  UpperLimit2 <- array(data = NA, dim = c(NrOfDays))
  UpperLimit3 <- array(data = NA, dim = c(NrOfDays))
  
  # Apply filter per day
  for (DayCount in 2:NrOfDays) {
    # Prediction stage
    xnn_1 <- A%*%x
    Pnn_1 <- A%*%P%*%t(A) + W
    # New observation
    y <- SensorDataSel$liggen[DayCount]
    e <- y - t(Cn)%*%xnn_1
    
    # Updating stage
    # Calculate Kalman gain
    Kn <- Pnn_1%*%Cn%*%solve(t(Cn)%*%Pnn_1%*%Cn + V)
    # improved estimate of state
    x <- xnn_1 + Kn%*%e
    P <- Pnn_1 - Kn%*%t(Cn)%*%Pnn_1
    Var_e <- t(Cn)%*%Pnn_1%*%Cn + V
    
    # Update matrix V with currect z vector
    zn = y - t(Cn)%*%x
    # Limit adaptation
    if (zn < -2.576*sqrt(V)) {zn <- -2.576*sqrt(V)}
    if (zn > 2.576*sqrt(V)) {zn <- 2.576*sqrt(V)}
    V = alfaV*zn%*%zn + (1-alfaV)*V
    
    # Calculate and store results
    UpperLimit1[DayCount] = sqrt(Var_e)*1.960
    UpperLimit2[DayCount] = sqrt(Var_e)*2.576
    UpperLimit3[DayCount] = sqrt(Var_e)*3.291
    if (e > UpperLimit1[DayCount]) {KalmanAlert[DayCount] <- 1}
    if (e < -1*UpperLimit1[DayCount]) {KalmanAlert[DayCount] <- -1}
    if (e > UpperLimit2[DayCount]) {KalmanAlert[DayCount] <- 2}
    if (e < -1*UpperLimit2[DayCount]) {KalmanAlert[DayCount] <- -2}
    if (e > UpperLimit3[DayCount]) {KalmanAlert[DayCount] <- 3}
    if (e < -1*UpperLimit3[DayCount]) {KalmanAlert[DayCount] <- -3}
    KalmanFit[DayCount] <- y-e
  }
  
  # Add Kalman results to plot
  lines(SensorDataSel$datum,KalmanFit) 
  lines(SensorDataSel$datum,KalmanFit+UpperLimit1, col = 'red') 
  lines(SensorDataSel$datum,KalmanFit-UpperLimit1, col = 'red')
  # Tricky way to include alerts in plot
  points(SensorDataSel$datum,23.5*match(KalmanAlert,1),pch = 17,col = 'yellow')
  points(SensorDataSel$datum,23.5*match(KalmanAlert,2),pch = 17,col = 'orange')
  points(SensorDataSel$datum,23.5*match(KalmanAlert,3),pch = 17,col = 'red')
  points(SensorDataSel$datum,23.5*match(KalmanAlert,-1),pch = 25,col = 'yellow')
  points(SensorDataSel$datum,23.5*match(KalmanAlert,-2),pch = 25,col = 'orange')
  points(SensorDataSel$datum,23.5*match(KalmanAlert,-3),pch = 25,col = 'red')
  
  # Remove selection of sensor data
  rm(SensorDataSel)
}

# Create pdf file by executing dev.off()
dev.off()
#par(mfrow=c(1,1))