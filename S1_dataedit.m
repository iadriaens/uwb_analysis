%% S1_dataedit
% this file aims at reading and selecting tracklab data with format .trk
% INPUT files = .trk files (-ascii txt) files from Tracklab
% OUTPUT = summary of the data
%   STEP0: set parameters
%   STEP1: read the .trk data in a data structure
%   STEP2: summarize contents of each file (= each day) per cow
%   STEP3: accumulate in average values of x seconds

clear variables 
close all
clc

tic
%% STEP0: set parameters
% directory in which data files are stored
dircopy = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_data\'];

dirresults = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];

       
% define fileids (dates) to read
fileids = ls([dircopy '*.trk']);
% ['20190714.trk';'20190715.trk';'20190716.trk';...
%            '20190724.trk';'20190725.trk';'20190726.trk'];
       
% set number of seconds to accumulate (=average) in a position value
nseconds = 5;


%% STEP1: read in data tables

% run over length fileids
for i = 1:size(fileids,1)

    % select file to load
    filename = fileids(i,:);

    % detect options
    opts = detectImportOptions([dircopy filename],'FileType','text');

    % set time to datetime var
    opts = setvartype(opts,'time','datetime');

    % change inputformat of datetime variable
    opts = setvaropts(opts,'time','InputFormat','yyyy/MM/dd HH:mm:ss.SSS');

    % read table into variable data
    fieldname = ['d' fileids(i,1:end-4)];
    data.(fieldname) = readtable(filename,opts);
end

% clear variables
clear ans i opts filename dircopy fieldname fileids

%% STEP2: data summary at day level

% detect and store fieldnames of structure
FNames = fieldnames(data);
summary.dataInconsist = array2table((1:size(FNames,1))','VariableNames',{'FileNo'});

for i = 1:size(FNames)
    
    % delete NaN
    disp(['No of NaN values = ' num2str(sum(isnan(data.(FNames{i}).object_name)))])
    summary.dataInconsist.FN(i,:) = FNames{i};
    summary.dataInconsist.DelNaN(i,:) = sum(isnan(data.(FNames{i}).object_name));  % add no deleted
    data.(FNames{i})(isnan(data.(FNames{i}).object_name),:) = [];    % delete

    % add col with numeric time stamp
    data.(FNames{i}).numtime = datenum(data.(FNames{i}).time);

    % summarize data per cow + add average and std of location data
    summary.(FNames{i}) = groupsummary(data.(FNames{i}),"object_name",...
             ["mean","std"],["location_x","location_y","location_z"]);
end

% clear up workspace
clear i

%% STEP3: accumulation of values in x sec averages (data reduction)

for i = 1:size(FNames,1)

    % check all data is made the same day
    disp(['No. of meas on different date = ' ...
          num2str(length(find(floor(data.(FNames{i}).numtime) - ...
          floor(data.(FNames{i}).numtime(1)) ~= 0)))]);
    summary.dataInconsist.NoDiffDay(i,:) = length(find(floor(data.(FNames{i}).numtime)...
                                           - floor(data.(FNames{i}).numtime(1)) ~= 0));

    % convert numeric timestamp to seconds of that day and round per second
    data.(FNames{i}).secdata = round((data.(FNames{i}).numtime ...
                                    - floor(data.(FNames{i}).numtime(1)))*24*3600);

    % calculate timestamps as preparation for subs (= indices for accumulation)
    data.(FNames{i}).accumsubs = (data.(FNames{i}).secdata ...
                                  - mod(data.(FNames{i}).secdata,nseconds))...
                                  ./ nseconds;

% %     % unique statement to have unique combination of nseconds and cowId
% %     unisubs = unique(table(data.(FNames{i}).object_name,data.(FNames{i}).accumsubs,...
% %                      'VariableNames',{'object_name','accumsubs'}),'rows');

    % array with theoretical number of values if no missing data
    A = ones(length(unique(data.(FNames{i}).object_name)),1);   % array of ones with length unique cows
    B = (1:(24*3600/nseconds))-1'; % start from zeros, 86400 unique seconds per day
    C = A*B;   % each row has 0:86399
    C = reshape(C',[length(B)*length(A),1]);  % reshape C all rows

    % do the same with cowIds
    A = unique(data.(FNames{i}).object_name);  % array of unique cows
    B = ones(1, ...
            length(1:(24*3600/nseconds)));    % array of ones length n sec/day
    D = A*B;   % array of cowids
    C(:,2) = reshape(D',[length(B)*length(A),1]);  % reshape C all rows

    % add a column to C with the indices/subs = unique per cow and per sec
    C(:,3) = (1:size(C,1))';

    % array to table and outerjoin
    C = array2table(C,'VariableNames',{'accumsubs','object_name','subs'});
    data_acc.(FNames{i}) = outerjoin(data.(FNames{i}),C,...
                                    'Keys',{'accumsubs','object_name'},...
                                    'MergeKeys',true);
    data_acc.(FNames{i}) = sortrows(data_acc.(FNames{i}),...
                                    {'object_name','subs'});

    % accumulate arrays in a new array with average location
    C.avg_x = accumarray(data_acc.(FNames{i}).subs,data_acc.(FNames{i}).location_x,[],@nanmean);
    C.avg_y = accumarray(data_acc.(FNames{i}).subs,data_acc.(FNames{i}).location_y,[],@nanmean);
    C.avg_z = accumarray(data_acc.(FNames{i}).subs,data_acc.(FNames{i}).location_z,[],@nanmean);
    
    % store in new variable per day
    data_nsec.(FNames{i}) = C;
    
    % add date and time stamp
    DateString = FNames{i};
    date = datenum(datetime(DateString(2:end),'InputFormat','yyyyMMdd'));
    dates = date + (data_nsec.(FNames{i}).accumsubs .* nseconds ./ (3600*24)); 
    data_nsec.(FNames{i}).date = ...
        datetime(dates, 'ConvertFrom','datenum');
    data_nsec.(FNames{i}).timeH = data_nsec.(FNames{i}).accumsubs .* nseconds ./ 3600;
    
    % remove redundant variables
    data_nsec.(FNames{i}) = removevars(data_nsec.(FNames{i}),'subs');
    data_nsec.(FNames{i}) = movevars(data_nsec.(FNames{i}),'accumsubs','After','timeH');
    data_nsec.(FNames{i}) = movevars(data_nsec.(FNames{i}),{'date','timeH'},'After','object_name');
    
end

% clear variables
clear A B C D i unisubs data_acc data

%% STEP 4: write results

for i = 1:size(FNames,1)
    % date
    DateString = FNames{i};
    date = DateString(2:end);
    
    % filename
    filename = ['trk_' date '_acc_' num2str(nseconds) 'sec.txt'];
    
    % write files per day in results txt file
    writetable(data_nsec.(FNames{i}),[dirresults filename])
end

% clear variables
clear ans date FNames i nseconds dates DateString

% write results summaries
FN = fieldnames(summary);
for i = 1:size(FN,1)
    % write summary results in excel file
    writetable(summary.(FN{i}),[dirresults 'summary.xlsx'],'Sheet',FN{i});
end
toc   % runs in about 40 minutes




