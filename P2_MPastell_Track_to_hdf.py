# -*- coding: utf-8 -*-
"""
Created on Wed Oct 01 13:39:40 2014

@author: psa119
"""
import tracklab
import pandas as pd
import glob
files = glob.glob("Tlab_data/*")



for f in files:
    try:
        df, l = tracklab.readTrackLab(f)
        outfile = f.replace("Tlab_data", "hdf5_data").replace(".tlp", ".h5")
        df.to_hdf(outfile, "data", complib="bzip2", complevel=9, append=False)
        print f
    except:
        print "Error reading", f
        continue