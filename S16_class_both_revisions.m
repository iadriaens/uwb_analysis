                                                                                    % classificatie
% created january 10, 2021
% from segmententation of both
% 1) statistical properties
% 2) train classifier
% 3) evaluate

clear variables
close all
clc


%% load data, set filepaths and constants

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
% load data
load([init_.resdir 'D8_sdata.mat'])
load([init_.resdir 'D2_data_meta.mat'])
load([init_.resdir 'D7all_cpts_multi.mat'])

% delete cow 280 and 495 (no iceQube data) 
% sdata = rmfield(sdata,'cow_280');
% sdata = rmfield(sdata,'cow_495');

% reference date for numeric time
refdate = datenum(2019,7,3);


%% calculate statistical properties, add changepoints to dataset

% cows
cows = fieldnames(cpts_rmulti);

for i = 1:length(cows) % all cows with results
    
    
    T1=1;    % count cpts for cd
       
    % all days for this cow
    days = fieldnames(cpts_rmulti.(cows{i}));
    for j = 1:length(days)
        % ref day numeric
        day = days{j}; day = str2double(day(5:end)); % num day (~refday)
        
        % select and store/split data
        data.(cows{i}).(days{j}) = sdata.(cows{i})(:,[2 3 7 13 17 18 20 21 15]);                                      
        data.(cows{i}).(days{j})(isnan(data.(cows{i}).(days{j}).avg_z_sm) | ...
                                 isnan(data.(cows{i}).(days{j}).centerdist),:) = [];
        data.(cows{i}).(days{j}).numdate = ...
               datenum(data.(cows{i}).(days{j}).date)-refdate; % numeric date
        data.(cows{i}).(days{j}) = data.(cows{i}).(days{j})(...
               floor(data.(cows{i}).(days{j}).numdate) == day,:);
        
           
           
        %------------------------------- Z -------------------------------

        % changepoints detected for z
        cpdata = array2table(cpts_rmulti.(cows{i}).(days{j}).ipoints,...
                            'VariableNames',{'istart','iend','avgZ','stdZ',...
                            'avgCD','stdCD'});
        
        % add additional statistical/categorization parameters:
        %       1. difference in level
        %       2. outliers
        %       3. std without outliers (90% CI?) Q10-90?
        %       4. missing data/gapsize 
        %       5. length of segment
       
        % calculate statistical proporties
        for l = 1:height(cpdata)
            istart = cpdata.istart(l);
            iend = cpdata.iend(l);
            segdata = data.(cows{i}).(days{j})(istart:iend,:);
            segdata.isoutlierZ = isoutlier(segdata.znorm,'mean');
            segdata.isoutlierCD = isoutlier(segdata.cdnorm,'mean');
            segdata.timegap(1) = 0; 
            segdata.timegap(2:end) = diff(segdata.time_in_h*3600);
            tstart = segdata.time_in_h(1)*3600;
            tend = segdata.time_in_h(end)*3600;

            segdata.timegap(1) = 0; 
            segdata.timegap(2:end) = diff(segdata.time_in_h*3600);
            tstart = segdata.time_in_h(1)*3600;
            tend = segdata.time_in_h(end)*3600;
            
            % general properties both series
            cpdata.inslatted(l) = double(...
                       sum(segdata.inslatted) ./ height(segdata) > 0.9);
                        cpdata.seglength(l) = tend - tstart;
            cpdata.maxgapsize(l) = max(segdata.timegap);
            cpdata.gappercent(l) = (sum(segdata.timegap(segdata.timegap > 180)) ./ ...
                                   (tend-tstart))*100;
            cpdata.numdatestart(l) = data.(cows{i}).(days{j}).numdate(...
                                            cpdata.istart(l)); % start
            cpdata.numdateend(l) = data.(cows{i}).(days{j}).numdate(...
                                            cpdata.iend(l)); % end
                                        
            % statistical properties Z
            Q = quantile(segdata.znorm,0.95) - ...
                                   quantile(segdata.znorm,0.05);
            cpdata.rangeZ(l) = range(segdata.znorm);
            cpdata.difquantrangeZ(l) = cpdata.rangeZ(l)-Q;
            cpdata.outlpercentZ(l) = (sum(segdata.isoutlierZ)./ ...
                                   cpdata.seglength(l))*100;
            cpdata.avgoutlZ(l) = nanmean(segdata.znorm(segdata.isoutlierZ == 0)); 
            cpdata.stdoutlZ(l) = nanstd(segdata.znorm(segdata.isoutlierZ == 0)); 
            
            % statistical properties CD
            Q = quantile(segdata.cdnorm,0.95) - ...
                                   quantile(segdata.cdnorm,0.05);
            cpdata.rangeCD(l) = range(segdata.cdnorm);
            cpdata.difquantrangeCD(l) = cpdata.rangeCD(l)-Q;
            cpdata.outlpercentCD(l) = (sum(segdata.isoutlierCD)./ ...
                                   cpdata.seglength(l))*100;
            cpdata.avgoutlCD(l) = nanmean(segdata.cdnorm(segdata.isoutlierCD == 0)); 
            cpdata.stdoutlCD(l) = nanstd(segdata.cdnorm(segdata.isoutlierCD == 0)); 
            
            % check data for clustering
            cpdata.checktruth(l) = (sum(segdata.islying) ./ height(segdata))*100;
            cpdata.checktruth_bin(l) = double(cpdata.checktruth(l) > 50);
        end
        
        
        % avg distances
        cpdata.avgdifoutlZ(1) = NaN;
        cpdata.avgdifoutlZ(2:end) = abs(diff(cpdata.avgoutlZ)); % level difference
        cpdata.avgdifoutlCD(1) = NaN;
        cpdata.avgdifoutlCD(2:end) = abs(diff(cpdata.avgoutlCD)); % level difference
        
        % gapsize in next segments
        cpdata.nextseggap(:,1) = NaN;
        cpdata.nextseggap(1:end-1) = cpdata.gappercent(2:end);
        
        % add that it's a changepoint to data
        data.(cows{i}).(days{j}).ischange(1) = T1;
        data.(cows{i}).(days{j}).ischange(cpdata.iend) = ...
                                    (T1+1:T1+height(cpdata))';
        
        % store data                      
                                
        fie = ['cp_' days{j}];
        data.(cows{i}).(fie) = cpdata;
        T1 = T1+height(cpdata);
        
        clear abs iend istart T tend tstart
    end
end

clear i iend istart j l T1 T2 tend tstart cpdata rcddata rzdata refdate 
clear Q day fie segdata



%% combine all CPTS/data 

% cpts z + properties
cows = fieldnames(cpts_rmulti);

for i = 1:length(cows)
    
    cpts_multi.(cows{i}).cp = []; 
    days = fieldnames(cpts_rmulti.(cows{i}));
    
    for j = 1:length(days)
        
        % add to previous set
        fieldname_= ['cp_' days{j}];
        cpts_multi.(cows{i}).cp = [cpts_multi.(cows{i}).cp;data.(cows{i}).(fieldname_)];
    end
end

%% classdata
% all data togehter
classdata = []; 
cows = fieldnames(cpts_rmulti);
for i = 1:length(cows)
   
    classdata = [classdata;...
        table(repmat(cows(i),height(cpts_multi.(cows{i}).cp),1),'VariableNames',{'id'}),...
        table(i*ones(height(cpts_multi.(cows{i}).cp),1),'VariableNames',{'cow'}), ...
        table(floor(cpts_multi.(cows{i}).cp.numdatestart(:,1)),'VariableNames',{'day'}),...
        cpts_multi.(cows{i}).cp(:,[24 7:10 25:27 13:22])];
    
end


%% classification performance - test multiple days
% DAYS split - data in day < x

for x = [3 4 5 6 8 9]
    % from 29 unique cows
    disp(['number of segments = ' num2str(length(find(classdata.day < x)))])  % number of segments = 5138
    disp(['number of segments with value = 0 = ' num2str(length(find(classdata.day < x & classdata.checktruth_bin == 0)))]) % with value 0 = 2229*
    disp(['number of unique cows = ' num2str(max(unique(classdata.cow)))]) % with number of cows = 30
    
    % training dataset and test data set
    ind = find(classdata.day < x);
    splitdata.training = classdata(ind,:);
    length(unique(splitdata.training.id))
    
    ind = find(classdata.day >= x);
    splitdata.test = classdata(ind,:);
    
    % train model classifier on training data only
    nx = ['ndays_' num2str(x)];
    [trainedClassifier.(nx), validationAccuracy.(nx)] = trainClassifier(...
            splitdata.training(:,3:end));
    % cross-validation accuracy 5-fold
    yfit.(nx) = trainedClassifier.(nx).predictFcn(splitdata.test(:,3:end));
    predictacc_xval.(nx) = length(find(double(yfit.(nx))-1 - splitdata.test.checktruth_bin==0)) / length(yfit.(nx));

    % correct 0
    ind = find(splitdata.test.checktruth_bin==0);
    correct_0(x-2,1) = x;
    correct_0(x-2,2) = length(ind);
    disp(['No. of non-lying segments in test = ' num2str(length(ind))])
    answer = length(find(double(yfit.(nx)(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
    disp(['No. of correctly predicted non-lying segments = ' num2str(answer)])
    disp(['% of correctly predicted non-lying segments = ' num2str(answer/length(ind))])
    correct_0(x-2,3) = answer;
    correct_0(x-2,4) = answer/length(ind)*100;
    
    % correct 1
    ind = find(splitdata.test.checktruth_bin==1);
    correct_1(x-2,1) = x;
    correct_1(x-2,2) = length(ind);
    disp(['No. of lying segments in test = ' num2str(length(ind))])
    answer = length(find(double(yfit.(nx)(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
    disp(['No. of correctly predicted lying segments = ' num2str(answer)])
    disp(['% of correctly predicted lying segments = ' num2str(answer/length(ind)*100)])
    correct_1(x-2,3) = answer;
    correct_1(x-2,4) = answer/length(ind)*100;
    
    % total duration comparison of non-lying time
    ind = find(splitdata.test.checktruth_bin==0);  % not lying
    dur_lying.(nx) = sum(splitdata.test.seglength(ind));
    disp(['Total non-lying time in testset = ' num2str(dur_lying.(nx)/3600) 'hours'])
    ind2 = find(double(yfit.(nx))-1 == 0 & splitdata.test.checktruth_bin ==0);
    answer = sum(splitdata.test.seglength(ind2));
    disp(['Total duration predicted non-lying = ' num2str(answer/3600) 'hours'])
    disp(['Difference total predicted non-lying = ' num2str((dur_lying.(nx)-answer)/3600)]);
    disp(['Procentual deviation non-lying = ' num2str(abs(dur_lying.(nx)-answer)/dur_lying.(nx)*100) '%' ])
    dur_nly(x-2,1) = x;
    dur_nly(x-2,2) = dur_lying.(nx)/3600;
    dur_nly(x-2,3) = answer;
    dur_nly(x-2,4) = abs(dur_lying.(nx)-answer)/dur_lying.(nx)*100;
    

    % total duration comparison of lying time
    ind = find(splitdata.test.checktruth_bin==1);  % not lying
    dur_lying.(nx) = sum(splitdata.test.seglength(ind));
    disp(['Total lying time in testset = ' num2str(dur_lying.(nx)/3600) 'hours'])
    ind2 = find(double(yfit.(nx))-1 == 1 & splitdata.test.checktruth_bin ==1);
    answer = sum(splitdata.test.seglength(ind2));
    disp(['Total duration predicted lying = ' num2str(answer/3600) 'hours'])
    disp(['Difference total predicted lying = ' num2str((dur_lying.(nx)-answer)/3600)]);
    disp(['Procentual deviation lying = ' num2str(abs(dur_lying.(nx)-answer)/dur_lying.(nx)*100) '%' ])
    dur_ly(x-2,1) = x;
    dur_ly(x-2,2) = dur_lying.(nx)/3600;
    dur_ly(x-2,3) = answer;
    dur_ly(x-2,4) = abs(dur_lying.(nx)-answer)/dur_lying.(nx)*100;


    % evaluation at cow level
    splitdata.test.predicted = double(yfit.(nx))-1;
    % confusion matrix
    figure;
    plotconfusion(categorical(splitdata.test.checktruth_bin),...
                  categorical(splitdata.test.predicted)) 
    xticklabels({'non-lying','lying',''})
    yticklabels({'non-lying','lying',''})
    xlabel('Class');
    ylabel('Predicted')
    h=gcf;
    saveas(h,[init_.resdir '\Fig_paper\confusion_all_x' num2str(x) '.tif'])


    % evaluate performance at cow level
    res.cow = unique(splitdata.test(:,1:2),'rows');
    cows = unique(splitdata.test.cow);

    for i = 1:height(res.cow)

        % training part
        idx = find(splitdata.training.cow == cows(i));
        res.cow.nosegtrain(i) = length(idx);
        res.cow.perctrain(i) = length(idx)/height(splitdata.training)*100;


        % results - segment classification
        rescow = splitdata.test(splitdata.test.cow == cows(i),:);
        res.cow.nosegtest(i) = height(rescow);
        res.cow.nosegtest_lie(i) = sum(rescow.checktruth_bin);
        res.cow.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
        res.cow.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
        res.cow.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
        res.cow.accuracy(i) = res.cow.correct(i)./(res.cow.correct(i)+res.cow.incorrect(i));

        % results - liedown accuracy
        res.cow.durdata(i) = sum(rescow.seglength)/3600;
        idx = find(rescow.checktruth_bin == 1);
        res.cow.durliedown(i) = sum(rescow.seglength(idx))/3600;

        idx = find(rescow.predicted == 1);
        res.cow.predliedown(i) = sum(rescow.seglength(idx))/3600;

        res.cow.liedownperc_dev(i) = 100*abs(res.cow.predliedown(i)-res.cow.durliedown(i))/...
                (res.cow.durliedown(i));
        res.cow.liedownacc(i) = 100-100*abs(res.cow.predliedown(i)-res.cow.durliedown(i))/...
                (res.cow.durliedown(i));
    end


    % evaluate performance at cow and at day level
    res.day = unique(splitdata.test(:,1:3),'rows');
    cows = unique(splitdata.test(:,2:3),'rows');

    for i = 1:height(res.day)  

        % results - segment classification
        rescow = splitdata.test(splitdata.test.cow == cows.cow(i) & ...
                                splitdata.test.day == cows.day(i),:);
        res.day.nosegtest(i) = height(rescow);
        res.day.nosegtest_lie(i) = sum(rescow.checktruth_bin);
        res.day.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
        res.day.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
        res.day.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
        res.day.accuracy(i) = res.day.correct(i)./(res.day.correct(i)+res.day.incorrect(i));

        % results - liedown accuracy
        res.day.durdata(i) = sum(rescow.seglength)/3600;
        idx = find(rescow.checktruth_bin == 1);
        res.day.durliedown(i) = sum(rescow.seglength(idx))/3600;

        idx = find(rescow.predicted == 1);
        res.day.predliedown(i) = sum(rescow.seglength(idx))/3600;

        res.day.liedownperc_dev(i) = 100*abs(res.day.predliedown(i)-res.day.durliedown(i))/...
                (res.day.durliedown(i));
        res.day.liedownacc(i) = 100-100*abs(res.day.predliedown(i)-res.day.durliedown(i))/...
                (res.day.durliedown(i));
    end

    figure('Units','centimeters','OuterPosition',[5 5 28 14])
     subplot(1,2,1); hold on; box on; grid on
    histogram(res.day.accuracy*100,'Normalization','probability','FaceColor',[0 102 102]./255)
    title('Classification accuracy per cow-day')
    xlabel('Accuracy [%]')
    ylabel('Probability, cow-days')

    subplot(1,2,2); hold on; box on; grid on;
    histogram(res.day.liedownacc,'Normalization','probability','FaceColor',[102 0 0]./255)
    title('Liedown duration prediction accuracy per cow-day')
    xlabel('Accuracy [%]')
    ylabel('Probability, cow-days')

    h = gcf;
    saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_a_' num2str(x) '.tif'])
    saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_b_' num2str(x) '.fig'])
    saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_c_' num2str(x) '.png'])
    saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_' num2str(x) '.pdf'])


%     answer = length(find(res.day.liedownacc < 80));
%     disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy lieduration less than 80%'])
% 
%     answer = length(find(res.day.liedownacc > 90));
%     disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy more than than 90%'])
% 
%     answer = length(find(res.day.accuracy >= 0.90));
%     disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy classification more than 90%'])

    clear idx Q ind ind2 ans answer

  
    
    
    
end




% train same classifier on training data only
[trainedClassifier, validationAccuracy] = trainClassifier(...
            splitdata.training(:,3:end))
        % cross-validation accuracy 5-fold
        
yfit = trainedClassifier.predictFcn(splitdata.test(:,3:end));

length(find(double(yfit)-1 - splitdata.test.checktruth_bin==0)) / length(yfit)
% prediction accuracy = 91.26%

% correct 0
ind = find(splitdata.test.checktruth_bin==0);
disp(['No. of non-lying segments in test = ' num2str(length(ind))])
answer = length(find(double(yfit(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
disp(['No. of correctly predicted non-lying segments = ' num2str(answer)])
disp(['% of correctly predicted non-lying segments = ' num2str(answer/length(ind))])

% correct 1
ind = find(splitdata.test.checktruth_bin==1);
disp(['No. of lying segments in test = ' num2str(length(ind))])
answer = length(find(double(yfit(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
disp(['No. of correctly predicted lying segments = ' num2str(answer)])
disp(['% of correctly predicted lying segments = ' num2str(answer/length(ind))])

% total duration comparison of non-lying time
ind = find(splitdata.test.checktruth_bin==0);  % not lying
dur_lying = sum(splitdata.test.seglength(ind));
disp(['Total non-lying time in testset = ' num2str(dur_lying/3600) 'hours'])
ind2 = find(double(yfit)-1 == 0 & splitdata.test.checktruth_bin ==0);
answer = sum(splitdata.test.seglength(ind2));
disp(['Total duration predicted non-lying = ' num2str(answer/3600) 'hours'])
disp(['Difference total predicted non-lying = ' num2str((dur_lying-answer)/3600)]);
disp(['Procentual deviation non-lying = ' num2str(abs(dur_lying-answer)/dur_lying*100) '%' ])

% total duration comparison of lying time
ind = find(splitdata.test.checktruth_bin==1);  % not lying
dur_lying = sum(splitdata.test.seglength(ind));
disp(['Total lying time in testset = ' num2str(dur_lying/3600) 'hours'])
ind2 = find(double(yfit)-1 == 1 & splitdata.test.checktruth_bin ==1);
answer = sum(splitdata.test.seglength(ind2));
disp(['Total duration predicted lying = ' num2str(answer/3600) 'hours'])
disp(['Difference total predicted lying = ' num2str((dur_lying-answer)/3600)]);
disp(['Procentual deviation lying = ' num2str(abs(dur_lying-answer)/dur_lying*100) '%' ])



% evaluation at cow level
splitdata.test.predicted = double(yfit)-1;
% confusion matrix
plotconfusion(categorical(splitdata.test.checktruth_bin),...
              categorical(splitdata.test.predicted)) 
xticklabels({'non-lying','lying',''})
yticklabels({'non-lying','lying',''})
xlabel('Class');
ylabel('Predicted')
h=gcf;
saveas(h,[init_.resdir '\Fig_paper\confusion_all.tif'])


% evaluate performance at cow level
res.cow = unique(splitdata.test(:,1:2),'rows');
cows = unique(splitdata.test.cow);

for i = 1:height(res.cow)
    
    % training part
    idx = find(splitdata.training.cow == cows(i));
    res.cow.nosegtrain(i) = length(idx);
    res.cow.perctrain(i) = length(idx)/height(splitdata.training)*100;
    
    
    % results - segment classification
    rescow = splitdata.test(splitdata.test.cow == cows(i),:);
    res.cow.nosegtest(i) = height(rescow);
    res.cow.nosegtest_lie(i) = sum(rescow.checktruth_bin);
    res.cow.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
    res.cow.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
    res.cow.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
    res.cow.accuracy(i) = res.cow.correct(i)./(res.cow.correct(i)+res.cow.incorrect(i));
    
    % results - liedown accuracy
    res.cow.durdata(i) = sum(rescow.seglength)/3600;
    idx = find(rescow.checktruth_bin == 1);
    res.cow.durliedown(i) = sum(rescow.seglength(idx))/3600;
    
    idx = find(rescow.predicted == 1);
    res.cow.predliedown(i) = sum(rescow.seglength(idx))/3600;
    
    res.cow.liedownperc_dev(i) = 100*abs(res.cow.predliedown(i)-res.cow.durliedown(i))/...
            (res.cow.durliedown(i));
    res.cow.liedownacc(i) = 100-100*abs(res.cow.predliedown(i)-res.cow.durliedown(i))/...
            (res.cow.durliedown(i));
end
    
    
% evaluate performance at cow and at day level
res.day = unique(splitdata.test(:,1:3),'rows');
cows = unique(splitdata.test(:,2:3),'rows');

for i = 1:height(res.day)  
    
    % results - segment classification
    rescow = splitdata.test(splitdata.test.cow == cows.cow(i) & ...
                            splitdata.test.day == cows.day(i),:);
    res.day.nosegtest(i) = height(rescow);
    res.day.nosegtest_lie(i) = sum(rescow.checktruth_bin);
    res.day.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
    res.day.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
    res.day.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
    res.day.accuracy(i) = res.day.correct(i)./(res.day.correct(i)+res.day.incorrect(i));
    
    % results - liedown accuracy
    res.day.durdata(i) = sum(rescow.seglength)/3600;
    idx = find(rescow.checktruth_bin == 1);
    res.day.durliedown(i) = sum(rescow.seglength(idx))/3600;
    
    idx = find(rescow.predicted == 1);
    res.day.predliedown(i) = sum(rescow.seglength(idx))/3600;
    
    res.day.liedownperc_dev(i) = 100*abs(res.day.predliedown(i)-res.day.durliedown(i))/...
            (res.day.durliedown(i));
    res.day.liedownacc(i) = 100-100*abs(res.day.predliedown(i)-res.day.durliedown(i))/...
            (res.day.durliedown(i));
end

figure('Units','centimeters','OuterPosition',[5 5 28 14])
 subplot(1,2,1); hold on; box on; grid on
histogram(res.day.accuracy*100,'Normalization','probability','FaceColor',[0 102 102]./255)
title('Classification accuracy per cow-day')
xlabel('Accuracy [%]')
ylabel('Probability, cow-days')

subplot(1,2,2); hold on; box on; grid on;
histogram(res.day.liedownacc,'Normalization','probability','FaceColor',[102 0 0]./255)
title('Liedown duration prediction accuracy per cow-day')
xlabel('Accuracy [%]')
ylabel('Probability, cow-days')

h = gcf;
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_a.tif'])
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_b.fig'])
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_c.png'])
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy.pdf'])


answer = length(find(res.day.liedownacc < 80));
disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy lieduration less than 80%'])

answer = length(find(res.day.liedownacc > 90));
disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy more than than 90%'])

answer = length(find(res.day.accuracy >= 0.90));
disp(['in ' num2str(answer/height(res.day)*100) '% of the cow-days accuracy classification more than 90%'])

clear idx Q ind ind2 ans answer

%% classification performance with a different test dataset
% COW_split data from 10 cows = 5138 segments, 2417 (=47.04%) = 0
% from 29 unique cows

randno = sort(randperm(max(classdata.cow),10)');



answer = length(find(ismember(classdata.cow,randno)))  % number of segments
disp(['% data in the training set = ' num2str(answer/height(classdata)*100)])
answer2 = length(find(ismember(classdata.cow,randno) & classdata.checktruth_bin == 0)) % with value 0
disp(['% non-lying segments in training = ' num2str(answer2/answer*100)])

% unique(classdata.cow(ind)) % with number of cows


% training dataset and test data set
ind = find(ismember(classdata.cow,randno));
splitdata.training = classdata(ind,:);
% splitdata.training.checktruth_bin = categorical(splitdata.training.checktruth_bin);
% splitdata.training.inslatted = categorical(splitdata.training.inslatted);

ind = find(~ismember(classdata.cow,randno));
splitdata.test = classdata(ind,:);
% splitdata.test.checktruth_bin = categorical(splitdata.test.checktruth_bin);
% splitdata.test.inslatted = categorical(splitdata.test.inslatted);

% train classifier -- all data
% % [trainedClassifier, validationAccuracy] = trainClassifier(classdata(:,3:end))


% train same classifier on training data only
[trainedClassifier, validationAccuracy] = trainClassifier(...
            splitdata.training(:,3:end))
yfit = trainedClassifier.predictFcn(splitdata.test(:,3:end));

length(find(double(yfit)-1 - splitdata.test.checktruth_bin==0)) / length(yfit)
% prediction accuracy = 91.26%

% correct 0
ind = find(splitdata.test.checktruth_bin==0);
disp(['No. of non-lying segments in test = ' num2str(length(ind))])
answer = length(find(double(yfit(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
disp(['No. of correctly predicted non-lying segments = ' num2str(answer)])
disp(['% of correctly predicted non-lying segments = ' num2str(answer/length(ind))])

% correct 1
ind = find(splitdata.test.checktruth_bin==1);
disp(['No. of lying segments in test = ' num2str(length(ind))])
answer = length(find(double(yfit(ind))-1 - splitdata.test.checktruth_bin(ind)==0));
disp(['No. of correctly predicted lying segments = ' num2str(answer)])
disp(['% of correctly predicted lying segments = ' num2str(answer/length(ind))])

% total duration comparison of non-lying time
ind = find(splitdata.test.checktruth_bin==0);  % not lying
dur_lying = sum(splitdata.test.seglength(ind));
disp(['Total non-lying time in testset = ' num2str(dur_lying/3600) 'hours'])
ind2 = find(double(yfit)-1 == 0 & splitdata.test.checktruth_bin ==0);
answer = sum(splitdata.test.seglength(ind2));
disp(['Total duration predicted non-lying = ' num2str(answer/3600) 'hours'])
disp(['Difference total predicted non-lying = ' num2str((dur_lying-answer)/3600)]);
disp(['Procentual deviation non-lying = ' num2str(abs(dur_lying-answer)/dur_lying*100) '%' ])

% total duration comparison of lying time
ind = find(splitdata.test.checktruth_bin==1);  % not lying
dur_lying = sum(splitdata.test.seglength(ind));
disp(['Total lying time in testset = ' num2str(dur_lying/3600) 'hours'])
ind2 = find(double(yfit)-1 == 1 & splitdata.test.checktruth_bin ==1);
answer = sum(splitdata.test.seglength(ind2));
disp(['Total duration predicted lying = ' num2str(answer/3600) 'hours'])
disp(['Difference total predicted lying = ' num2str((dur_lying-answer)/3600)]);
disp(['Procentual deviation lying = ' num2str(abs(dur_lying-answer)/dur_lying*100) '%' ])



% evaluation at cow level
figure; hold on
splitdata.test.predicted = double(yfit)-1;
% confusion matrix
plotconfusion(categorical(splitdata.test.checktruth_bin),...
              categorical(splitdata.test.predicted)) 
xticklabels({'non-lying','lying',''})
yticklabels({'non-lying','lying',''})
xlabel('Class');
ylabel('Predicted')
h=gcf;
saveas(h,[init_.resdir '\Fig_paper\confusion_all_cowbasedsplit.png'])


% evaluate performance at cow level
res.cow_split2 = unique(splitdata.test(:,1:2),'rows');
cows = unique(splitdata.test.cow);

for i = 1:height(res.cow_split2)
    
    % training part
    idx = find(splitdata.training.cow == cows(i));
    res.cow_split2.nosegtrain(i) = length(idx);
    res.cow_split2.perctrain(i) = length(idx)/height(splitdata.training)*100;
    
    
    % results - segment classification
    rescow = splitdata.test(splitdata.test.cow == cows(i),:);
    res.cow_split2.nosegtest(i) = height(rescow);
    res.cow_split2.nosegtest_lie(i) = sum(rescow.checktruth_bin);
    res.cow_split2.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
    res.cow_split2.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
    res.cow_split2.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
    res.cow_split2.accuracy(i) = res.cow_split2.correct(i)./(res.cow_split2.correct(i)+res.cow_split2.incorrect(i));
    
    % results - liedown accuracy
    res.cow_split2.durdata(i) = sum(rescow.seglength)/3600;
    idx = find(rescow.checktruth_bin == 1);
    res.cow_split2.durliedown(i) = sum(rescow.seglength(idx))/3600;
    
    idx = find(rescow.predicted == 1);
    res.cow_split2.predliedown(i) = sum(rescow.seglength(idx))/3600;
    
    res.cow_split2.liedownperc_dev(i) = 100*abs(res.cow_split2.predliedown(i)-res.cow_split2.durliedown(i))/...
            (res.cow_split2.durliedown(i));
    res.cow_split2.liedownacc(i) = 100-100*abs(res.cow_split2.predliedown(i)-res.cow_split2.durliedown(i))/...
            (res.cow_split2.durliedown(i));
end

% evaluate performance at cow and at day level
res.day_split2 = unique(splitdata.test(:,1:3),'rows');
cows = unique(splitdata.test(:,2:3),'rows');

for i = 1:height(res.day_split2)  
    
    % results - segment classification
    rescow = splitdata.test(splitdata.test.cow == cows.cow(i) & ...
                            splitdata.test.day == cows.day(i),:);
    res.day_split2.nosegtest(i) = height(rescow);
    res.day_split2.nosegtest_lie(i) = sum(rescow.checktruth_bin);
    res.day_split2.nosegtest_nonlie(i) = sum(rescow.checktruth_bin==0);
    res.day_split2.correct(i) = length(find(rescow.checktruth_bin == rescow.predicted));
    res.day_split2.incorrect(i) = length(find(rescow.checktruth_bin ~= rescow.predicted));
    res.day_split2.accuracy(i) = res.day_split2.correct(i)./(res.day_split2.correct(i)+res.day_split2.incorrect(i));
    
    % results - liedown accuracy
    res.day_split2.durdata(i) = sum(rescow.seglength)/3600;
    idx = find(rescow.checktruth_bin == 1);
    res.day_split2.durliedown(i) = sum(rescow.seglength(idx))/3600;
    
    idx = find(rescow.predicted == 1);
    res.day_split2.predliedown(i) = sum(rescow.seglength(idx))/3600;
    
    res.day_split2.liedownperc_dev(i) = 100*abs(res.day_split2.predliedown(i)-res.day_split2.durliedown(i))/...
            (res.day_split2.durliedown(i));
    res.day_split2.liedownacc(i) = 100-100*abs(res.day_split2.predliedown(i)-res.day_split2.durliedown(i))/...
            (res.day_split2.durliedown(i));
end
    


figure('Units','centimeters','OuterPosition',[5 5 28 14])
 subplot(1,2,1); hold on; box on; grid on
histogram(res.day_split2.accuracy*100,'Normalization','probability','FaceColor',[0 102 102]./255)
title('Classification accuracy per cow-day')
xlabel('Accuracy [%]')
ylabel('Probability, cow-days')

subplot(1,2,2); hold on; box on; grid on;
histogram(res.day_split2.liedownacc,'Normalization','probability','FaceColor',[102 0 0]./255)
title('Liedown duration prediction accuracy per cow-day')
xlabel('Accuracy [%]')
ylabel('Probability, cow-days')

h = gcf;
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_cowselsplit.tif'])
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_cowselsplit.png'])
saveas(h,[init_.resdir '\Fig_paper\prediction_accuracy_cowselsplit.fig'])

answer = length(find(res.day_split2.liedownacc < 80));
disp(['in ' num2str(answer/height(res.day_split2)*100) '% of the cow-days accuracy lieduration less than 80%'])

answer = length(find(res.day_split2.liedownacc > 90));
disp(['in ' num2str(answer/height(res.day_split2)*100) '% of the cow-days accuracy more than than 90%'])

answer = length(find(res.day_split2.accuracy >= 0.90));
disp(['in ' num2str(answer/height(res.day_split2)*100) '% of the cow-days accuracy classification more than 90%'])

save('final_results.mat','splitdata','trainedClassifier','res')

%% check changes and when not detected
% in 'data.cow_x.day_y': "ground truth" = when islying changes
%                        "ischange" = when change is detected
% data_ld.cow_x = gives summary of lying behaviour

%-------------------------------PAPER--------------------------------------
% table 1: summary of the cow data (previous script)
% table 2: summary of the lying bout data
% table 3: performance metrics of the algorithm - changepoints ifv gaps
% table 4: performance metrics of the algorithm - classification
%-------------------------------PAPER--------------------------------------


%-------- TABLE 2 ---------
cows = fieldnames(data_ld);
res.lie = table(cows,'VariableNames',{'cow'});
res.bouts = table({''},'VariableNames',{'cow'});
for i = 1:length(cows)
    
    % first select all data for which cpts are detected
    ind = find(contains(classdata.id, cows{i}));
    subset = classdata(ind,:);
    days = unique(subset.day);
    data_ld.(cows{i})(ismember(floor(data_ld.(cows{i}).numdate),days)==0,:)=[];
    sdata.(cows{i})(ismember(floor(sdata.(cows{i}).numdate),days)==0,:)=[];
    
    % add standing duration to data_ld
    data_ld.(cows{i}).standdur(:,1) = NaN;
    data_ld.(cows{i}).standdur(2:end) = diff(data_ld.(cows{i}).numdate)*24*60;
    data_ld.(cows{i}).duration = (datenum(data_ld.(cows{i}).dateup)-...
                                 datenum(data_ld.(cows{i}).date))*24*60; % in min
    data_ld.(cows{i}).duration(data_ld.(cows{i}).duration > 12*60) = NaN;
    data_ld.(cows{i}).standdur(data_ld.(cows{i}).standdur > 12*60) = NaN;
    
    % summarize per cow
    res.lie.numstart(i) = min(floor(data_ld.(cows{i}).numdate));
    res.lie.numend(i) = max(floor(data_ld.(cows{i}).numdate));
    res.lie.nodays(i) = length(unique(floor(data_ld.(cows{i}).numdate)));
    res.lie.nobouts(i) = height(data_ld.(cows{i}));
    res.lie.noshort10minLIE(i) = length(find(data_ld.(cows{i}).duration < 10));
    res.lie.noshort10minUP(i) = length(find(data_ld.(cows{i}).standdur < 10));
    res.lie.avg_dur(i) = nanmean(data_ld.(cows{i}).duration);
    res.lie.avg_noperday(i) = res.lie.nobouts(i)/res.lie.nodays(i);

    % summarize lying bout data
    bdata = sdata.(cows{i});
    bouts = unique(bdata.IceBout);
    res.bouts = table({''},'VariableNames',{'cow'});
    res.bouts.cow(1) = cows(i); 
    for j = 1:length(bouts)
        res.bouts.cow(j) = cows(i); 
        ind = find(bdata.IceBout == bouts(j));
        res.bouts.IceBout(j) = bouts(j);
        res.bouts.avg_z_ld(j) = nanmean(sdata.(cows{i}).avg_z_sm(ind));
        res.bouts.std_z_ld(j) = nanstd(sdata.(cows{i}).avg_z_sm(ind));
        res.bouts.avg_znorm_ld(j) = nanmean(sdata.(cows{i}).znorm(ind));
        res.bouts.std_znorm_ld(j) = nanstd(sdata.(cows{i}).znorm(ind));
        res.bouts.std_cd_ld(j) = nanstd(sdata.(cows{i}).centerdist(ind));
        res.bouts.std_cdnorm_ld(j) = nanstd(sdata.(cows{i}).cdnorm(ind));

%         T = T+1;
    end
    
    % summarize per cow lying
    res.lie.avg_z_ld(i) = nanmean(res.bouts.avg_z_ld);
    res.lie.std_z_ld(i) = nanmean(res.bouts.std_z_ld);
    res.lie.avg_znorm_ld(i) = nanmean(res.bouts.avg_znorm_ld);
    res.lie.std_znorm_ld(i) = nanmean(res.bouts.std_znorm_ld);
    res.lie.std_cd_ld(i) = nanmean(res.bouts.std_cd_ld);
    res.lie.std_cdnorm_ld(i) = nanstd(res.bouts.std_cdnorm_ld);
    
    % summarize lying bout data
    bdata = sdata.(cows{i});
    bouts = unique(bdata.IceBout);
    res.bouts = table({''},'VariableNames',{'cow'});
    res.bouts.cow(1) = cows(i); 
    for j = 1:length(bouts)
        res.bouts.cow(j) = cows(i); 
        ind = find(bdata.IceBout == bouts(j));
        res.bouts.IceBout(j) = bouts(j);
        res.bouts.avg_z_ld(j) = nanmean(sdata.(cows{i}).avg_z_sm(ind));
        res.bouts.std_z_ld(j) = nanstd(sdata.(cows{i}).avg_z_sm(ind));
        res.bouts.avg_znorm_ld(j) = nanmean(sdata.(cows{i}).znorm(ind));
        res.bouts.std_znorm_ld(j) = nanstd(sdata.(cows{i}).znorm(ind));
        res.bouts.std_cd_ld(j) = nanstd(sdata.(cows{i}).centerdist(ind));
        res.bouts.std_cdnorm_ld(j) = nanstd(sdata.(cows{i}).cdnorm(ind));
    end
    
    % add number of non-lying phases to sdata
    ldata = sdata.(cows{i});
    ldata.prevly(1) = 1;
    ldata.prevly(2:end) = ldata.islying(1:end-1);
    ind = find(ldata.islying ~= ldata.prevly);
    ind(end+1) = height(ldata);
    ind(1,2) = 1; ind(2:end,2)= ind(1:end-1,1);
    ind(:,3) = 1:length(ind);
    for j = 1:size(ind,1)
        ldata.seg(ind(j,2):ind(j,1)) = ind(j,3);
    end
    
    % summarize lying bout data
    bdata = ldata(ldata.islying == 0,:);
    bouts = unique(bdata.seg);
    res.bouts = table({''},'VariableNames',{'cow'});
    res.bouts.cow(1) = cows(i); 
    for j = 1:length(bouts)
        res.bouts.cow(j) = cows(i); 
        ind = find(ldata.seg == bouts(j));
        res.bouts.seg(j) = bouts(j);
        res.bouts.avg_z_nld(j) = nanmean(ldata.avg_z_sm(ind));
        res.bouts.std_z_nld(j) = nanstd(ldata.avg_z_sm(ind));
        res.bouts.avg_znorm_nld(j) = nanmean(ldata.znorm(ind));
        res.bouts.std_znorm_nld(j) = nanstd(ldata.znorm(ind));
        res.bouts.std_cd_nld(j) = nanstd(ldata.centerdist(ind));
        res.bouts.std_cdnorm_nld(j) = nanstd(ldata.cdnorm(ind));
    end
    
    
    % summarize per cow not lying
    res.lie.avg_z_nld(i) = nanmean(res.bouts.avg_z_nld);
    res.lie.std_z_nld(i) = nanmean(res.bouts.std_z_nld);
    res.lie.avg_znorm_nld(i) = nanmean(res.bouts.avg_znorm_nld);
    res.lie.std_znorm_nld(i) = nanmean(res.bouts.std_znorm_nld);
    res.lie.std_cd_nld(i) = nanmean(res.bouts.std_cd_nld);
    res.lie.std_cdnorm_nld(i) = nanmean(res.bouts.std_cdnorm_nld);
    
end

writetable(res.lie,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','liebehave')

clear ans answer answer2 bouts dur_lying i idx ind ind2 j k l randno
clear h bdata cpdata fieldname_ T yfit validationAccuracy rescow

%-------- TABLE 3 ---------
% table 3: performance metrics of the algorithm - changepoints ifv gaps
%       

% add true changes window of 300
cows = fieldnames(data);

for i = 1:length(cows)
    days = fieldnames(data.(cows{i}));
    cpfield = days(2:2:end);
    days = days(1:2:end);
    
    % express  time relative in seconds
    sdata.(cows{i}).numsec = round(sdata.(cows{i}).numdate*3600*24);
    truedetected = [];
    T = 1;
    for j = 1:length(cpfield)
        % add what is considered as a true change
        data.(cows{i}).(cpfield{j}).numsec = ...
                   data.(cows{i}).(cpfield{j}).numdatestart *24*3600;
        for k = 1:height(data.(cows{i}).(cpfield{j}))
            truedetected = [truedetected; (round(data.(cows{i}).(cpfield{j}).numsec(k)) - 300 : ...
                        round(data.(cows{i}).(cpfield{j}).numsec(k)) + 300)' T*ones(601,1)];
                    T=T+1; % count changes
        end
    end
    
    % to table
    truedetected = array2table(truedetected,'VariableNames',{'numsec','cptdetected'});
    [~,ind] = unique(truedetected.numsec);
    truedetected = truedetected(ind,:); % delete overlapping
    
    % join
    sdata.(cows{i}) = outerjoin(sdata.(cows{i}),truedetected,'Keys','numsec','MergeKeys',1);
    sdata.(cows{i})(isnan( sdata.(cows{i}).object_name),:) = [];
    
    
    % find unique rows/time-cp
    sdata.(cows{i}).prevly(1) = 0;
    sdata.(cows{i}).prevly(2:end) = sdata.(cows{i}).islying(1:end-1);
    ind = find(sdata.(cows{i}).prevly ~= sdata.(cows{i}).islying);
    ind(:,2) = 1:length(ind);
    sdata.(cows{i}).changeno(:,1) = 0;
    sdata.(cows{i}).changeno(ind(:,1)) = ind(:,2);
    
    % add changedetected
    sdata.(cows{i}).changedetected(~isnan(sdata.(cows{i}).cptdetected)) = 1;
    
    
    %%% add the gapsize of the segment where the change is in
    changesidx = find(sdata.(cows{i}).changeno ~= 0);
    for l = 1:length(changesidx)
        try
            ind = find(isnan(sdata.(cows{i}).avg_x_sm((changesidx(l)-1800):(changesidx(l)+1800))));
        catch
            try
                ind = find(isnan(sdata.(cows{i}).avg_x_sm((changesidx(l)):(changesidx(l)+1800))));
                ind = [zeros(1800,1);ind];
            catch
                ind = find(isnan(sdata.(cows{i}).avg_x_sm((changesidx(l)-1800):end)));
            end
        end
        sdata.(cows{i}).noNaNhour(changesidx(l)) = length(ind);
    end
    
    %%% first measurement of day
    for l = 0:12
        ind = find(floor(sdata.(cows{i}).numdate) == l & ...
                   ~isnan(sdata.(cows{i}).cdnorm),1,'first');
        if ~isempty(ind)
            sdata.(cows{i}).isfirst(ind) = 1;
        end
    end
  
    
    
end


cows = fieldnames(data);
for i = 1:length(cows)
    % add segmentno!!!
    segdatamerge = cpts_multi.(cows{i}).cp(:,[11 12]);
    segdatamerge.numdatestart = round(segdatamerge.numdatestart*86400);
    segdatamerge.numdateend = [round(segdatamerge.numdatestart(2:end));...
                               height(sdata.(cows{i}))];
    segdatamerge.segnumber = (1:height(segdatamerge))';
    
    for l = 1:height(segdatamerge)
        idx1 = find(sdata.(cows{i}).numsec == segdatamerge.numdatestart(l));
        idx2 = find(sdata.(cows{i}).numsec == segdatamerge.numdateend(l))-1;
        sdata.(cows{i}).segnumber(idx1:idx2) = segdatamerge.segnumber(l);
    end
end

% create table with all changes (ground truth)
cows = fieldnames(data);
allchanges = [];
for i = 1:length(cows)
    
    allchanges = [allchanges;sdata.(cows{i})(sdata.(cows{i}).changeno ~= 0,:)];
    
    %%%% to do: gapsize chart of closest gap with diff
    %%%% to do: time to previous change
    
end

allchanges(allchanges.isfirst == 1,:) = [];

% add gapsize between Current and next + add trime to previous
allchanges.currentsegNaN(:,1) = NaN;
allchanges.prevsegNaN(:,1) = NaN; 
allchanges.nextsegNaN(:,1) = NaN;
for i = 1:height(allchanges)
    
    field_ = sprintf('cow_%d',allchanges.object_name(i));
    
    % nan% in current
    ind = find(sdata.(field_).segnumber == allchanges.segnumber(i));
    ind2 = sum(isnan(sdata.(field_).avg_z_sm(ind)));
    allchanges.currentsegNaN(i) = ind2/length(ind)*100;
    % nan% in previous
    ind = find(sdata.(field_).segnumber == allchanges.segnumber(i)-1);
    ind2 = sum(isnan(sdata.(field_).avg_z_sm(ind)));
    allchanges.prevsegNaN(i) = ind2/length(ind)*100;
    % nan% in next
    ind = find(sdata.(field_).segnumber == allchanges.segnumber(i)+1);
    ind2 = sum(isnan(sdata.(field_).avg_z_sm(ind)));
    allchanges.nextsegNaN(i) = ind2/length(ind)*100;
    
end

disp(['in total, the dataset contains ' num2str(height(allchanges)) ' changes'])
answer = length(find(isnan(allchanges.cptdetected)))/height(allchanges)*100;

disp(['from these, ' num2str(answer) '% does not correspond with cpt detected'])

% x changes not detected  = y chenges per cowday in the dataset
% from which z are not corresponding to a changepoint
% x unique cowdays = z per cow per day

disp([ num2str(sum(allchanges.changedetected)./height(allchanges)*100) '% of the changes corresponding to detected CP'])
answer = height(unique([allchanges.object_name floor(allchanges.numdate)],'rows'));
disp(['with ' num2str(answer) ' cowdays, this is ' num2str(sum(allchanges.changedetected==0)/answer) ' per cow-day'])

writetable(allchanges,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','changedet')

% plot time of the day where changes are not detected
subset = allchanges(allchanges.changedetected==0,:);
figure;
histogram((subset.time_in_h),'Normalization','probability')





% plot time since previous change
allchanges.timesinceprevious(1) = NaN;
allchanges.timesinceprevious(2:end) = diff(allchanges.numsec);
allchanges.timesinceprevious(allchanges.timesinceprevious < 0) = NaN;
% plot time since previous when no cp is detected
subset = allchanges(allchanges.changedetected==0,:);
figure;
histogram((subset.timesinceprevious/60),0:10:500,'Normalization','probability')

histogram((subset.currentsegNaN),0:5:100,'Normalization','cdf')


disp(['The number of undetected changes associated with another change within 10 minutes = ' num2str(length(find(subset.timesinceprevious/60 <= 10)))])
disp(['The number of undetected changes associated with another change within 10 minutes = ' num2str(length(find(subset.timesinceprevious/60 <= 10))/height(subset)*100) '%'])
disp(['The number of undetected changes associated with another change within 20 minutes = ' num2str(length(find(subset.timesinceprevious/60 <= 20)))])
disp(['The number of undetected changes associated with another change within 20 minutes = ' num2str(length(find(subset.timesinceprevious/60 <= 20))/height(subset)*100) '%'])


% the number of changes lying next to a bigger gap (within 1h of data, gap
% > 5 min
disp(['The number of undetected changes associated with >15 min missing data surrounding the change = ' num2str(length(find(subset.noNaNhour > 15*60)))])
disp(['The number of undetected changes associated with >15 min missing data surrounding the change = ' num2str(length(find(subset.noNaNhour > 15*60))/height(subset)*100) '%'])
disp(['The number of undetected changes associated with >30 min missing data surrounding the change = ' num2str(length(find(subset.noNaNhour > 30*60)))])
disp(['The number of undetected changes associated with >30 min missing data surrounding the change = ' num2str(length(find(subset.noNaNhour > 30*60))/height(subset)*100) '%'])

disp(['The number of undetected changes in segment with gappercent larger than 20% = ' ...
      num2str(sum(subset.currentsegNaN >= 20)/height(subset)*100)])


% at cow level
cows = unique(allchanges.object_name);
res.cowcpts = table(cows,'VariableNames',{'cowid'});
for i = 1:length(cows)
    subset = allchanges(allchanges.object_name == cows(i),:);
    res.cowcpts.nogroundtruth(i) = height(subset);
    res.cowcpts.nonotdetected(i) = sum(subset.changedetected == 0);
    res.cowcpts.percnotdetected(i) = res.cowcpts.nonotdetected(i) /res.cowcpts.nogroundtruth(i);
    
    subset2 = subset(subset.changedetected == 0,:);
    res.cowcpts.nonotdetected_NaN20(i) = sum(subset2.currentsegNaN >= 20)/height(subset2)*100;
    
    
end

writetable(res.cowcpts,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','cowcpts_TP')




writetable(res.cow,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','cowcpts')
writetable(res.day,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','daycpts')

writetable(res.cow_split2,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','cowcpts2')
writetable(res.day_split2,[init_.resdir '\Fig_paper\ex_tables.xlsx'],'Sheet','daycpts2')



%---------------------TABLE4-----------------------------------------------
% table 4: performance metrics of the algorithm - classification








