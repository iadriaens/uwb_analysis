%% S15


% clear workspace
clear variables
close all
clc

%% set file paths

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
% load data
load([init_.resdir 'D3_sdata_IQ_UWB.mat'])
load([init_.resdir 'D2_data_meta.mat'])

% delete cow 280 and 495 (no iceQube data) 
sdata = rmfield(sdata,'cow_280');
sdata = rmfield(sdata,'cow_495');

%% set constants

% set technique for changepoint analysis and segmentation
cpts_multi.stat = 'std';      % technique based on which cpts are found for z

% set settings
cpts_multi.MinDistance = 300;     % 10 min 
cpts_multi.NumChanges = 60;        % total max number of changes

% set detection for true positive cpt detection
cpts_multi.window = 600/(60*60*24);      % windowsize in which cpts are detected

% figure settings
cpts_multi.Units = 'centimeters';     % set units
cpts_multi.figPos = [105 -5 35 15];      % set position

% reference date for numeric time
refdate = datenum(2019,7,3);

%% calculate changepoints for z and cd together

fields_ = fieldnames(sdata);
for f = 1:length(fields_)
    % clear and close
    clear data
    close all

    % set cow
    cow = fields_{f};
    
    % add numeric date
    sdata.(cow).numdate = datenum(sdata.(cow).date)-refdate;
    
    % add length of lying or not lying bout and delete these changes when
    % <300s
    liedur = sdata.(cow).islying(1:end-1);
    liedur(:,2) = sdata.(cow).islying(2:end);
    idx = find(liedur(:,1) ~= liedur(:,2));
    idx(1,2) = 1;
    idx(2:end,2) = idx(1:end-1,1);
    idx(:,3) = idx(:,1)-idx(:,2);
    idx(idx(:,3) > 300,:) = [];
    if ~isempty(idx)
        for jj = 1:size(idx,1)
            sdata.(cow).islying(idx(jj,2):idx(jj,1)) = ...
                sdata.(cow).islying(idx(jj,2)-1);
            sdata.(cow).IceBout(idx(jj,2):idx(jj,1)) = ...
                sdata.(cow).IceBout(idx(jj,2)-1);
        end
    end
    
    % for cow 1144 -D11; 6518 - D7; 7460 -D5 and 9905 -D5 data are missing
    % delete data of these days
    if contains(cow,num2str(1144)) 
        sdata.(cow)(floor(sdata.(cow).numdate) == 11,:) = [];
    elseif contains(cow,num2str(6518))
        sdata.(cow)(floor(sdata.(cow).numdate) == 7,:) = [];
    elseif contains(cow,num2str(7460)) || contains(cow,num2str(9905))
        sdata.(cow)(floor(sdata.(cow).numdate) == 5,:) = [];
    end

    % normalize at cow level
    sdata.(cow).znorm = normalize(sdata.(cow).avg_z_sm,'range');
    sdata.(cow).cdnorm = normalize(sdata.(cow).centerdist,'range');
    
    % select data z and CD from first lying to last lying bout of cow
    data = sdata.(cow)(:,[2 3 19 7 13 17 18 15 20 21 ]);
                                              
    % summary of the lying bouts
    data.islying_prev(1) = 0;
    data.islying_prev(2:end) = data.islying(1:end-1);
    data.lychange = ((data.islying == 1 & data.islying_prev == 0) | ...
                     (data.islying == 0 & data.islying_prev == 1));
    data_ld.(cow) = data(data.lychange == 1,:);
    data_ld.(cow).dateup(:,1) = NaT;           
    data_ld.(cow).dateup(1:2:end-1) = data_ld.(cow).date(2:2:end);
    data_ld.(cow)(isnat(data_ld.(cow).dateup),:) = [];
    data_ld.(cow) = movevars(data_ld.(cow),'dateup','After','date');
    data_ld.(cow).numdatedown = datenum(data_ld.(cow).date) - refdate;
    data_ld.(cow).numdateup = datenum(data_ld.(cow).dateup) - refdate;

    % select data without missing
    data2 = data(~isnan(data.znorm) & ~isnan(data.cdnorm),:);
    data2.numdate = datenum(data2.date)-refdate;
    
    
    
    %---------------------------TO DO---------------------------------
    %   add summary of data_ld.(cow) to general summary of lying bouts
    %---------------------------TO DO---------------------------------
    
    
    % changepoint detection per cowday
    days = unique(floor(data2.numdate));
    for i = 1:length(days)
        % select daydata 
        data_day = data2(floor(data2.numdate) == days(i),:);

        % analyse segment, but only when enough data available
        if height(data_day) > 12*60*60
            % prepare results
            day = sprintf('day_%d',days(i));

            % prepare figure
            k = figure('Units',cpts_multi.Units,...
               'OuterPosition',cpts_multi.figPos);            % figure
            subplot(1,1,1); hold on; box on;            % prepare z plot
            title([cow ', day = ' num2str(days(i)) ', Z'], ...
                   'Interpreter','none');      
            xlabel('Day'); ylabel('Z, height (m)')
            ylim([0 1])       
%             subplot(2,1,2); hold on; box on;title('CD');        % prepare CD plot
%             ylim([0 12]); xlabel('Day'); ylabel('Distance from center (m)')

            

            
            
            
            % plot z and cd of the segment
%             subplot(2,1,1);
            plot(data_day.numdate, data_day.znorm, ...
                 'LineWidth',0.9,'Color',[160/255 160/255 160/255])
%             subplot(2,1,2);
            plot(data_day.numdate, data_day.cdnorm, ...
                 'LineWidth',0.9,'Color',[0/255 153/255 153/255])
             axis tight

            % plot when up and down
%             subplot(2,1,1);
            plot(data_day.numdate, -data_day.islying*0.8+0.9,'k','LineWidth',1.2)
%             subplot(2,1,2);
%             plot(data_day.numdate, -data_day.islying*10+11,'k','LineWidth',1.2)
           
            % select events
            downevents = data_ld.(cow)(floor(data_ld.(cow).numdatedown) == days(i),:);   % all down
            upevents = data_ld.(cow)(floor(data_ld.(cow).numdatedown) == days(i),:);       % all up

            % add detection window for true positive cases (cp evaluation)
            downevents.winstart = downevents.numdatedown - cpts_multi.window/2;
            downevents.winend = downevents.numdatedown + cpts_multi.window/2;
            upevents.winstart = upevents.numdateup - cpts_multi.window/2;
            upevents.winend = upevents.numdateup + cpts_multi.window/2;

            % plot events as ground thruth + window
            for j = 1:height(downevents)
                % z subplot
%                 subplot(2,1,1);
                plot([downevents.numdatedown(j) downevents.numdatedown(j)], ...
                     [0 1],'LineWidth',0.8,'Color',[204/255 0 102/255])
                plot([downevents.winstart(j) downevents.winend(j)],...
                      [0.8 0.8],'LineWidth',4.5,'Color',[204/255 0 102/255])
%                 % cd subplot
%                 subplot(2,1,2);
%                 plot([downevents.numdatedown(j) downevents.numdatedown(j)], ...
%                      [0 15],'LineWidth',0.8,'Color',[204/255 0 102/255])
%                 plot([downevents.winstart(j) downevents.winend(j)],...
%                       [6 6],'LineWidth',4.5,'Color',[204/255 0 102/255])
            end
            for j = 1:height(upevents)
                % z subplot
%                 subplot(2,1,1); xlim([days(i) days(i)+1])
                plot([upevents.numdateup(j) upevents.numdateup(j)], ...
                     [0 1],'LineWidth',0.8,'Color',[204/255 0 102/255])
                plot([upevents.winstart(j) upevents.winend(j)],...
                     [0.8 0.8],'LineWidth',4.5,'Color',[204/255 0 102/255])

                % cd subplot
%                 subplot(2,1,2);
%                 xlim([days(i) days(i)+1])
%                 plot([upevents.numdateup(j) upevents.numdateup(j)], ...
%                      [0 15],'LineWidth',0.8,'Color',[204/255 0 102/255]) 
%                 plot([upevents.winstart(j) upevents.winend(j)],...
%                       [6 6],'LineWidth',4.5,'Color',[204/255 0 102/255])
            end
            
            %--------------------------------------------------------------
            % NEW:  combined segmentation
            tic
            [cpts_rmulti.(cow).(day).ipoints, cpts_rmulti.(cow).(day).resid] = ...
                 findchangepts([data_day.znorm(:,1) data_day.cdnorm(:,1)]',...
                               'Statistic',cpts_multi.stat,...
                               'MinDistance',cpts_multi.MinDistance,...
                               'MaxNumChanges',cpts_multi.NumChanges);
            cpts_rmulti.(cow).(day).ipoints = cpts_rmulti.(cow).(day).ipoints';
            cpts_rmulti.(cow).(day).ipoints = [1;cpts_rmulti.(cow).(day).ipoints];
            cpts_rmulti.(cow).(day).ipoints(1:end-1,2) = cpts_rmulti.(cow).(day).ipoints(2:end,1);
            cpts_rmulti.(cow).(day).ipoints(end,2) = height(data_day);
            toc
            

            % calculate mean value + std
            for j = 1:size(cpts_rmulti.(cow).(day).ipoints,1)
                cpts_rmulti.(cow).(day).ipoints(j,3) = ...
                    mean(data_day.znorm(...
                            cpts_rmulti.(cow).(day).ipoints(j,1):...
                            cpts_rmulti.(cow).(day).ipoints(j,2)));
                cpts_rmulti.(cow).(day).ipoints(j,4) = ...
                    std(data_day.znorm(...
                            cpts_rmulti.(cow).(day).ipoints(j,1):...
                            cpts_rmulti.(cow).(day).ipoints(j,2)));
                cpts_rmulti.(cow).(day).ipoints(j,5) = ...
                    mean(data_day.cdnorm(...
                            cpts_rmulti.(cow).(day).ipoints(j,1):...
                            cpts_rmulti.(cow).(day).ipoints(j,2)));
                cpts_rmulti.(cow).(day).ipoints(j,6) = ...
                    std(data_day.cdnorm(...
                            cpts_rmulti.(cow).(day).ipoints(j,1):...
                            cpts_rmulti.(cow).(day).ipoints(j,2)));        
                        
                % plot changepoints znorm
                plot([data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,2)) ...
                      data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,2))],...
                      [0 1],'b','LineWidth',1)
                plot([data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,1)) ...
                      data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,2))],...
                     [cpts_rmulti.(cow).(day).ipoints(j,3) ...
                      cpts_rmulti.(cow).(day).ipoints(j,3)],'b','LineWidth',2)
                plot([data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,1)) ...
                      data_day.numdate(cpts_rmulti.(cow).(day).ipoints(j,2))],...
                     [cpts_rmulti.(cow).(day).ipoints(j,5) ...
                      cpts_rmulti.(cow).(day).ipoints(j,5)],'b','LineWidth',2) 
            end

            
    %         saveas(k,[init_.resdir 'fig_cpts_' cow '_' day '.fig'])
            saveas(k,[init_.resdir 'fig_cpts_both_' cow '_' day '.tif'])
            close all
        end
    end

    % summarize lying bouts included for manuscript
    data_ld.(cow).duration = 60*(data_ld.(cow).numdateup-data_ld.(cow).numdatedown);

end



save([init_.resdir 'D7all_cpts_multi.mat'],'cpts_rmulti','cpts_multi','data_ld')
save([init_.resdir 'D8_sdata.mat'],'sdata', '-v7.3')
