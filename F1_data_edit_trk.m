function [out] = F1_data_edit_trk(data_,nseconds)
% this function starts from raw tracklab data and does following editing
% steps:    - sort data and delete lines with NaN values for object/cow
%           - insert missing values when no data available
%           - summarize / accumulate in average values of nseconds
%
% input   =   tracklab data of one day with at least following columns:
%                   - object_name = cow id (numeric)
%                   - time = timestamp yyyy/MM/dd HH:mm:ss.SSS
%                   - location_x
%                   - location_y
%                   - location_z
%
% output  =   out     with following columns:
%                   - object_name = cow id (numeric)
%                   - date = date and time
%                   - time_in_h = time of the day in hours
%                   - avg_x = average x position
%                   - avg_y = average y position
%                   - avg_z = average z position
%
%

% sort per cow (object_name)
data_ = sortrows(data_,'object_name');

% delete lines with object_name = NaN;
data_(isnan(data_.object_name),:) = [];

% calculate the numeric timestamp
data_.numtime = datenum(data_.time);

% convert numeric timestamp to seconds of that day and round per second
data_.secdata = round((data_.numtime - floor(data_.numtime(1)))*24*3600);

% calculate timestamps as preparation for subs (= indices for accumulation)
data_.accumsubs = (data_.secdata - mod(data_.secdata,nseconds))./ nseconds;

% array with theoretical number of values if no missing data
A = ones(length(unique(data_.object_name)),1);   % unique cows
B = (1:(24*3600/nseconds))-1';  % start from zeros, 86400 unique s/d
C = A*B;                        % each row has 0:86399
C = reshape(C',[length(B)*length(A),1]);        % reshape C all rows

% assign a cow number to each time point
A = unique(data_.object_name);                  % array of unique cows
B = ones(1, length(1:(24*3600/nseconds)));      % array of ones length s/d
D = A*B;                                        % array of cowids
C(:,2) = reshape(D',[length(B)*length(A),1]);   % reshape C all rows

% add a column to C with the indices/subs = unique per cow and per sec
C(:,3) = (1:size(C,1))';

% array to table and outerjoin into input variable
C = array2table(C,'VariableNames',{'accumsubs','object_name','subs'});
data_ = outerjoin(data_,C,...
                'Keys',{'accumsubs','object_name'},...
                'MergeKeys',true);
data_ = sortrows(data_,{'object_name','subs'});
   
% accumulate arrays in a new array with average location
C.avg_x = round(accumarray(data_.subs,data_.location_x,[],@nanmean),2);
C.avg_y = round(accumarray(data_.subs,data_.location_y,[],@nanmean),2);
C.avg_z = round(accumarray(data_.subs,data_.location_z,[],@nanmean),2);

% create output
out = C;

% add date and time stamp
date = mean(floor(data_.numtime(~isnan(data_.numtime))));
dates = date + (out.accumsubs .* nseconds ./ (3600*24));
out.date = ...
    datetime(dates, 'ConvertFrom','datenum');
out.time_in_h = out.accumsubs .* nseconds ./ 3600;

% remove redundant variables
out = removevars(out,{'subs','accumsubs'});
out = movevars(out,{'date','time_in_h'},'After','object_name');
