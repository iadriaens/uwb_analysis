%% S8_cpnts  -- final
%--------------------------------------------------------------------------
% created by @adria036 (ines adriaens)
%         on January 4, 2022
%   
%--------------------------------------------------------------------------
% after research on automated detection of optimal number of CP and to be
% detected / penalty > decision to implement a slightly different strategy
% and use the segmentation of the time series in addition to a set of
% sensible, expert-based criteria to judge whether a segment belongs to a
% lying bout or not, or whether for that segment there is too much
% uncertainty (e.g. because of gaps)
% This gives 3 categories: (1) lying; (2) not lying; (3) undefined
% Also the bouts of (1) and (2) with a very short duration will be
% categorized in (3).
%--------------------------------------------------------------------------
% The criteria that will be implemented rely on:
%   - position in the barn
%   - absolute level 
%   - relative level compared to previous segments / compared to entire
%   cowday time series?
%   - variability of level and std
%--------------------------------------------------------------------------
% Doing the separate categorization allows to assess (1) accuracy of
% changepoints in terms of sensitivity (WHEN a cow lies down or gets up, is
% there a CP detected?); and (2) categorization (separately, and assuming
% that not all CP inherently agree with a change.
%--------------------------------------------------------------------------
% at the same time, number of changepoints are still limited to a sensible
% amount, namely 3x the average number of changes = 10 * 30 * 2 (up/do) =
% 60; and the minimum distance (i.e. 300 second = 5 minutes) separation
% between cp.
%--------------------------------------------------------------------------


% clear workspace
clear variables
close all
clc

%% set file paths

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
% load data
load([init_.resdir 'D3_sdata_IQ_UWB.mat'])
load([init_.resdir 'D2_data_meta.mat'])

% delete cow 280 and 495 (no iceQube data) 
sdata = rmfield(sdata,'cow_280');
sdata = rmfield(sdata,'cow_495');

%% set constants

% set technique for changepoint analysis and segmentation
cpts.Zstat = 'std';      % technique based on which cpts are found for z
cpts.CDstat = 'std';      % technique based on which cpts are found for CD

% set settings
cpts.MinDistance = 600;     % 10 min 
cpts.NumChanges = 40;        % total max number of changes

% set detection for true positive cpt detection
cpts.window = 600/(60*60*24);      % windowsize in which cpts are detected

% figure settings
cpts.Units = 'centimeters';     % set units
cpts.figPos = [105 -5 30 22];      % set position

% reference date for numeric time
refdate = datenum(2019,7,3);
    
%% calculate changepoints for z and cd

fields_ = fieldnames(sdata);
for f = 1:length(fields_)
    % clear and close
    clear data
    close all

    % set cow
    cow = fields_{f};

    % select data z and CD from first lying to last lying bout of cow
    data = sdata.(cow)(:,[2 3 7 13 17 18 15]);
                                              
    % summary of the lying bouts
    data.islying_prev(1) = 0;
    data.islying_prev(2:end) = data.islying(1:end-1);
    data.lychange = ((data.islying == 1 & data.islying_prev == 0) | ...
                     (data.islying == 0 & data.islying_prev == 1));
    data_ld.(cow) = data(data.lychange == 1,:);
    data_ld.(cow).dateup(:,1) = NaT;           
    data_ld.(cow).dateup(1:2:end-1) = data_ld.(cow).date(2:2:end);
    data_ld.(cow)(isnat(data_ld.(cow).dateup),:) = [];
    data_ld.(cow) = movevars(data_ld.(cow),'dateup','After','date');
    data_ld.(cow).numdatedown = datenum(data_ld.(cow).date) - refdate;
    data_ld.(cow).numdateup = datenum(data_ld.(cow).dateup) - refdate;

    % select data without missing
    data2 = data(~isnan(data.avg_z_sm) & ~isnan(data.centerdist),:);
    data2.numdate = datenum(data2.date)-refdate;
    
    
    %---------------------------TO DO---------------------------------
    %   add summary of data_ld.(cow) to general summary of lying bouts
    %---------------------------TO DO---------------------------------
    
    
    % changepoint detection per cowday
    days = unique(floor(data2.numdate));
    for i = 1:length(days)
        % select daydata 
        data_day = data2(floor(data2.numdate) == days(i),:);

        % analyse segment, but only when enough data available
        if height(data_day) > 12*60*60
            % prepare results
            day = sprintf('day_%d',days(i));

            % prepare figure
            k = figure('Units',cpts.Units,...
               'OuterPosition',cpts.figPos);            % figure
            subplot(2,1,1); hold on; box on;            % prepare z plot
            title([cow ', day = ' num2str(days(i)) ', Z'], ...
                   'Interpreter','none');      
            xlabel('Day'); ylabel('Z, height (m)')
            ylim([0 1.7])       
            subplot(2,1,2); hold on; box on;title('CD');        % prepare CD plot
            ylim([0 12]); xlabel('Day'); ylabel('Distance from center (m)')

            % plot z and cd of the segment
            subplot(2,1,1);
            plot(data_day.numdate, data_day.avg_z_sm, ...
                 'LineWidth',0.9,'Color',[60/255 179/255 113/255])
            subplot(2,1,2);
            plot(data_day.numdate, data_day.centerdist, ...
                 'LineWidth',0.9,'Color',[60/255 179/255 113/255])

            % plot when up and down
            subplot(2,1,1);
            plot(data_day.numdate, -data_day.islying*1.5+1.6,'k','LineWidth',1.2)
            subplot(2,1,2);
            plot(data_day.numdate, -data_day.islying*10+11,'k','LineWidth',1.2)
           
            % select events
            downevents = data_ld.(cow)(floor(data_ld.(cow).numdatedown) == days(i),:);   % all down
            upevents = data_ld.(cow)(floor(data_ld.(cow).numdatedown) == days(i),:);       % all up

            % add detection window for true positive cases (cp evaluation)
            downevents.winstart = downevents.numdatedown - cpts.window/2;
            downevents.winend = downevents.numdatedown + cpts.window/2;
            upevents.winstart = upevents.numdateup - cpts.window/2;
            upevents.winend = upevents.numdateup + cpts.window/2;

            % plot events as ground thruth + window
            for j = 1:height(downevents)
                % z subplot
                subplot(2,1,1);
                plot([downevents.numdatedown(j) downevents.numdatedown(j)], ...
                     [0 2.5],'LineWidth',0.8,'Color',[204/255 0 102/255])
                plot([downevents.winstart(j) downevents.winend(j)],...
                      [1 1],'LineWidth',4.5,'Color',[204/255 0 102/255])
                % cd subplot
                subplot(2,1,2);
                plot([downevents.numdatedown(j) downevents.numdatedown(j)], ...
                     [0 15],'LineWidth',0.8,'Color',[204/255 0 102/255])
                plot([downevents.winstart(j) downevents.winend(j)],...
                      [6 6],'LineWidth',4.5,'Color',[204/255 0 102/255])
            end
            for j = 1:height(upevents)
                % z subplot
                subplot(2,1,1); xlim([days(i) days(i)+1])
                plot([upevents.numdateup(j) upevents.numdateup(j)], ...
                     [0 2.5],'LineWidth',0.8,'Color',[204/255 0 102/255])
                plot([upevents.winstart(j) upevents.winend(j)],...
                     [1 1],'LineWidth',4.5,'Color',[204/255 0 102/255])

                % cd subplot
                subplot(2,1,2);
                xlim([days(i) days(i)+1])
                plot([upevents.numdateup(j) upevents.numdateup(j)], ...
                     [0 15],'LineWidth',0.8,'Color',[204/255 0 102/255]) 
                plot([upevents.winstart(j) upevents.winend(j)],...
                      [6 6],'LineWidth',4.5,'Color',[204/255 0 102/255])
            end
            

            % changepoints avg_z
            [cptsr.(cow).(day).ipoints, cptsr.(cow).(day).resid] = ...
                 findchangepts(data_day.avg_z_sm,...
                               'Statistic',cpts.Zstat,...
                               'MinDistance',cpts.MinDistance,...
                               'MaxNumChanges',cpts.NumChanges);
            cptsr.(cow).(day).ipoints = [[1;...
                        cptsr.(cow).(day).ipoints(1:end-1);...
                        cptsr.(cow).(day).ipoints(end)],...
                        [cptsr.(cow).(day).ipoints;height(data_day)]];

            % calculate mean value + std
            for j = 1:size(cptsr.(cow).(day).ipoints,1)
                cptsr.(cow).(day).ipoints(j,3) = ...
                    mean(data_day.avg_z_sm(...
                            cptsr.(cow).(day).ipoints(j,1):...
                            cptsr.(cow).(day).ipoints(j,2)));
                cptsr.(cow).(day).ipoints(j,4) = ...
                    std(data_day.avg_z_sm(...
                            cptsr.(cow).(day).ipoints(j,1):...
                            cptsr.(cow).(day).ipoints(j,2)));
                % plot changepoints avg_z_sm
                subplot(2,1,1)
                plot([data_day.numdate(cptsr.(cow).(day).ipoints(j,2)) ...
                      data_day.numdate(cptsr.(cow).(day).ipoints(j,2))],...
                      [0 3],'b','LineWidth',1)
                plot([data_day.numdate(cptsr.(cow).(day).ipoints(j,1)) ...
                      data_day.numdate(cptsr.(cow).(day).ipoints(j,2))],...
                     [cptsr.(cow).(day).ipoints(j,3) ...
                      cptsr.(cow).(day).ipoints(j,3)],'b','LineWidth',2) 
            end

            % changepoints cd
            [cptsr.(cow).(day).ipointsCD, cptsr.(cow).(day).residCD] = ...
                findchangepts(data_day.centerdist,...
                               'Statistic',cpts.CDstat,...
                               'MinDistance',cpts.MinDistance,...
                               'MaxNumChanges',cpts.NumChanges);
            cptsr.(cow).(day).ipointsCD = [[1;...
                      cptsr.(cow).(day).ipointsCD(1:end-1);...
                      cptsr.(cow).(day).ipointsCD(end)],...
                      [cptsr.(cow).(day).ipointsCD;height(data_day)]];

            % calculate mean value + std
            for j = 1:size(cptsr.(cow).(day).ipointsCD,1)
                cptsr.(cow).(day).ipointsCD(j,3) = mean(data_day.centerdist(...
                                    cptsr.(cow).(day).ipointsCD(j,1):...
                                    cptsr.(cow).(day).ipointsCD(j,2)));
                cptsr.(cow).(day).ipointsCD(j,4) = std(data_day.centerdist(...
                                    cptsr.(cow).(day).ipointsCD(j,1):...
                                    cptsr.(cow).(day).ipointsCD(j,2)));
                % plot changepoints centerdist
                subplot(2,1,2)
                plot([data_day.numdate(cptsr.(cow).(day).ipointsCD(j,2)) ...
                      data_day.numdate(cptsr.(cow).(day).ipointsCD(j,2))],...
                      [0 15],'b','LineWidth',1)
                plot([data_day.numdate(cptsr.(cow).(day).ipointsCD(j,1)) ...
                      data_day.numdate(cptsr.(cow).(day).ipointsCD(j,2))],...
                     [cptsr.(cow).(day).ipointsCD(j,3) 
                      cptsr.(cow).(day).ipointsCD(j,3)],'b',...
                      'LineWidth',2) 
            end
    %         saveas(k,[init_.resdir 'fig_cpts_' cow '_' day '.fig'])
            saveas(k,[init_.resdir 'fig_cpts_cat_' cow '_' day '.tif'])
            close all
        end
    end

    % summarize lying bouts included for manuscript
    data_ld.(cow).duration = 60*(data_ld.(cow).numdateup-data_ld.(cow).numdatedown);

end
% clear variables
clear ans cow data data_day  day days downevents f fields_ j i h ind
clear x y upevents seglist data2 k refdate

% save results
save([init_.resdir 'D6all_cpts_res_cat.mat'],'cptsr','cpts','data_ld')






