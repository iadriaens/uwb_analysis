# -*- coding: utf-8 -*-
"""
Created on Tue Sep 23 15:15:01 2014

@author: psa119
"""
#import io
import pandas as pd
import struct
import numpy as np
#import sys
#sys.setrecursionlimit(10000)


def readTrackLab(name):
    f2 = open(name, "rb")
    b = f2.read()
    f2.close()
    sp = b.split("S\x00e\x00s\x00s\x00i\x00o\x00n")
    #The last element of split contains also a Track, this needs to be fixed
    ids = []
    coords = []
    for track in sp[1:-1]:    
        #for track in sp[1:2]:
        if (len(track) > (94+83)):
            id = track.replace("\x00", "")[11:26]     
            ids.append(id)
            #print len(track)
            coords.append(TrackToCoords(track[95:], id))

    df = pd.concat(coords)
    return(df, coords)

#Recursive is too slow
def splitter(s,r=[],w=84):
    pos = 0
    N = len(s)    
    while pos < (N-w):
        r.append(s[pos:(w+pos)])
        pos = pos+w
    return(r)
        


#Split to chunks of 83 bytes
#def splitter(s, r=[], w=84):
#    if len(s) < w:
#        r.append(s)
#        return(r)
#    else:
#        r.append(s[:w])
#        return(splitter(s[w:], r))

def TrackToCoords(chunk, id):
    x = []
    y = []
    z = []
    time = []     
    Track = splitter(chunk, [])
    #print "Splitter is done!"    

    #print map(len, Track)
    #global sika    
    #sika=Track[-1]
    tulos = []
    
    for row in Track:
        time = row[:52].replace('\x00',"")
        x,y,z = struct.unpack('3d', row[55:][:24])
        tulos.append({"time" : time, "id" : id , "x" : x, "y" : y, "z" : z})        
        #y.append(struct.unpack('d', row[55+8:][:8])[0])
        #z.append(struct.unpack('d', row[55+16:][:8])[0])
    #print "Converter is done!"    
    
    return(pd.DataFrame(tulos))


def distancetraveled(x, y):
    x1 = x[:-1,]
    x2 = x[1:,]
    y1 = y[:-1,]
    y2 = y[1:,]
    return(np.sqrt(((x2-x1)**2+(y2-y1)**2)))        

def speed(xs, ys):
    return(np.sqrt(xs**2+ys**2))



if __name__=="__main__":
    import tracklab
    df, coords = tracklab.readTrackLab("Tlab_data/Kuitukoe 3.6.2014.tlp")
    
    c1 = coords[1][1000:2000]   
    plot(c1["x"], c1["y"])    







