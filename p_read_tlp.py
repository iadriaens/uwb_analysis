# -*- coding: utf-8 -*-
"""
Created on April 15, 2021 based on code provided by M. Pastell [orcid 0000-0002-5810-4801]
Authors: Matti Pastell
Modified by Ines Adriaens

Code to read and decode x,y,z location data in .tlp projects from tracklab (NOLDUS) and write them to text files
"""
# import packages
import glob
import p_tracklab as tracklab

# define file paths .tlp and data folder to write
files = glob.glob(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\\" +
                  r"Projects\eEllen\B4F_indTracking_cattle\B4F_data\*.tlp")
print(files)

savedir = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\\" + \
          r"Projects\eEllen\B4F_indTracking_cattle\B4F_data\\"

# read over files and store
for f in files:
    print(f)
    df = tracklab.readTrackLab(f)
    print(df.head())
    filename_out = f.replace(".tlp", ".txt")
    print(filename_out)
    df.to_csv(filename_out,
              sep=";",
              index=False
              )
